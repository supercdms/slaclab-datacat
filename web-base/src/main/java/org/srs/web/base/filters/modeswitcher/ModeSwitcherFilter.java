package org.srs.web.base.filters.modeswitcher;

/**
 *
 * @author The FreeHEP team @ SLAC
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import org.srs.web.base.utils.SrsWebUtils;
import org.srs.web.base.db.ConnectionManager;

public class ModeSwitcherFilter implements Filter {

    private static String configDb;
    private static String experiment, contextName;
    private static final String initParameterStr = "experiment.mode.filter.";
    private static final CopyOnWriteArrayList<ModeSwitcherChangeListener> listeners = new CopyOnWriteArrayList<>();
    private static FilterConfig filterConfig;
    private static boolean initialized = false;

    /**
     * Destroy the Filter and cleanup.
     * The defalt value is also stored in the preferences.
     *
     */
    @Override
    public void destroy() {
    }

    //
    // This method returns the mode from the session, if present. If not it returns the default Mode.
    //
    public static Mode getMode(HttpSession session, String modeName) {
        return getModes(session).get(modeName);
    }
    public static Mode getMode(String modeName,PageContext context) {
        return getMode(context.getSession(),modeName);
    }

    public static HashMap<String, Mode> getModes(HttpSession session) {
        if (session != null) {
            Object sessionObj = session.getAttribute("appModes");
            if (sessionObj == null) {
                sessionObj = initializeModes(filterConfig, getExperimentFromSession(session, experiment), contextName, configDb);
                session.setAttribute("appModes", sessionObj);
            }
            return (HashMap<String, Mode>) sessionObj;
        }
        return initializeModes(filterConfig, experiment, contextName, configDb);
    }

    public static String getVariable(HttpServletRequest request, String varName) {
        return getVariable(request.getSession(false), varName);
    }

    public static String getVariable(HttpSession session, String varName) {
        return getVariables(session).get(varName);
    }

    public static HashMap<String, String> getVariables(HttpSession session) {
        try {
            if (session != null) {
                Object vars = session.getAttribute("appVariables");
                if (vars == null) {
                    vars = initializeVariables(getModes(session), getExperimentFromSession(session, experiment), contextName, configDb);
                    session.setAttribute("appVariables", vars);
                }
                return (HashMap<String, String>) vars;
            }
            return initializeVariables(getModes(session), experiment, contextName, configDb);
        } catch (ServletException e) {
            throw new RuntimeException(e);
        }
    }

    // Static methods here
    public static String getExperimentFromSession(HttpSession session) {
        return getExperimentFromSession(session, null);
    }

    public static String getExperimentFromSession(HttpSession session, String defaultExperiment) {
        if (session == null) {
            return defaultExperiment != null ? defaultExperiment : experiment;
        }
        Object modes = session.getAttribute("appModes");
        return modes == null ? experiment : ((HashMap<String, Mode>) modes).get("experiment").getValue();
    }

    /**
     * Initialize the Filter.
     *
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

        ModeSwitcherFilter.initialized = true;

        ModeSwitcherFilter.filterConfig = filterConfig;

        contextName = SrsWebUtils.getContextName(filterConfig);
        if (contextName == null || "".equals(contextName)) {
            contextName = "ROOT";
        }
        experiment = filterConfig.getInitParameter("experiment");
        configDb = filterConfig.getInitParameter("configDb");

        if (configDb == null) {
            throw new ServletException("Configuration database must be provided as init parameter for ModeSwitcherFilter.");
        }

        try {
            try (Connection c = ConnectionManager.getConnection(configDb)) {
                ModeSwitcherDatabaseUtils.checkModeSwitcherDatabaseTables(c, experiment);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }

    }

    public static boolean hasBeenInitialized() {
        return initialized;
    }

    static HashMap<String, Mode> initializeModes(FilterConfig filterConfig, String experiment, String contextName, String configDb) {

        HashMap<String, Mode> modes = loadModesFromConfigDb(experiment, contextName, configDb);

        // Setting modes from system properties
        Enumeration systemProperties = System.getProperties().keys();
        loadModesFromEnvironment(modes, filterConfig, systemProperties, "System.Properties");

        Enumeration initParameterNames = filterConfig.getInitParameterNames();
        loadModesFromEnvironment(modes, filterConfig, initParameterNames, "web.xml");

        if ( ! modes.get("experiment").getValue().equals(experiment) ) {
            modes.get("experiment").setValue(experiment,"user");
        }
        return modes;
    }

    static HashMap<String, String> initializeVariables(HashMap<String, Mode> modes, String experiment, String contextName, String configDb) throws ServletException {
        return loadVariablesFromConfigDb(modes, experiment, contextName, configDb);
    }

    void refreshModesAndVariablesForRequest(HttpServletRequest request, HashMap<String, String> changedModes) throws ServletException {
        // First Initialize default Modes and Variables

        HttpSession session = request.getSession(false);
        String experiment_var = session != null ? getMode(session, "experiment").getValue() : experiment;

        boolean reloadModes = changedModes == null || changedModes.containsKey("experiment");
        
        if (session != null) {
            session.removeAttribute("appVariables");

            HashMap<String, Mode> modes = getModes(session);
            if ( reloadModes ) {
                session.removeAttribute("appModes");
                modes = initializeModes(filterConfig, experiment_var, contextName, configDb);
                session.setAttribute("appModes", modes);
            }
            session.setAttribute("appVariables", initializeVariables(modes, experiment_var, contextName, configDb));
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);

        //
        // First Loop over the parameters to see if any mode is to be changed.
        //
        HashMap<String, String> changedModes = new HashMap<>();
        Set<String> modeNames = getModes(session).keySet();
        for (String modeName : modeNames) {

            String modeParam = request.getParameter(modeName);
            if (modeParam != null) {
                Mode mode = getMode(session, modeName);
                try {
                    if (!mode.getValue().equals(modeParam)) {
                        // At this point, if the session is null, it must be created.
                        if (session == null) {
                            session = request.getSession(true);
                            mode = getMode(session, modeName);
                        }
                        mode.setValue(modeParam, "user");
                        changedModes.put(mode.getName(), mode.getValue());
                    }
                } catch (IllegalArgumentException iae) {
                    System.out.print("** Value " + modeParam + " is not allowed for mode " + mode.getName());
                }
            }
        }

        //Check if Mode experiment changed with respect to the variable.
        //This code is to detect if there is a redirection from the ExperimentContextFilter.
        if ( getMode(session,"experiment").getValue() != getVariable(session,"experiment") ) {
            if ( !changedModes.containsKey("experiment") ) {
                changedModes.put("experiment", getMode(session,"experiment").getValue());
            }
            
        }
        
        // If needed load the variables
        if (changedModes.size() > 0) {
            refreshModesAndVariablesForRequest(request, changedModes);
            // Send Notifications here!
            notifyListeners(request, session, changedModes);
        }

        PrintWriter writer = null;
        /* Generate the /modeSwitcherModes HTML */
        if (request.getServletPath().equals("/modeSwitcherModes")) {
            writer = servletResponse.getWriter();
            Iterator<Mode> modesIterator = getModes(session).values().iterator();
            while (modesIterator.hasNext()) {
                Mode m = modesIterator.next();
                writer.println(m.getName() + " " + m.getValue() + " " + m.getAllowedValues() + " " + m.getDefaultValue() + " " + m.getEnvironment() + " " + m.getExperiment() + " " + m.getApplication());
            }
        } else if (request.getServletPath().equals("/modeSwitcherVariables")) {
            writer = servletResponse.getWriter();
            HashMap<String, String> vars = getVariables(session);
            Iterator<String> varsIterator = vars.keySet().iterator();
            while (varsIterator.hasNext()) {
                String var = varsIterator.next();
                writer.println(var + " " + vars.get(var));
            }
        } else if (request.getServletPath().equals("/modeSwitcherRefresh")) {
            refreshModesAndVariablesForRequest(request,null);
            String redirect = request.getParameter("redirect");
            if (redirect != null) {
                response.sendRedirect(redirect);
            }
        } else if (chain != null) {
            chain.doFilter(servletRequest, servletResponse);
        }

    }

    /**
     * Load the modes for this filter from the configDb.
     *
     */
    private static HashMap<String, Mode> loadModesFromConfigDb(String exp, String contextName, String configDb) {
        HashMap<String, Mode> modes = new HashMap<>();
        try {
            try (Connection c = ConnectionManager.getConnection(configDb)) {
                String sqlQuery = "select mode_name, allowed_values, default_value, application, experiment from application_mode where (experiment is null or experiment = ? ) " + " and (application is null or application = ?) " + " order by application,experiment asc";

                try (PreparedStatement statement = c.prepareStatement(sqlQuery)) {
                    statement.setString(1, exp);
                    statement.setString(2, contextName);
                    ResultSet r = statement.executeQuery();
                    while (r.next()) {
                        String name = r.getString("mode_name");
                        if (!modes.containsKey(name)) {
                            modes.put(name, new Mode(name, r.getString("default_value"), r.getString("allowed_values"), r.getString("experiment"), r.getString("application")));
                        }
                    }
                    r.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        } catch (JspException | RuntimeException e) {
            throw new RuntimeException(e);
        }
        return modes;
    }

    /**
     * Load the variables for this session.
     *
     */
    private static HashMap<String, String> loadVariablesFromConfigDb(HashMap<String, Mode> modes, String experiment, String contextName, String configDb) {
        HashMap<String, String> variables = new HashMap<>();

        try {
            try (Connection c = ConnectionManager.getConnection(configDb)) {

                String sqlQuery = "select variable_name as name, variable_value as value from application_variable where (experiment is null or experiment = ?) " + " and (application is null or application = ?) ";

                String modeSql = "";
                Iterator<Mode> modeIterator = modes.values().iterator();
                while (modeIterator.hasNext()) {
                    Mode m = modeIterator.next();
                    modeSql += " ( mode_name = ? and mode_value = ?) ";
                    if (modeIterator.hasNext()) {
                        modeSql += " or ";
                    }
                    //Add the modes to the list of variables
                    String name = m.getName();
                    variables.put(name, m.getValue());
                }

                if (!modeSql.equals("")) {
                    sqlQuery += " and ( ( mode_name is null  and mode_value is null ) or " + modeSql + " ) ";
                }

                sqlQuery += " order by application, experiment, mode_name, mode_value asc";

                try (PreparedStatement statement = c.prepareStatement(sqlQuery)) {
                    statement.setString(1, experiment);
                    statement.setString(2, contextName);
                    
                    int count = 3;
                    modeIterator = modes.values().iterator();
                    while (modeIterator.hasNext()) {
                        Mode m = modeIterator.next();
                        statement.setString(count++, m.getName());
                        statement.setString(count++, m.getValue());
                    }
                    
                    try (ResultSet r = statement.executeQuery()) {
                        while (r.next()) {
                            String name = r.getString("name");
                            if (!variables.containsKey(name)) {
                                variables.put(name, r.getString("value"));
                            }
                        }
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }

        for (String mode : modes.keySet()) {
            variables.put(mode, modes.get(mode).getValue());
        }

        return variables;
    }
    
    
    
    /**
     * Load the values of a given variable for all Modes
     *
     * @param variableName
     * @param pageContext
     * @return 
     */
    public static Map<String,String> getVariableValues(String variableName, PageContext pageContext) {
        HashMap<String,String> variableForAllModes = new HashMap<>();
        String expName = getVariable(pageContext.getSession(),"experiment");
        String contName = SrsWebUtils.getContextName(pageContext);

        try {
            try (Connection c = ConnectionManager.getConnection(configDb)) {

                String sqlQuery = "select variable_value as value, mode_value from application_variable where (experiment is null or experiment = ?)  and (application is null or application = ?) and variable_name = ?";

                try (PreparedStatement statement = c.prepareStatement(sqlQuery)) {
                    statement.setString(1, expName);
                    statement.setString(2, contName);
                    statement.setString(3, variableName);
                    
                    try (ResultSet r = statement.executeQuery()) {
                        while (r.next()) {
                            String modeValue = r.getString("mode_value");
                            if (modeValue == null) {
                                modeValue = "";
                            }
                            String variableValue = r.getString("value");
                            if ( variableValue == null ) {
                                variableValue = "";
                            }
                            variableForAllModes.put(modeValue, variableValue);
                        }
                    }
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        } catch (RuntimeException | JspException e) {
            throw new RuntimeException(e);
        }

        return variableForAllModes;
    }

    private static void loadModesFromEnvironment(HashMap<String, Mode> modes, FilterConfig filterConfig, Enumeration e, String environment) {
        while (e.hasMoreElements()) {
            String elementName = (String) e.nextElement();
            if (elementName.startsWith(initParameterStr)) {
                String modeName = elementName.substring(initParameterStr.length());
                String modeValue = null;
                if (environment.equals("web.xml")) {
                    modeValue = filterConfig.getInitParameter(elementName);
                } else if (environment.equals("System.Properties")) {
                    modeValue = System.getProperty(elementName);
                } else {
                    throw new RuntimeException("**** Invalid environment " + environment);
                }

                if (!modes.containsKey(modeName)) {
                    System.out.println("**** Invalid mode name " + initParameterStr + modeName + " provided in " + environment + ".");
                } else {
                    Mode m = modes.get(modeName);
                    if (m.isAllowedValue(modeValue)) {
                        System.out.println("**** Setting value of mode " + modeName + " to " + modeValue + " from " + environment + " for " + contextName + "( " + experiment + ", " + configDb + ")");
                        m.setValue(modeValue, environment);
                    } else {
                        System.out.println("**** Invalid mode value " + modeValue + " for mode " + modeName + " provided in " + environment + ".");
                    }
                }
            }
        }
    }

    public static void addModeSwitcherChangeListener(ModeSwitcherChangeListener listener) {
        listeners.add(listener);
    }

    public static void removeModeSwitcherChangeListener(HttpSession session) {
        Iterator<ModeSwitcherChangeListener> iter = listeners.iterator();
        while (iter.hasNext()) {
            ModeSwitcherChangeListener listener = iter.next();
            if (listener.getSession().equals(session)) {
                iter.remove();
            }
        }
    }

    public static void notifyListeners(ServletRequest request, HttpSession session, HashMap<String, String> list) {
        Iterator<ModeSwitcherChangeListener> iter = listeners.iterator();
        while (iter.hasNext()) {
            ModeSwitcherChangeListener listener = iter.next();
            if (listener.getSession().equals(session)) {
                listener.modeChanged(new ModeSwitcherChangedEvent(request, list));
            }
        }
    }
}
