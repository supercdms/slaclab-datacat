package org.srs.web.base.filters.login;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public class LoginStatusChangedEvent {
    
    public static final int USER_LOGGED_IN  = 0;
    public static final int USER_LOGGED_OUT = 1;
    public static final int USER_CHANGED = 2;
    
    private int id;
    private String userName;
    private HttpServletRequest request;
    
    LoginStatusChangedEvent(int id, String userName, HttpServletRequest request) {
        this.id = id;
        this.userName = userName;
        this.request = request;
    }
    
    public int getId() {
        return id;
    }
    
    public String getUserName() {
        return userName;
    }
    
    public HttpServletRequest getRequest() {
        return request;
    }
    
}
