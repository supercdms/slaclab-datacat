package org.srs.web.base.filters.refresh;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.StringTokenizer;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.srs.web.base.preferences.filter.PreferencesFilter;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public class RefreshFilter  implements Filter {
    
    private static int refreshRate = 60, minRefreshRate = 60, maxNumberOfRefresh = 30;
    private static String defaultPage = "defaultPage.jsp";
    private static ArrayList pages = new ArrayList();
    public static String refreshRateStr = "refreshRate";
    public static String refreshStr = "refreshIsOn";
    public static String startRefreshStr = "startRefresh";
    public static String stopRefreshStr = "stopRefresh";
    public static String refreshCountStr = "refreshCount";
    public static String errorMessageStr = "refreshRateErrorMessage";
    public static boolean preferencesAutoSave = false;
    public static String preferencesName = null;
    
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        
        String currentPage = getCurrentPage(request);
        
        if ( session.getAttribute(errorMessageStr) != null )
            session.removeAttribute(errorMessageStr);
        
        int sessionRefreshRate = refreshRate;
        Object refreshRateInSession = session.getAttribute(refreshRateStr);
        if ( refreshRateInSession != null )
            sessionRefreshRate = ((Integer)refreshRateInSession).intValue();
        String requestRefreshRate = request.getParameter(refreshRateStr);
        boolean problems = false;
        if ( requestRefreshRate != null ) {
            try {
                int tmp = Integer.valueOf(requestRefreshRate).intValue();
                if ( checkRefreshRate(tmp) ) {
                    sessionRefreshRate = tmp;
                } else {
                    session.setAttribute(errorMessageStr,"Illegal input value <b>"+requestRefreshRate+"</b>. Must be >= "+minRefreshRate);
                    problems = true;
                }
            } catch (Exception e) {
                session.setAttribute(errorMessageStr,"Illegal input value <b>"+requestRefreshRate+"</b>. Must be a positive integer >= "+minRefreshRate);
                problems = true;
            }
        }
        if ( ! problems ) {
            if ( currentPage.endsWith("jsp") ) {
                String refreshIsOnPar = request.getParameter(refreshStr);
                boolean refreshIsOn = false;
                if ( refreshIsOnPar != null && isRefreshable(currentPage) )
                    refreshIsOn = Boolean.valueOf(refreshIsOnPar).booleanValue();
                
                boolean startRefreshing = request.getParameter(startRefreshStr) != null;
                
                int refreshCount = maxNumberOfRefresh;
                if ( refreshIsOn ) {
                    
                    int refreshFromSession = -1;
                    if ( session.getAttribute(refreshCountStr) != null ) {
                        refreshFromSession = ((Integer)session.getAttribute(refreshCountStr)).intValue();
                    }
                    int refreshFromRequest = -1;
                    if ( request.getParameter(refreshCountStr) != null ) {
                        refreshFromRequest = Integer.valueOf( request.getParameter(refreshCountStr) ).intValue();
                    } 
                    
                    if ( ! startRefreshing )
                        refreshCount = refreshFromRequest;
                    
                    refreshCount--;
                    session.setAttribute(refreshCountStr,new Integer(refreshCount));
                    if ( refreshCount < 0 ) {
                        refreshIsOn = false;
                        refreshCount = maxNumberOfRefresh;
                    }
                }
                
                if ( refreshIsOn ) {
                    String requestUri = request.getRequestURI();
                    String queryString = "";
                    
                    Enumeration e = request.getParameterNames();
                    while ( e.hasMoreElements() ) {
                        String par = (String) e.nextElement();
                        if (  ! par.equals(RefreshFilter.startRefreshStr) && ! par.equals(RefreshFilter.refreshCountStr) && ! par.equals(RefreshFilter.stopRefreshStr )) {
                            String val = request.getParameter(par);
                            if ( ! queryString.equals("") )
                                queryString += "&";
                            queryString += par+"="+URLEncoder.encode(val,"UTF-8");
                        }
                    }
                    queryString += "&"+refreshCountStr+"="+refreshCount;
                    String tmp = String.valueOf(sessionRefreshRate)+";url="+requestUri+"?"+queryString;
                    response.setHeader("refresh", tmp );
                }
            }
        }
        
        if ( filterChain != null )
            filterChain.doFilter(servletRequest, servletResponse);
    }
    
    public static boolean checkRefreshRate(int rr) {
        return rr > minRefreshRate-1;
    }
    
    public static int getMinRefreshRate() {
        return minRefreshRate;
    }
    
    public static int getRefreshRate() {
        return refreshRate;
    }
    
    public void addRefreshablePage(String page) {
        pages.add(page);
    }
    
    public static boolean isRefreshable(String page) {
        return pages.size() == 0 ? true : pages.contains(page);
    }
    
    public static boolean isRefreshable(HttpServletRequest request) {
        return isRefreshable(getCurrentPage(request));
    }
    
    public static String getCurrentPage(HttpServletRequest request) {
        String contextPath = request.getContextPath();
        String requestURI = request.getRequestURI();
        String tmp = requestURI.substring(contextPath.length()+1);
        if ( ! tmp.equals("") )
            return tmp;
        return defaultPage;
    }
    
    public void init(FilterConfig filterConfig) throws ServletException {
        String param = filterConfig.getInitParameter("minRefreshRate");
        if ( param != null )
            minRefreshRate = Integer.valueOf(param).intValue();
        
        param = filterConfig.getInitParameter("refreshRate");
        if ( param != null ) {
            int tmp = Integer.valueOf(param).intValue();
            checkRefreshRate(tmp);
            refreshRate = tmp;
        }
        
        param = filterConfig.getInitParameter("maxNumberOfRefresh");
        if ( param != null )
            maxNumberOfRefresh = Integer.valueOf(param).intValue();
        
        param = filterConfig.getInitParameter("defaultPage");
        if ( param != null )
            defaultPage = param;
        
        param = filterConfig.getInitParameter("pagesToRefresh");
        if ( param != null ) {
            StringTokenizer stt = new StringTokenizer(param,",");
            while(stt.hasMoreTokens())
                addRefreshablePage(stt.nextToken());
        }
        
        param = filterConfig.getInitParameter("preferencesName");
        if ( param != null ) {
            PreferencesFilter.addPreference(param,"org.srs.web.base.filters.refresh.RefreshPreferencesBean");
            preferencesName = param;
        }
        
        param = filterConfig.getInitParameter("preferencesAutoSave");
        if ( param != null )
            preferencesAutoSave = Boolean.valueOf(param);
    }
    
    public static boolean getPreferencesAutoSave() {
        return preferencesAutoSave;
    }
    
    public static String getPreferencesName() {
        return preferencesName;
    }
    
    public void destroy() {
        // Nothing to do
    }
    
}