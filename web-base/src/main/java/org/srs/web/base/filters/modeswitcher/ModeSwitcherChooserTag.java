package org.srs.web.base.filters.modeswitcher;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * @author The FreeHEP team @ SLAC.
 *
 */
public class ModeSwitcherChooserTag extends SimpleTagSupport {
    
    private String href = "", target = null, mode = null;

     public void doTag() throws JspException, IOException {
        if ( mode == null )
            throw new JspException("Parameter \"mode\" must be provided to this tag ");
        this.writeHTMLChoice(mode);
    }
    
    protected void writeHTMLChoice(String mode) throws JspException, IOException {
        PageContext pageContext = (PageContext)getJspContext();
        
        Mode m = ModeSwitcherFilter.getMode(pageContext.getSession(), mode);
        
        if ( m == null )
            return;
        
        Writer writer = pageContext.getOut();
        List<String> allowedValues = m.getAllowedValuesList();
        for ( int i = 0; i < allowedValues.size(); i++ ) {
            String value = (String) allowedValues.get(i);
            if ( value.equals(m.getValue()) )
                writer.write("<b>"+value+"</b>");
            else {
                writer.write("<a href=\""+href+"?"+mode+"="+value+"\" ");
                if ( target != null )
                    writer.write(" target=\""+target+"\" ");
                writer.write(">"+value+"</a>");
            }
            if ( i < allowedValues.size() - 1)
                writer.write("&nbsp;|&nbsp;");
        }        
    }
    
    public void setHref(String href) {
        this.href = href;
    }
    
    public void setTarget(String target) {
        this.target = target;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
    
}