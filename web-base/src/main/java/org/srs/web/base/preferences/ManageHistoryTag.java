package org.srs.web.base.preferences;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class ManageHistoryTag extends SimpleTagSupport {
    
    private String name, property;
    
    public void doTag() throws JspException, IOException {
        Object preferences = PreferencesUtils.getPreferencesBean(name,getJspContext());
        Object history = PreferencesUtils.getPreference(preferences,property);
        
        String removeParameter = property+"_remove";
        
        HttpServletRequest request = (HttpServletRequest)((PageContext)getJspContext()).getRequest();
        String[] parValues = request.getParameterValues(removeParameter);
        if ( parValues != null ) {
            for ( int i = 0; i < parValues.length; i++ )
                PreferencesUtils.removeFromHistory(history,parValues[i]);
            
            if ( parValues.length > 0 )
                PreferencesUtils.savePreferences(preferences,name, getJspContext());
        }
        
        Iterator iter = PreferencesUtils.getHistoryIterator(history);
        int size = PreferencesUtils.getHistorySize(history);
        if ( size > 0 ) {
            Writer writer = getJspContext().getOut();
            
            writer.write("<form name=\"preference_"+name+"_manage_form\" >\n");
            writer.write("<table class=\"filtertable\">\n");
            writer.write("<tr>\n");
            writer.write("<th>Property : "+property+"</th>\n");
            writer.write("</tr>\n");
            while(iter.hasNext()) {
                writer.write("<tr>\n");
                String key = (String)iter.next();
                writer.write("<td>\n");
                writer.write("<input type=\"checkbox\" name=\""+removeParameter+"\" value=\""+key+"\">"+key+"</input>\n");
                writer.write("</td>\n");
                writer.write("</tr>\n");
            }
            writer.write("<tr>\n");
            writer.write("<td align=\"right\">\n");
            writer.write("<input type=\"submit\" name=\"RemovePreferences\" value=\"Remove Selected\"/>\n");
            writer.write("</td>\n");
            writer.write("</tr>\n");
            writer.write("</table>\n");
            writer.write("</form>\n");
        }
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setProperty(String property) {
        this.property = property;
    }
    
}