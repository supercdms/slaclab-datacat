package org.srs.web.base.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author The FreeHEP team @ SLAC
 */
public abstract class TimeUtils {
    
    public static long LAST_HOUR = 3600000;
    public static long LAST_24HOUR = 86400000;
    // The time in the database is in Mission Elapsed Time seconds, i.e. since Jan 01 2001.
    public static long millisecondsOffsetFromUnixEpoch = 978307200000L;

    public static Timestamp convertEpochTimeToTimestamp(long time) {
        return new Timestamp(time);
    }
    
    public static long convertMetSecondsToEpochTime(double secs) {
        return millisecondsOffsetFromUnixEpoch+(long)((secs)*1000);
    }
    
    public static double convertEpochTimeToMetSeconds(long time) {
        return ((double)(time-millisecondsOffsetFromUnixEpoch))/1000.;
    }
    
    public static String convertToSimpleDateFormat(String format) {
        String simpleDateFormat = new String(format);
        simpleDateFormat = simpleDateFormat.replace("%a","EEE");
        simpleDateFormat = simpleDateFormat.replace("%A","EEEE");
        simpleDateFormat = simpleDateFormat.replace("%b","MMM");
        simpleDateFormat = simpleDateFormat.replace("%B","MMMM");
        simpleDateFormat = simpleDateFormat.replace("%d","dd");
        simpleDateFormat = simpleDateFormat.replace("%e","d");
        simpleDateFormat = simpleDateFormat.replace("%H","HH");
        simpleDateFormat = simpleDateFormat.replace("%k","H");
        simpleDateFormat = simpleDateFormat.replace("%I","hh");
        simpleDateFormat = simpleDateFormat.replace("%l","h");
        simpleDateFormat = simpleDateFormat.replace("%j","DDD");
        simpleDateFormat = simpleDateFormat.replace("%m","MM");
        simpleDateFormat = simpleDateFormat.replace("%M","mm");
        simpleDateFormat = simpleDateFormat.replace("%p","a");
        simpleDateFormat = simpleDateFormat.replace("%s","SSS");
        simpleDateFormat = simpleDateFormat.replace("%S","ss");
        simpleDateFormat = simpleDateFormat.replace("%y","yy");
        simpleDateFormat = simpleDateFormat.replace("%Y","yyyy");
        return simpleDateFormat;
    }
    
    public static long now(String timezone) {
        return Calendar.getInstance(TimeZone.getTimeZone(timezone)).getTimeInMillis();
    }
    
    public static String toTime(long millis, String format, String timezone) {
        if ( millis < 0 )
            return "None";
        SimpleDateFormat df = new SimpleDateFormat(convertToSimpleDateFormat(format));
        if ( timezone != null )
            df.setTimeZone(TimeZone.getTimeZone(timezone));
        return df.format(new Date(millis));                
    }
    
    public static long toMillis(String date, String format, String timezone) throws java.text.ParseException {
        if ( date == null || date.equals("None") )
            return -1;
        SimpleDateFormat df = new SimpleDateFormat(convertToSimpleDateFormat(format));
        if ( timezone != null )
            df.setTimeZone(TimeZone.getTimeZone(timezone));
        return df.parse(date).getTime();                
    }
}
