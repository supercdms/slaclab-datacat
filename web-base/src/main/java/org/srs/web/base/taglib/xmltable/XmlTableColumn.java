package org.srs.web.base.taglib.xmltable;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public class XmlTableColumn {
    
    private String name, type, classStr="", group = null, decorator=null, title = null;
    private boolean isSortable;
    
    public XmlTableColumn(String name, String title, String type, boolean isSortable, String classStr, String group,String decorator) {
        this.name = name;
        this.type = type;
        this.title = title;
        this.isSortable = isSortable;
        if ( classStr != null )
            this.classStr = classStr;
        this.group = group;
        this.decorator = decorator;
    }
    
    public String getName() {
        return name;
    }
    
    public String getTitle() {
        if ( title != null )
            return title;
        return name;
    }
    
    public String getType() {
        return type;
    }
    
    public boolean getSortable() {
        return isSortable;
    }

    public String getClassstr() {
        return classStr;
    }
    
    public String getGroup() {
        return group;
    }

    public String getDecorator() {
        return decorator;
    }
    
}
