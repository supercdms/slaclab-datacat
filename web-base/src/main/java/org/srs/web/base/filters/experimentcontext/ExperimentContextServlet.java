/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.srs.web.base.filters.experimentcontext;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author turri
 */
public class ExperimentContextServlet extends HttpServlet {

    private static boolean debug = false;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if ( request.getParameter("debugExperimentContextServlet") != null )
            debug = request.getParameter("debugExperimentContextServlet").equals("true");

        String servletPath = request.getServletPath();

        String requestURL = request.getRequestURL().toString();
        String expMount = request.getContextPath()+"/exp/";
        String dispatch = requestURL.substring(requestURL.indexOf(expMount)).replace(expMount, "");
        int index = dispatch.indexOf("/");
        if ( index >= 0 )
            dispatch = dispatch.substring(dispatch.indexOf("/"));
        else
            dispatch = "/";

        if ( debug ) {
            System.out.println("***** ExperimentContextServlet "+servletPath+" "+requestURL+" "+dispatch);
        }
        if ( getServletContext() != null ) {
            getServletContext().getRequestDispatcher(dispatch).forward(request, response);
        }


    }


}
