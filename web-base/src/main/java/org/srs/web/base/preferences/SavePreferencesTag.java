package org.srs.web.base.preferences;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class SavePreferencesTag extends SimpleTagSupport {
    
    private String name;
    
    public void doTag() throws JspException, IOException {        
        Object preferences = PreferencesUtils.getPreferencesBean(name, getJspContext() );
        PreferencesUtils.savePreferences(preferences,name,getJspContext());
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
}