package org.srs.web.base.decorator;

import javax.servlet.jsp.PageContext;
import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.properties.MediaTypeEnum;

/**
 *
 * @author tonyj
 */
public class EventColumnDecorator implements DisplaytagColumnDecorator
{  
   public Object decorate(Object columnValue, PageContext pageContext, MediaTypeEnum media)
   {
      return columnValue instanceof Number ? String.format("%,d",((Number) columnValue).longValue()) : columnValue;
   }
}
