package org.srs.web.base.preferences.filter;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import org.srs.web.base.db.ConnectionManager;
import org.srs.web.base.filters.login.LoginUtils;
import org.srs.web.base.filters.modeswitcher.Mode;
import org.srs.web.base.filters.modeswitcher.ModeSwitcherFilter;

/**
 *
 * @author The FreeHEP team @ SLAC.
 *
 */

public class PreferencesFilter implements Filter {
    
    private static String preferencesDatabase = null, application = null;
    private static Hashtable prefs = new Hashtable();
    private static Hashtable prefsUser = new Hashtable();
    private boolean hasPreferences = false;
    private static String experiment = null;
    
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        
        if ( hasPreferences && request.getSession().getAttribute("preferencesLoaded") == null ) {
            request.getSession().setAttribute("preferencesLoaded","true");
            doPreferences(request);
        }
        filterChain.doFilter(servletRequest,servletResponse);
    }
    
    public void init(FilterConfig filterConfig) throws ServletException {
        
        // Look if a database is provided for the preferences
        preferencesDatabase = filterConfig.getInitParameter("preferencesDatabase");
        application = filterConfig.getInitParameter("application");
        experiment = filterConfig.getInitParameter("experiment");


        // If the database is available, look for the preferences
        if ( preferencesDatabase != null && application != null ) {
            Enumeration parameters = filterConfig.getInitParameterNames();
            while( parameters.hasMoreElements() ) {
                String par = (String) parameters.nextElement();
                if ( par.equals("preferencesDatabase") || par.equals("application") || par.equals("experiment") )
                    continue;
                hasPreferences = true;
                String parValue = filterConfig.getInitParameter(par);
                StringTokenizer stt = new StringTokenizer(parValue,",");
                int nTokens = stt.countTokens();
                String preferenceClass = stt.nextToken();
                String userName = null;
                if ( nTokens > 1 )
                    userName = stt.nextToken();
                addPreference(par,preferenceClass,userName);
            }
        }
        
    }
    
    public static void addPreference(String name, String className, String userName) {
        prefs.put(name,className);
        if ( userName != null )
            prefsUser.put(name,userName);
    }
    public static void addPreference(String name, String className) {
        addPreference(name,className,null);
    }
    
    public void destroy() {
    }
    
    
    static void doPreferences(HttpServletRequest request) {
        loadPreferences(request);
    }
    
    public static String getPreferenceUserName(String preference) {
        return (String)prefsUser.get(preference);
    }
    public static String getPreferenceUserName(String preference, JspContext jspContext) {
        String preferenceUserName = getPreferenceUserName(preference);
        if ( preferenceUserName != null )
            return preferenceUserName;
        return LoginUtils.userName(jspContext);
    }
    
    public static void savePreference(Object preference, String pref, JspContext jspContext) throws JspException {
        String userName = LoginUtils.userName(jspContext);

        HttpSession session = ((HttpServletRequest)((PageContext)jspContext).getRequest()).getSession();
        String thisExperiment = ModeSwitcherFilter.getExperimentFromSession(session, experiment);

        String prefName = getPreferenceUserName(pref);
        if ( prefName != null )
            userName = prefName;
        if ( userName == null || userName.equals("") )
            return;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        XMLEncoder encoder = new XMLEncoder(out);
        encoder.writeObject(preference);
        encoder.close();
        String clob = out.toString();
        
        Connection conn = ConnectionManager.getConnection(preferencesDatabase);
        try {
            String sqlQuery = "select data from userpreferences where preference_name = ? and userid = ? and application = ? and experiment = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sqlQuery);
            preparedStatement.setString(1,pref);
            preparedStatement.setString(2,userName);
            preparedStatement.setString(3,application);
            preparedStatement.setString(4,thisExperiment);
            ResultSet rs = preparedStatement.executeQuery();
            
            String savingQuery = null;
            if ( rs.next() ) {
                savingQuery = "update userpreferences set data = ? where preference_name = ? and userid = ? and application = ? and experiment = ?";
            } else {
                savingQuery = "insert into userpreferences (data, preference_name, userid, application, experiment) values(?,?,?,?,?)";
            }
            rs.close();
            
            PreparedStatement savingStatement = conn.prepareStatement(savingQuery);
            savingStatement.setString(1,clob);
            savingStatement.setString(2,pref);
            savingStatement.setString(3,userName);
            savingStatement.setString(4,application);
            savingStatement.setString(5,thisExperiment);
            savingStatement.execute();
            
        } catch (Exception e) {
            throw new RuntimeException("Problem in Preferences Filter loading preference "+e);
        } finally {
            try {
                conn.close();
            } catch (SQLException sqle) {
                throw new RuntimeException("Problem in Preferences Filter closing connection "+sqle);
            }
        }
    }
    
    static void loadPreferences(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Enumeration preferences = prefs.keys();
        String userName = LoginUtils.userName(request);
        while( preferences.hasMoreElements() ) {
            String pref = (String) preferences.nextElement();
            String storedAs = (String)session.getAttribute(pref+"_LoadedAs");
            String prefUserName = getPreferenceUserName(pref);
            if ( prefUserName == null )
                prefUserName = userName;
            if ( storedAs == null || ! storedAs.equals(prefUserName) ) {
                try {
                    session.setAttribute(pref, loadPreference(pref,request));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                session.setAttribute(pref+"_LoadedAs",prefUserName);
            }
        }
    }
    
    static Object loadPreference(String pref, HttpServletRequest request) throws Exception {
        String prefClass = (String) prefs.get(pref);
        Class c = Class.forName(prefClass);
        
        HttpSession session = request.getSession();
        String userName = LoginUtils.userName(request);
        String thisExperiment = ModeSwitcherFilter.getExperimentFromSession(session, experiment);

        if ( userName == null || userName.equals("") )
            return getDefaultPreference(c);
        else {
            Connection conn = ConnectionManager.getConnection(preferencesDatabase);
            Object obj = null;
            try {
                String sqlQuery = "select data from userpreferences where preference_name = ? and userid = ? and application = ? and experiment = ?";
                PreparedStatement preparedStatement = conn.prepareStatement(sqlQuery);
                preparedStatement.setString(1,pref);
                preparedStatement.setString(2,userName);
                preparedStatement.setString(3,application);
                preparedStatement.setString(4,thisExperiment);
                ResultSet rs = preparedStatement.executeQuery();
                if ( rs.next() ) {
                    Clob clob = rs.getClob("data");
                    XMLDecoder decoder = new XMLDecoder(clob.getAsciiStream());
                    obj = decoder.readObject();
                    decoder.close();
                } else {
                    obj = getDefaultPreference(c);
                }
                rs.close();
            } catch (Exception e) {
                throw new RuntimeException("Problem in Login Filter "+e);
            } finally {
                conn.close();
            }
            return obj;
        }
    }
    
    static Object getDefaultPreference(Class c) throws Exception {
        return c.getConstructor(new Class[0]).newInstance(new Object[0]);
    }
    
    public static String getPreferencesDatabase() {
        return preferencesDatabase;
    }
    
    public static String getApplicationName() {
        return application;
    }
    
}
