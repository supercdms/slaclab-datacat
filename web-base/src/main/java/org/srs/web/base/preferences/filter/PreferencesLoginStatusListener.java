/*
 * PreferencesLoginStatusListener.java
 *
 * Created on June 12, 2007, 9:25 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package org.srs.web.base.preferences.filter;

import java.io.Serializable;
import javax.servlet.http.HttpSession;
import org.srs.web.base.filters.login.LoginStatusChangedEvent;
import org.srs.web.base.filters.login.LoginStatusListener;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public class PreferencesLoginStatusListener implements LoginStatusListener, Serializable {

    private HttpSession session;
    
    PreferencesLoginStatusListener(HttpSession session) {
        this.session = session;
    }

    public void loginStatusChanged(LoginStatusChangedEvent e) {
        e.getRequest().getSession().removeAttribute("preferencesLoaded");
    }

    public HttpSession getSession() {
        return session;
    }
}
