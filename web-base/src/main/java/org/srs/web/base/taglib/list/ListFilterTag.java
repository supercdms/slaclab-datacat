package org.srs.web.base.taglib.list;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author tonyj
 * @version $Id: ListFilterTag.java,v 1.1 2008/01/11 02:28:52 tonyj Exp $
 */

public class ListFilterTag extends SimpleTagSupport
{
   private List input;
   private List output = new ArrayList();
   private boolean accept;
   private String var;

   public void doTag() throws JspException
   {
      try
      {
         JspWriter out = getJspContext().getOut();
         JspFragment f = getJspBody();
         for (Object item : input)
         {
            accept = false;
            getJspContext().setAttribute("item",item);
            if (f != null) f.invoke(out);
            if (accept) output.add(item);
         }
         if (var != null) getJspContext().setAttribute(var,output);
      }
      catch (IOException x)
      {
         throw new JspException("IO error in list filter",x);
      }
   }
   void accept(boolean value)
   {
      accept = value;
   }
   public void setVar(String var)
   {
      this.var = var;
   }
   public void setItems(List list)
   {
      this.input = list;
   }
}
