package org.srs.web.base.taglib.xmltable;

import java.io.IOException;
import java.io.Writer;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author The FreeHEP team @ SLAC.
 *
 */
public class XmlTableColumnTag extends SimpleTagSupport {
    
    private String name = null, title = null, property = null, href = null, paramId = null, paramProperty = null, type = "string", classStr = null, group = null, decorator = null;
    private boolean sortable = false;
    
    
    public void doTag() throws JspException, IOException {
        XmlTableTag tableTag = (XmlTableTag) findAncestorWithClass(this, XmlTableTag.class);
        tableTag.addColumn(this);
    }
    
    public void setProperty(String property) {
        this.property = property;
    }
    
    String getProperty() {
        return property;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    String getTitle() {
        return title;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    String getName() {
        return name;
    }
    
    public void setHref(String href) {
        this.href = href;
    }
    
    String getHref() {
        return href;
    }

    public void setGroup(String group) {
        this.group = group;
    }
    
    String getGroup() {
        return group;
    }

    public void setClassname(String classStr) {
        this.classStr = classStr;
    }
    
    String getClassstr() {
        return classStr;
    }

    public void setParamId(String paramId) {
        this.paramId = paramId;
    }
    
    String getParamId() {
        if ( paramId == null )
            return paramProperty;
        return paramId;
    }

    public void setParamProperty(String paramProperty) {
        this.paramProperty = paramProperty;
    }
    
    String getParamProperty() {
        return paramProperty;
    }
    
    public void setSortable(boolean sortable) {
        this.sortable = sortable;
    }
    
    String getSortable() {
        return String.valueOf(sortable).toString();
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    String getType() {
        return type;
    }
    
    public void setDecorator(String decorator) {
        this.decorator = decorator;
    }
    
    String getDecorator() {
        return decorator;
    }
    

    void invokeBody(Writer writer) throws JspException, IOException {
        getJspBody().invoke(writer);
    }
    
}