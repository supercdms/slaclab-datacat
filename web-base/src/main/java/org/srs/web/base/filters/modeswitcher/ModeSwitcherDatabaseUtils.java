/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.srs.web.base.filters.modeswitcher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.srs.web.base.filters.experiment.ExperimentDatabaseUtils;

/**
 *
 * @author turri
 */
public abstract class ModeSwitcherDatabaseUtils {

    public static void checkModeSwitcherDatabaseTables(Connection c, String experiment) {

        ExperimentDatabaseUtils.checkExperimentDatabaseTables(c, experiment);


        try {
            String applicationModeTableExistsSql = "select count(1) from application_mode";
            PreparedStatement applicationModeTableExistsStatement = c.prepareStatement(applicationModeTableExistsSql);
            ResultSet applicationModeTableExistsResult = null;
            try {
                applicationModeTableExistsResult = applicationModeTableExistsStatement.executeQuery();
            } catch (Exception e) {
                String createApplicationModeTableSql = "CREATE TABLE application_mode ( mode_name VARCHAR(20) NOT NULL, allowed_values VARCHAR(200) NOT NULL, "
                        + " default_value VARCHAR(50) NOT NULL, application VARCHAR(50), experiment VARCHAR(25) )";
                PreparedStatement createApplicationModeTableStatement = c.prepareStatement(createApplicationModeTableSql);
                int createApplicationModeTableStatementResult = createApplicationModeTableStatement.executeUpdate();
                createApplicationModeTableStatement.close();

                String createApplicationVariableTableSql = "CREATE TABLE application_variable ( mode_name VARCHAR(20), mode_value VARCHAR(50), "
                        + " application VARCHAR(50), experiment VARCHAR(25), variable_name VARCHAR(50) NOT NULL, variable_value VARCHAR(250) NOT NULL )";
                PreparedStatement createApplicationVariableTableStatement = c.prepareStatement(createApplicationVariableTableSql);
                int createApplicationVariableTableStatementResult = createApplicationVariableTableStatement.executeUpdate();
                createApplicationVariableTableStatement.close();

            } finally {
                if (applicationModeTableExistsResult != null) {
                    applicationModeTableExistsResult.close();
                }
                applicationModeTableExistsStatement.close();
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
