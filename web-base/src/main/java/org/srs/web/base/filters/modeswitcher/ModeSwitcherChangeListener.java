package org.srs.web.base.filters.modeswitcher;

import javax.servlet.http.HttpSession;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public interface ModeSwitcherChangeListener {
    
    public void modeChanged(ModeSwitcherChangedEvent msce);
    
    public HttpSession getSession();
    
}
