package org.srs.web.base.taglib.xmltable;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.text.Format;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.displaytag.pagination.PaginatedList;
import org.displaytag.properties.SortOrderEnum;
import org.jdom.Content;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author The FreeHEP team @ SLAC
 */
public class XmlTable {
    
    private final int rows;
    private final List list = new ArrayList();
    private List xmlTableColumns = new ArrayList();
    private List xmlTableRows = new ArrayList();
    private String defaultsort = "1", defaultorder = "ascending", decorator = null;
    private static XMLOutputter out = new XMLOutputter();
    private String caption = null;
    
    public static XmlTable createXmlTableFromUrl(String url) {
        
        HttpClient httpClient = new HttpClient();
        
        GetMethod method = new GetMethod(url);
        // Provide custom retry handler is necessary
        method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                new DefaultHttpMethodRetryHandler(3, false));
        Reader r = null;
        try {
            // Execute the method.
            int statusCode = httpClient.executeMethod(method);
            InputStream input = new BufferedInputStream( new ByteArrayInputStream(method.getResponseBody()));
            
            r = new BufferedReader(new InputStreamReader( input ));
//            input = new BufferedInputStream( method.getResponseBodyAsStream() );
        } catch (HttpException e) {
            throw new RuntimeException("Fatal protocol violation:  ",e);
        } catch (IOException e) {
            throw new RuntimeException("Fatal transport error:  ",e);
        } finally {
            // Release the connection.
            method.releaseConnection();
        }
        
        return new XmlTable(r);
    }
    
    
    
    public static XmlTable createXmlTable(String xml) {
        return new XmlTable(xml);
    }
    
    public XmlTable(String xml) {
        this(new StringReader(xml));
    }
    public XmlTable(Reader in) {
        try {
            SAXBuilder builder = new SAXBuilder(false);
            Document doc = builder.build(in);
            Element root = doc.getRootElement();
            
            Element xmltable = findXmlTableElement(root);
            if ( xmltable.getAttributeValue("defaultorder") != null )
                defaultorder = xmltable.getAttributeValue("defaultorder");
            if ( xmltable.getAttributeValue("defaultsort") != null ) 
                defaultsort = xmltable.getAttributeValue("defaultsort");
            if ( xmltable.getAttributeValue("decorator") != null ) 
                decorator = xmltable.getAttributeValue("decorator");
            
            Element caption = xmltable.getChild("caption");
            if ( caption != null ) {
                this.caption = caption.getText();
            }
            
            Element columns = xmltable.getChild("columns");
            List cols = columns.getChildren("column");
            List rs = xmltable.getChild("rows").getChildren("row");
            for ( int i = 0; i < cols.size(); i++ ) {
                Element column = (Element) cols.get(i);
                String sortable = column.getAttributeValue("sortable");
                boolean isSortable = sortable == null ? false : Boolean.valueOf(sortable).booleanValue();
                addColumn( new XmlTableColumn(column.getAttributeValue("name"), column.getAttributeValue("title"),column.getAttributeValue("type"), isSortable, column.getAttributeValue("classstr"), column.getAttributeValue("group"), column.getAttributeValue("decorator") ) );
            }
            
            rows = rs.size();
            for ( int i = 0; i < rows; i++ ) {
                Map map = new LinkedHashMap();
                List cells = ((Element) rs.get(i)).getChildren("cell");
                for (int j=0; j< cols.size(); j++) {
                    String name = getColumn(j).getName();
                    Element cell = (Element) cells.get(j);
                    Object value = getCell(cell,getColumn(j));
                    map.put(name,value);
                }
                xmlTableRows.add(map);
            }
        } catch ( JDOMException jde ) {
            throw new RuntimeException("JDOM Exception ",jde);
        } catch ( IOException io ) {
            throw new RuntimeException("IO Exception ",io);
        }
    }
    
    public String getDecorator() {
        return decorator;
    }
    
    public String getDefaultsort() {
        return defaultsort;
    }

    public String getDefaultorder() {
        return defaultorder;
    }
    
    public String getCaption() {
        return caption;
    }
    
    private XmlTableCell getCell(Element cell, XmlTableColumn col) {
        String value = "";
        String href = cell.getAttributeValue("href");
        if ( href != null )
            value += "<a href=\""+href+"\">";        
        if ( cell.getText() != null )
            value += cell.getText();
        if ( href != null )
            value += "</a>";
        return new XmlTableCell(value, col);
    }
    
    private Element findXmlTableElement(Element root) {
        if ( root.getName().equals("xmltable") )
            return root;
        
        Element table = root.getChild("xmltable");
        if ( table != null )
            return table;
        
        List children = root.getChildren();
        int size = children.size();
        for ( int i = 0; i < size; i++ ) {
            Element child = (Element) children.get(i);
            Element t = findXmlTableElement(child);
            if ( t != null )
                return t;
        }
        throw new IllegalArgumentException("Could not find Element for xmltable");
    }
    
    private void addColumn(XmlTableColumn col) {
        xmlTableColumns.add(col);
    }
    
    private XmlTableColumn getColumn(int i) {
        return (XmlTableColumn) xmlTableColumns.get(i);
    }
    
    public List getColumns() {
        return xmlTableColumns;
    }
    
    public List getRows() {
        return xmlTableRows;
    }
}