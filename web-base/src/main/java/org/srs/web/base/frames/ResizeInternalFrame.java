package org.srs.web.base.frames;

import java.io.IOException;
import java.io.Writer;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * @author The FreeHEP team @ SLAC.
 *
 */
public class ResizeInternalFrame extends SimpleTagSupport {
    
    private String name;
    
    public void doTag() throws JspException, IOException {
        
        JspContext jspContext = getJspContext();
        Writer writer = jspContext.getOut();
        writer.write("\n");
        writer.write("<script type=\"text/javascript\">\n");
        writer.write("        function dyniframesize() {\n");
        writer.write("           var framename=\""+name+"\"\n");
        writer.write("           var document = parent.document;\n");
        writer.write("           if ( document )\n");
        writer.write("               resize(document, framename);\n");
        writer.write("        }\n");
        writer.write("        //Should script hide iframe from browsers that don't support this script (non IE5+/NS6+ browsers. Recommended):\n");
        writer.write("        var iframehide=\"yes\"\n");
        writer.write("        var getFFVersion=navigator.userAgent.substring(navigator.userAgent.indexOf(\"Firefox\")).split(\"/\")[1]\n");
        writer.write("        var FFextraHeight=parseFloat(getFFVersion)>=0.1? 16 : 0 //extra height in px to add to iframe in FireFox 1.0+ browsers\n");
        writer.write("\n");
        writer.write("        function resize(document,framename) {\n");
        writer.write("           var doc = document;\n");
        writer.write("           if (doc.getElementById){ //begin resizing iframe procedure\n");
        writer.write("              var dyniframe = doc.getElementById(framename);\n");
        writer.write("              if (dyniframe && !window.opera){\n");
        writer.write("                 dyniframe.style.display=\"block\"\n");
        writer.write("                 if (dyniframe.contentDocument && dyniframe.contentDocument.body.offsetHeight) //ns6 syntax\n");
        writer.write("                    dyniframe.height = dyniframe.contentDocument.body.offsetHeight+FFextraHeight;\n");
        writer.write("                 else if (dyniframe.Document && dyniframe.Document.body.scrollHeight) //ie5+ syntax\n");
        writer.write("                    dyniframe.height = dyniframe.Document.body.scrollHeight;\n");
        writer.write("              }\n");
        writer.write("           }\n");
        writer.write("           //reveal iframe for lower end browsers? (see var above):\n");
        writer.write("           if ((doc.all || doc.getElementById) && iframehide==\"no\"){\n");
        writer.write("              var tempobj=doc.all? doc.all[framename] : doc.getElementById(framename)\n");
        writer.write("              tempobj.style.display=\"block\"\n");
        writer.write("           }\n");
        writer.write("        }\n");
        writer.write("\n");
        writer.write("        if (window.addEventListener)\n");
        writer.write("           window.addEventListener(\"load\", dyniframesize, false)\n");
        writer.write("        else if (window.attachEvent)\n");
        writer.write("           window.attachEvent(\"onload\", dyniframesize)\n");
        writer.write("        else\n");
        writer.write("           window.onload=dyniframesize\n");
        writer.write("</script>\n");
        writer.write("\n");
    }
    
    public void setName(String name) {
        this.name = name;
    }
}
