package org.srs.web.base.filters.modeswitcher;

import java.util.HashMap;
import javax.servlet.ServletRequest;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public class ModeSwitcherChangedEvent {
    
    private HashMap<String,String> list;
    private ServletRequest request;
    
    public ModeSwitcherChangedEvent(ServletRequest request, HashMap<String,String> list) {
        this.list = list;
        this.request = request;
    }
    
    public HashMap<String,String> getChangedModesList() {
        return list;
    }

    public ServletRequest getRequest() {
        return request;
    }
}
