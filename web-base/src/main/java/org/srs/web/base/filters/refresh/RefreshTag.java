package org.srs.web.base.filters.refresh;

import java.io.IOException;
import java.io.Writer;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import org.srs.web.base.preferences.PreferencesUtils;
import org.srs.web.base.filters.login.LoginFilter;
import org.srs.web.base.filters.login.LoginUtils;

public class RefreshTag extends SimpleTagSupport {
    
    private int refresh = -1;
    private boolean alertOnStop = true;
    
    public void doTag() throws JspException, IOException {
        HttpServletRequest request = (HttpServletRequest) ((PageContext)getJspContext()).getRequest();
        
        if ( RefreshFilter.isRefreshable( request ) ) {
            
            HttpSession session = request.getSession();
            
            RefreshPreferencesBean bean = null;
            if ( RefreshFilter.getPreferencesName() != null )
                bean = (RefreshPreferencesBean) session.getAttribute(RefreshFilter.getPreferencesName());
            if ( refresh < 0 ) {
                if ( bean != null )
                    refresh = bean.getRefreshRate();
                else
                    refresh = RefreshFilter.getRefreshRate();
            }
            
            
            
            Object obj = session.getAttribute(RefreshFilter.refreshRateStr);
            if ( obj != null )
                refresh = ((Integer)obj).intValue();
            
            String refreshRateStr = request.getParameter(RefreshFilter.refreshRateStr);
            
            if ( refreshRateStr != null ) {
                try {
                    int tmp = Integer.valueOf(refreshRateStr).intValue();
                    if ( tmp != refresh && RefreshFilter.checkRefreshRate(tmp) ) {
                        refresh = tmp;
                        session.setAttribute(RefreshFilter.refreshRateStr,refresh);
                        if ( bean != null ) {
                            if ( refresh != bean.getRefreshRate() ) {
                                bean.setRefreshRate(refresh);
                                if ( RefreshFilter.getPreferencesAutoSave() && ! LoginUtils.userName(request).equals("") )
                                    PreferencesUtils.savePreferences(bean, RefreshFilter.getPreferencesName(), getJspContext());
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            
            
            Writer writer = getJspContext().getOut();
            
            writer.write("<form>\n");
            writer.write("<table>\n");
            writer.write("<tr>\n");
            
            
            String refreshStrPar = request.getParameter(RefreshFilter.refreshStr);
            
            boolean refreshIsOn = false;
            if ( request.getParameter((RefreshFilter.refreshStr)) != null )
                refreshIsOn = Boolean.valueOf(request.getParameter(RefreshFilter.refreshStr)).booleanValue();
            if ( session.getAttribute( RefreshFilter.errorMessageStr ) != null )
                refreshIsOn = false;
            
            Object refreshCountObj = session.getAttribute(RefreshFilter.refreshCountStr);
            int refreshInt = 100;
            if ( refreshCountObj != null ) {
                refreshInt = ((Integer)refreshCountObj).intValue();
            }
            
            boolean refreshStopped = request.getParameter(RefreshFilter.stopRefreshStr) != null ;
            boolean refreshExpired = false;
            if ( refreshInt < 0 ) {
                refreshIsOn = false;
                if ( ! refreshStopped )
                    refreshExpired = true;
            }
            
            String name = "Start Refreshing";
            
            
            writer.write("<td>\n");
            if ( refreshIsOn ) {
                name = "Stop Refreshing";
                writer.write("Refreshing page every "+refresh+" secs ("+refreshCountObj+") ");
            } else {
                writer.write("Start refreshing page every ");
                writer.write("<input type=\"text\" name=\"refreshRate\" value=\""+refresh+"\" size=\"4\" />");
                writer.write(" secs ");
            }
            writer.write("</td>\n");
            
            writer.write("<td>\n");
            
            Enumeration e = request.getParameterNames();
            while ( e.hasMoreElements() ) {
                String par = (String) e.nextElement();
                if ( ! par.equals(RefreshFilter.refreshRateStr) && ! par.equals(RefreshFilter.startRefreshStr) && ! par.equals(RefreshFilter.refreshStr) && ! par.equals(RefreshFilter.refreshCountStr) && ! par.equals(RefreshFilter.stopRefreshStr)) {
                    String val = request.getParameter(par);
                    writer.write("<input type=\"hidden\" name=\""+par+"\" value=\""+val+"\" />\n");
                }
            }
            if ( !refreshIsOn ) {
                writer.write("<input type=\"hidden\" name=\""+RefreshFilter.startRefreshStr+"\" value=\"true\" />\n");
                writer.write("<input type=\"hidden\" name=\""+RefreshFilter.refreshStr+"\" value=\"true\" />\n");
            } else {
                writer.write("<input type=\"hidden\" name=\""+RefreshFilter.refreshCountStr+"\" value=\"0\" />\n");
                writer.write("<input type=\"hidden\" name=\""+RefreshFilter.stopRefreshStr+"\" value=\"true\" />\n");
            }
            writer.write("<input type=\"submit\" value=\""+name+"\"/>\n");
            writer.write("</td>\n");
            writer.write("</tr>\n");
            
            if ( session.getAttribute(RefreshFilter.errorMessageStr) != null ) {
                int colspan = refreshIsOn ? 2 : 1;
                writer.write("<tr>\n");
                writer.write("<td colspan=\""+colspan+"\">\n");
                writer.write("<font color=\"red\">"+session.getAttribute(RefreshFilter.errorMessageStr)+"</font>");
                writer.write("</td>\n");
                writer.write("</tr>\n");
            }
            
            if ( refreshExpired && alertOnStop ) {
                int colspan = refreshIsOn ? 2 : 1;
                writer.write("<tr>\n");
                writer.write("<td colspan=\""+colspan+"\">\n");
                writer.write("<font color=\"red\">Refreshing stopped</font>");
                writer.write("</td>\n");
                writer.write("</tr>\n");
//                    writer.write("\n");
//                    writer.write("<script type=\"text/javascript\">\n");
//                    writer.write("   alert(\"Refreshing stopped\");\n");
//                    writer.write("</script>\n");
//                    writer.write("\n");
                
            }
            
            
            writer.write("</table>\n");
            writer.write("</form>\n");
            
        }
    }
    
    public void setRefresh(int refresh) {
        this.refresh = refresh;
    }
    
    public void setAlertOnStop(boolean alertOnStop) {
        this.alertOnStop = alertOnStop;
    }
}