/*
 * PreferencesLoginStatusListener.java
 *
 * Created on June 12, 2007, 9:25 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package org.srs.web.base.preferences.filter;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.srs.web.base.filters.login.LoginUtils;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public class PreferencesSessionListener implements HttpSessionListener {
    
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        LoginUtils.addLoginStatusListener(new PreferencesLoginStatusListener(httpSessionEvent.getSession()) );
    }
    
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
    }
    
}
