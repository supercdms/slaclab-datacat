package org.srs.web.base.preferences;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class ShowHistoryTag extends SimpleTagSupport {
    
    private String name, property, href, target;
    
    public void doTag() throws JspException, IOException {        
        Object preferences = PreferencesUtils.getPreferencesBean(name,getJspContext());    
        Object obj = PreferencesUtils.getPreference(preferences,property);        
        String propertyValue = (String)((PageContext)getJspContext()).getSession().getAttribute(property);
        PreferencesUtils.writeHistory(obj, getJspContext().getOut(),property, href, target, propertyValue);
    }
        
    public void setName(String name) {
        this.name = name;
    }
    
    public void setHref(String href) {
        this.href = href;
    }
    
    public void setTarget(String target) {
        this.target = target;
    }
    
    public void setProperty(String property) {
        this.property = property;
    }    

}