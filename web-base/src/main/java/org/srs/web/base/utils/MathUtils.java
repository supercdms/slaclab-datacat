package org.srs.web.base.utils;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public abstract class MathUtils {
    
    public static int ceil(double errorPercentage) {
        return (int)Math.ceil(errorPercentage);
    }
    
}
