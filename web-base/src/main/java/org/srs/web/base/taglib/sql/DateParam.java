package org.srs.web.base.taglib.sql;

import java.io.IOException;
import java.util.Date;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.jstl.sql.SQLExecutionTag;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * Result tag for use with Call tag
 * @author tonyj
 */
public class DateParam extends SimpleTagSupport
{
   private Date value;
   private String type;
   
   public void doTag() throws JspException, IOException
   {    
      SQLExecutionTag parent = (SQLExecutionTag) findAncestorWithClass(this, SQLExecutionTag.class);
      if (parent == null)
      {
         throw new JspTagException("Invalid use of DateParam tag");
      }
       
      Date parameter = value;
      if ("timestamp".equals(type))
      {
         if (!(value instanceof java.sql.Timestamp)) parameter = new java.sql.Timestamp(value.getTime());
      }
      else if ("time".equals(type))
      {
         if (!(value instanceof java.sql.Time)) parameter = new java.sql.Time(value.getTime());
      }
      else 
      {
         if (!(value instanceof java.sql.Date)) parameter = new java.sql.Date(value.getTime());
      }
      parent.addSQLParameter(parameter);
   }

   public void setValue(Date value)
   {
      this.value = value;
   }
   public void setType(String type)
   {
      this.type = type;
   }
}