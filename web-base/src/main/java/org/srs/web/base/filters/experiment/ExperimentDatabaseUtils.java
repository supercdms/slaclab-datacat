/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.srs.web.base.filters.experiment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author turri
 */
public abstract class ExperimentDatabaseUtils {

    public static void checkExperimentDatabaseTables(Connection c,String experiment) {

            try {
                String experimentTableExistsSql = "select count(1) from experiment";
                PreparedStatement experimentTableExistsStatement = c.prepareStatement(experimentTableExistsSql);
                ResultSet experimentTableExistsResult = null;
                try {
                    experimentTableExistsResult = experimentTableExistsStatement.executeQuery();
                } catch (Exception e) {
                    String createExperimentTableSql = "CREATE TABLE experiment ( experiment VARCHAR(20) NOT NULL, PRIMARY KEY (experiment) )";
                    PreparedStatement createExperimentTableStatement = c.prepareStatement(createExperimentTableSql);
                    int createExperimentTableStatementResult = createExperimentTableStatement.executeUpdate();
                    createExperimentTableStatement.close();
                } finally {
                    if ( experimentTableExistsResult != null )
                        experimentTableExistsResult.close();
                    experimentTableExistsStatement.close();
                }

                String checkIfExperimentExistsSql = "select count(1) from experiment where experiment = ?";
                PreparedStatement checkIfExperimentExistsStatement = c.prepareStatement(checkIfExperimentExistsSql);
                checkIfExperimentExistsStatement.setString(1, experiment);
                ResultSet checkIfExperimentExistsResult = checkIfExperimentExistsStatement.executeQuery();
                checkIfExperimentExistsResult.next();
                boolean exists = checkIfExperimentExistsResult.getInt(1) == 1;
                checkIfExperimentExistsResult.close();
                checkIfExperimentExistsStatement.close();                  
                
                if ( ! exists ) {
                    String addExperimentToTableSql = "insert into experiment values(?)";
                    PreparedStatement addExperimentToTableStatement = c.prepareStatement(addExperimentToTableSql);
                    addExperimentToTableStatement.setString(1, experiment);
                    addExperimentToTableStatement.executeUpdate();
                    addExperimentToTableStatement.close();
                }

            } catch (Exception e) {
                throw new RuntimeException(e);
            } 

    }


}
