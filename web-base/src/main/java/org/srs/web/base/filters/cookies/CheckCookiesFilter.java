package org.srs.web.base.filters.cookies;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author The FreeHEP team @ SLAC.
 *
 */

public class CheckCookiesFilter implements Filter {
    
    private static String checkCookiesParameter = "checkCookiesPar";
    private static String checkCookiesParameterValue = "haveBeenRedirected";
    private static String noCookiesParameterValue = "noCookiesFound";
    private static String checkCookiesCookieName = "checkCookiesTestCookie";
    private static String checkCookiesCookieValue = "testCookie";
    
    /**
     *
     * @param request The servlet request we are processing
     * @param result The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
        HttpServletRequest httpRequest   = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        
        Boolean cookiesChecked = (Boolean) httpRequest.getSession().getAttribute("cookiesChecked");
        
        if ( cookiesChecked!= null && cookiesChecked.booleanValue() ) {
            chain.doFilter(request, response);
            return;
        }
        
        
        String redirectedStr = httpRequest.getParameter(checkCookiesParameter);
        if ( redirectedStr != null && redirectedStr.equals(checkCookiesParameterValue) ) {
            Cookie[] cookies = httpRequest.getCookies();
            
            if ( cookies != null ) {
                for ( int i = 0; i < cookies.length; i++ ) {
                    Cookie c = cookies[i];
                    if ( c.getName().equals(checkCookiesCookieName) && c.getValue().equals(checkCookiesCookieValue) ) {
                        httpRequest.getSession().setAttribute("cookiesChecked", Boolean.TRUE);
                        String requestURL = removeFromQuery(httpRequest);
                        try {
                            httpResponse.sendRedirect(requestURL);
                        } catch (Exception e) {
                            throw new ServletException("Problem with redirect to "+requestURL, e);
                        }
                        return;
                    }
                }
            }
            
            try {
                httpResponse.sendRedirect(noCookiesInQuery(httpRequest));
            } catch (Exception e) {
                throw new ServletException("Problem with redirect to noCookies", e);
            }
            
            return;
            
        } else if ( httpRequest.getRequestURL().toString().contains(noCookiesParameterValue) ) {
            PrintWriter writer = response.getWriter();
            writer.println("<html>");
            writer.println("<head><title>Disabled Cookies</title></head>");
            writer.println("<body>");
            writer.println("<b>Your browser's cookies are disabled.</b>");
            writer.println("<b>This web appliation requires that cookies are enabled.</b>");
            writer.println("<b>Please enable the browser's cookies.</b>");
            writer.println("</body>");
            writer.println("<html>");
            return;
        } else {
            httpResponse.addCookie( new Cookie(checkCookiesCookieName, checkCookiesCookieValue ) );
            
            String requestURL = addToQuery(httpRequest);
            try {
                httpResponse.sendRedirect(requestURL);
            } catch (Exception e) {
                throw new ServletException("Problem with redirect to "+requestURL, e);
            }
            
            return;
        }
    }
    
    private String addToQuery(HttpServletRequest request) {
        String addToQuery = checkCookiesParameter+"="+checkCookiesParameterValue;
        String requestURL = request.getRequestURL().toString()+"?"+addToQuery;
        String requestQuery = request.getQueryString();
        if ( requestQuery != null )
            requestURL += "&"+requestQuery;
        return requestURL;
    }
    
    private String removeFromQuery(HttpServletRequest request) {
        String removeFromQuery = checkCookiesParameter+"="+checkCookiesParameterValue;
        String requestQuery = request.getQueryString().replace(removeFromQuery,"");
        String requestURL = request.getRequestURL().toString();
        if ( requestQuery != null && ! requestQuery.equals("") )
            requestURL += "?"+requestQuery;
        requestURL = requestURL.replace("&&","&");
        requestURL = requestURL.replace("?&","?");
        return requestURL;
    }
    
    private String noCookiesInQuery(HttpServletRequest request) {
        String requestURL = request.getRequestURL().toString();
        if ( ! requestURL.endsWith("/") )
            requestURL += "/";
        requestURL+=noCookiesParameterValue+".jsp";
        return requestURL;
    }
    
    
    /**
     * Destroy method for this filter
     *
     */
    public void destroy() {
    }
    
    
    /**
     * Init method for this filter
     *
     */
    public void init(FilterConfig filterConfig) {
    }
    
}
