package org.srs.web.base.utils;

import javax.servlet.ServletRequest;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public abstract class DisplayTagUtils {
    
    public static String getSortedColumn(ServletRequest request, String tableId) {
        String sortedColumnParameter = new ParamEncoder(tableId).encodeParameterName(TableTagParameters.PARAMETER_SORT); 
        return request.getParameter(sortedColumnParameter);
    }

    public static String getSortOrder(ServletRequest request, String tableId) {
        String sortOrderParameter = new ParamEncoder(tableId).encodeParameterName(TableTagParameters.PARAMETER_ORDER); 
        return request.getParameter(sortOrderParameter);
    }
    
    public static int getPage(ServletRequest request, String tableId) {
        String pageParameter = new ParamEncoder(tableId).encodeParameterName(TableTagParameters.PARAMETER_PAGE); 
        String p = request.getParameter(pageParameter);
        if ( p == null )
            return 0;
        else
            return Integer.valueOf(p).intValue();
    }
    
    public static boolean isExporting(ServletRequest request, String tableId) {
        String isExportingParamter = new ParamEncoder(tableId).encodeParameterName(TableTagParameters.PARAMETER_EXPORTTYPE);
        return request.getParameter(isExportingParamter) != null;
    }
    
}
