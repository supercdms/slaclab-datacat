package org.srs.web.base.filters.cache;

import java.io.IOException;
import java.util.Date;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author The FreeHEP team @ SLAC.
 *
 */

public class CacheFilter implements Filter {
    
    private String validationParameter = null;
    private int maxage = 86400;
    
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
                
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        
        String ifModified = request.getHeader("If-Modified-Since");
        if ( ifModified != null ) {
            if ( validationParameter == null )
                response.setStatus(response.SC_NOT_MODIFIED);
            else {
                String lastModified = getLastModified(request);
                if ( ifModified.equals(lastModified) )
                    response.setStatus(response.SC_NOT_MODIFIED);
                else
                    addCacheToResponse(response, lastModified);
            }
            
        } else {
            addCacheToResponse(response, getLastModified(request));
        }
        filterChain.doFilter(servletRequest,servletResponse);
    }
    
    private void addCacheToResponse(HttpServletResponse response, String lastModified) {
        String cacheControl = "max-age="+maxage;
        if ( validationParameter != null )
            cacheControl += ", no-cache";
        response.setHeader("Cache-Control",cacheControl);
        response.setHeader("Last-Modified",lastModified);
    }
    
    private String getLastModified(HttpServletRequest request) {
        String lastModified = null;
        if ( validationParameter != null ) {
            Object validation = request.getSession().getAttribute(validationParameter);
            if ( validation != null )
                lastModified = (String)validation;
        }
        if ( lastModified == null ) {
            Date d = new Date(System.currentTimeMillis());
            lastModified = d.toString();
        }
        return lastModified;
    }
    
    
    public void init(FilterConfig filterConfig) throws ServletException {
        String maxageStr = filterConfig.getInitParameter("maxage");
        if ( maxageStr != null ) {
            try {
                maxage = Integer.valueOf(maxageStr).intValue();
            } catch (Exception e) {
                
            }
        }
        
        validationParameter = filterConfig.getInitParameter("validation");
    }
    
    public void destroy() {
    }
    
}
