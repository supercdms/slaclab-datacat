package org.srs.web.base.taglib.xmltable;

import java.io.IOException;
import java.io.Writer;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author The FreeHEP team @ SLAC.
 *
 */
public class XmlTableCaptionTag extends SimpleTagSupport {
    
    public void doTag() throws JspException, IOException {
        XmlTableTag tableTag = (XmlTableTag) findAncestorWithClass(this, XmlTableTag.class);
        tableTag.setCaption(this);
    }
    
    void invokeBody(Writer writer) throws JspException, IOException {
        getJspBody().invoke(writer);
    }
    
}