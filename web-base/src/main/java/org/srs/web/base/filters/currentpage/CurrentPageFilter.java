package org.srs.web.base.filters.currentpage;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.StringTokenizer;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.srs.web.base.preferences.filter.PreferencesFilter;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public class CurrentPageFilter  implements Filter {
    
    private String defaultPage = null;
    private ArrayList pages = new ArrayList();
    private String currentPageAttribute = "currentPage";
    
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        
        String currentPage = getCurrentPage(request);
        
        if ( currentPage != null )
            session.setAttribute(currentPageAttribute,currentPage);
        
        if ( filterChain != null )
            filterChain.doFilter(servletRequest, servletResponse);
    }
        
    public void addCurrentPage(String page) {
        pages.add(page);
    }
    
    public boolean belongsToCurrentPagesList(String page) {
        return pages.contains(page);
    }
    
    public String getCurrentPage(HttpServletRequest request) {
        String contextPath = request.getContextPath();
        String requestURI = request.getRequestURI();
        String tmp = requestURI.substring(contextPath.length()+1);
        if ( ! tmp.equals("") ) {
            if ( belongsToCurrentPagesList(tmp) )
                return tmp;
            else
                return null;
        }
        return defaultPage;
    }
    
    public void init(FilterConfig filterConfig) throws ServletException {
        
        String param = filterConfig.getInitParameter("defaultPage");
        if ( param != null )
            defaultPage = param;
        
        param = filterConfig.getInitParameter("pages");
        if ( param != null ) {
            StringTokenizer stt = new StringTokenizer(param,",");
            while(stt.hasMoreTokens())
                addCurrentPage(stt.nextToken());
        }
        
    }
    
    
    public void destroy() {
        // Nothing to do
    }
    
}