package org.srs.web.base.filters.multipart;

/**
 *
 * @author The FreeHEP team @ SLAC
 */
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;

public class MultipartFilter implements Filter {

    public MultipartFilter() {
    }
    
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if (FileUpload.isMultipartContent(request)) {
            filterChain.doFilter(new MultiPartWrapper(request), servletResponse);
        } else {
            filterChain.doFilter(servletRequest,servletResponse);
        }
    }
    
    public void init(FilterConfig filterConfig) throws ServletException {
        // Nothing to do
    }
    
    public void destroy() {
        // Nothing to do
    }
    
    private class MultiPartWrapper extends HttpServletRequestWrapper {
        private final Map<String,String[]> params;
        private Map<String,FileItem> fileItems = new Hashtable<String,FileItem>();

        MultiPartWrapper(HttpServletRequest request) throws ServletException {
            super(request);
            
            try {
                Map<String,String[]> p = new HashMap<>();
                // Create a new file upload handler
                DiskFileUpload upload = new DiskFileUpload();
                
                // Parse the request
                List<FileItem> items = upload.parseRequest(request);
                for (FileItem item : items) {
                    p.put(item.getFieldName(), new String[] {item.getString()});
                    fileItems.put(item.getFieldName(), item);
                }
                params = Collections.unmodifiableMap(p);
            } catch (FileUploadException x) {
                throw new ServletException("Error uploading file",x);
            }
            request.setAttribute("fileItems", fileItems);
            request.getSession().setAttribute("fileItems", fileItems);
        }
        
        public String[] getParameterValues(String name) {
            if ( params.get(name) == null )
                return null;
            return params.get(name);
        }
        
        public String getParameter(String name) {
            return params.get(name) == null ? null : params.get(name)[0];
        }
        
        public Enumeration getParameterNames() {
            return Collections.enumeration(params.keySet());
        }
        
        public Map getParameterMap() {
            return params;
        }
    }
}