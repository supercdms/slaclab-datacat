package org.srs.web.base.preferences;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import org.srs.web.base.preferences.filter.PreferencesFilter;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public abstract class PreferencesUtils {
    
    public static Object[] emptyObjectArg = new Object[] {};
    public static Class[]  emptyClassArg  = new Class[] {};
    
    public static void savePreferences(Object preferences, String prefsName, JspContext context) throws JspException {
        PreferencesFilter.savePreference(preferences,prefsName,context);
    }
    
    public static Method getSetterMethod(Object preferences, String name) throws JspException {
        String setterMethodName = "set"+name.substring(0,1).toUpperCase()+name.substring(1);
        try {
            Method[] methods = preferences.getClass().getMethods();
            Method setterMethod = null;
            for ( int i = 0; i < methods.length; i++ ) {
                Method m = methods[i];
                if ( m.getName().equals(setterMethodName) ) {
                    setterMethod = m;
                    Class[] setterMethodParameters = setterMethod.getParameterTypes();
                    if ( setterMethodParameters.length != 1 )
                        throw new JspException("Setter method for property "+name+" must take one argument. This one takes "+setterMethodParameters.length);
                    return setterMethod;
                }
            }
        } catch (Exception e) {
            throw new JspException(e);
        }
        throw new JspException("Could not find setter method for property "+name);
    }
    
    public static void setPreference(Object preferences, Method setterMethod, Object value) throws JspException {
        Class[] setterMethodParameters = setterMethod.getParameterTypes();
        Class setterMethodParameterClass = setterMethodParameters[0];
        try {
            setterMethod.invoke(preferences, new Object[] { getTypedObject(value,setterMethodParameterClass) });
        } catch (Exception e) {
            throw new JspException(e);
        }
    }
    
    public static Object getPreferencesBean(String name, JspContext context) throws JspException {
        Object preferences = context.getAttribute(name,PageContext.SESSION_SCOPE);
        if ( preferences == null )
            throw new JspException("Preference bean "+name+" does not exist in this session.");
        return preferences;
    }
    
    public static Object getPreference(Object preferences, String property) throws JspException {
        String getterMethodName = "get"+property.substring(0,1).toUpperCase()+property.substring(1);
        Object value = null;
        try {
            Method getterMethod = preferences.getClass().getMethod(getterMethodName, emptyClassArg);
            value = getterMethod.invoke(preferences, emptyObjectArg);
        } catch (Exception e) {
            throw new JspException(e);
        }
        return value;
    }
    
    public static boolean hasHistory(String property, Object preferences) {
        Object history = getPreferenceHistory(property, preferences);
        if ( history == null )
            return false;
        return getHistorySize(history) > 0;
    }
    
    public static Object getPreferenceHistory(String property, Object preferences) {
        Object history = null;
        String hasHistoryMethodName = "get"+property.substring(0,1).toUpperCase()+property.substring(1);
        try {
            Method historyMethod = preferences.getClass().getMethod(hasHistoryMethodName, emptyClassArg );
            history = historyMethod.invoke(preferences, emptyObjectArg);
        } catch (Exception e) {}
        return history;
    }
    
    public static String getHistoryValue( Object history, String key ) {
        if ( history instanceof List )
            return key;
        else return (String)((Map)history).get(key);
    }
    
    public static void writeHistory(Object history, Writer writer, String property, String href) throws IOException {
        writeHistory(history,writer,property,href,null);
    }
    public static void writeHistory(Object history, Writer writer, String property, String href, String target) throws IOException {
        writeHistory(history,writer,property,href,target,null);        
    }
    public static void writeHistory(Object history, Writer writer, String property, String href, String target,String propertyValue) throws IOException {
        Iterator iter = getHistoryIterator(history);
        int size = getHistorySize(history);
        int count = 0;
        while(iter.hasNext()) {
            String key = (String)iter.next();
            String value = getHistoryValue(history,key);
            if ( value != null ) {
                boolean isHref = value.startsWith("http");
                if ( history instanceof List )
                    value = "?"+property+"="+value;
                else
                    value += "&"+property+"="+key;
                if ( href != null )
                    value = href+value;
                String targetStr = "";
                if ( target != null )
                    targetStr = "target=\""+target+"\"";
                if ( propertyValue != null && propertyValue.equals(key) )
                    writer.write("<b>"+key+"</b>");
                else
                    writer.write("<a href=\""+value+"\" "+targetStr+">"+key+"</a>");
                
                if ( count < size - 1)
                    writer.write(",");
            }
            count++;
        }
    }
    
    public static void removeFromHistory(Object history, String key) {
        if ( history instanceof List )
            ((List) history).remove(key);
        else
            ((Map) history).remove(key);
    }
    
    public static Iterator getHistoryIterator( Object history ) {
        if ( history instanceof List )
            return ((List)history).iterator();
        return ((Map)history).keySet().iterator();
    }
    
    
    public static int getHistorySize( Object history ) {
        if ( history instanceof List )
            return ((List)history).size();
        else return ((Map)history).keySet().size();
    }
    
    
    public static Object getTypedObject(Object input, Class c) throws JspException {
        if ( c.isAssignableFrom(input.getClass() ) )
            return input;
        if ( input instanceof String ) {
            String inputStr = (String) input;
            if ( c == String.class )
                return inputStr;
            else if ( c == Integer.TYPE )
                return Integer.valueOf(inputStr).intValue();
            else if ( c == Double.TYPE )
                return Double.valueOf(inputStr).doubleValue();
            else if ( c == Float.TYPE )
                return Float.valueOf(inputStr).floatValue();
        }
        throw new JspException("Class "+c+" is not currently supported. Please contact the developers for support.");
    }
    
    
}
