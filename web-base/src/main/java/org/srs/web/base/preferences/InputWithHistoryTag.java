package org.srs.web.base.preferences;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class InputWithHistoryTag extends SimpleTagSupport {
    
    private String name, property, href, value;
    private int size = 5;
    
    public void doTag() throws JspException, IOException {        
        Object preferences = PreferencesUtils.getPreferencesBean(name,getJspContext());    
        List list = (List)PreferencesUtils.getPreference(preferences,property);

        HttpServletRequest request = (HttpServletRequest)((PageContext)getJspContext()).getRequest();
        String inputValue = request.getParameter(property);
        if ( inputValue != null ) {
            request.getSession().setAttribute(property,inputValue);
            if ( ! list.contains(inputValue) ) {
                list.add(inputValue);
                PreferencesUtils.savePreferences(preferences,name,getJspContext());
            }
        }
        
        Object obj = request.getSession().getAttribute(property);
        if ( obj != null )
            value = (String)obj;
        
        Writer writer = getJspContext().getOut();
        
        writer.write("<table>\n");
        writer.write("<tr>\n");
        writer.write("<td>\n");
        writer.write("<input name=\""+property+"\" value=\""+value+"\" ");
        if ( size > 0 )
            writer.write(" size = \""+size+"\" ");
        writer.write("/> \n");
        writer.write("</td>\n");
        writer.write("</tr>\n");
        writer.write("<tr>\n");
        writer.write("<td>\n");
        PreferencesUtils.writeHistory(list, writer,property, href);
        writer.write("</td>\n");
        writer.write("</tr>\n");
        writer.write("</table>\n");
        
    }
    
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setHref(String href) {
        this.href = href;
    }
    
    public void setProperty(String property) {
        this.property = property;
    }    

    public void setValue(String value) {
        this.value = value;
    }    
    
    public void setSize(int size) {
        this.size = size;
    }    
    
}