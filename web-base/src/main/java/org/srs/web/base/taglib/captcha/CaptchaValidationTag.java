package org.srs.web.base.taglib.captcha;

import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Tag to validate Captcha response
 *
 * @author turri
 */
public class CaptchaValidationTag extends SimpleTagSupport {

    private volatile String validationUrl = "https://www.google.com/recaptcha/api/siteverify";

    private String secret;
    private String var;

    public void doTag() throws JspException, IOException {
        
        ServletRequest request = ((PageContext) getJspContext()).getRequest();
        String captchaResponse = request.getParameter("g-recaptcha-response");
        
        boolean valid = false;

        if (captchaResponse != null) {

            Client client = null;

            try {
                client = ClientBuilder.newClient();
                WebTarget serv = client.target(validationUrl);
                try (Response response = serv.queryParam("secret", secret).queryParam("response", captchaResponse).request(MediaType.APPLICATION_JSON).header(HttpHeaders.CONTENT_LENGTH, 0).post(Entity.json(""))) {                    

                    String stringResponse = response.readEntity(String.class);
                    if (response.getStatus() != 200) {
                        throw new RuntimeException("Failed to validate captcha " + response.getStatus() + " " + stringResponse);
                    }
                    
                    JsonNode jsonNode = new ObjectMapper().readTree(stringResponse);
                    valid = jsonNode.get("success").asBoolean();
                }
            } finally {
                if (client != null) {
                    client.close();
                }
            }

        }
        getJspContext().setAttribute(var, valid, PageContext.PAGE_SCOPE);
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public void setVar(String var) {
        this.var = var;
    }
}
