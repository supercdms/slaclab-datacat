/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.srs.web.base.filters.modeswitcher;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 *
 * @author turri
 */
public class Mode implements Serializable {

    private final String name, defaultValue, allowedValuesStr, experiment, application;
    private String environment = "database", value;
    private final List<String> allowedValues = new ArrayList<>();


    Mode(String name, String defaultValue, String allowedValuesStr, String experiment, String application) {
        this.name = name;
        this.defaultValue = defaultValue;
        this.value = defaultValue;
        this.allowedValuesStr = allowedValuesStr;
        StringTokenizer stt = new StringTokenizer(allowedValuesStr,",");
        while( stt.hasMoreTokens() )
            allowedValues.add(stt.nextToken());
        this.application = application;
        this.experiment = experiment;
    }
    
    public String getName() {
        return name;
    }
    public String getDefaultValue() {
        return defaultValue;
    }
    public String getValue() {
        return value;
    }
    public String getAllowedValues() {
        return allowedValuesStr;
    }
    public String getExperiment() {
        return experiment;
    }
    public String getApplication() {
        return application;
    }
    public List<String> getAllowedValuesList() {
        return allowedValues;
    }
    public String getEnvironment() {
        return environment;
    }

    public boolean isAllowedValue(String value) {
        return getAllowedValuesList().contains(value);
    }
    
    public void setValue(String value, String environment) {
        if ( isAllowedValue(value) ) {
            this.value = value;
            this.environment = environment;
        }
        else
            throw new IllegalArgumentException("Cannot set mode "+getName()+" to value "+value+". It is not one of the allowed values ("+getAllowedValues()+")");
    }

    public Mode clone() {
        return new Mode( getName(), getDefaultValue(), allowedValuesStr, getExperiment(), getApplication() );
    }
}
