package org.srs.web.base.preferences;

import java.io.IOException;
import java.lang.reflect.Method;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class SetPreferenceTag extends SimpleTagSupport {
    
    private String name, property;
    private Object value;
    
    public void doTag() throws JspException, IOException {        
        Object preferences = PreferencesUtils.getPreferencesBean(name, getJspContext() );
        Method setterMethod = PreferencesUtils.getSetterMethod(preferences,property);
        PreferencesUtils.setPreference(preferences,setterMethod,value);
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setProperty(String property) {
        this.property = property;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
}