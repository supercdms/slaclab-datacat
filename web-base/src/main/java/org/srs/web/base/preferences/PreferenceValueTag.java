package org.srs.web.base.preferences;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class PreferenceValueTag extends SimpleTagSupport {
    
    private Object obj;
    private String text = null;
    
    public void doTag() throws JspException, IOException {               
        PreferenceTag preferenceTag = (PreferenceTag)findAncestorWithClass(this,PreferenceTag.class);
        preferenceTag.addToValues(obj,text);
    }
    
    
    public void setValue(Object obj) {
        this.obj = obj;
    }
    
    public void setText(String text) {
        this.text = text;
    }
}