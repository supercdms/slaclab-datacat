
package org.srs.web.base.filters.login;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * Request wrapper for POST/REDIRECT/GET requests
 * @author bvan
 */
public class FlashRequest extends HttpServletRequestWrapper {
    private Map<String, String[]> params;
    
    public FlashRequest(HttpServletRequest request){
        super(request);
    }
    
    public FlashRequest(HttpServletRequest request, Map<String, String[]> params){
        super(request);
        this.params = params;
    }

    @Override
    public Map getParameterMap(){
        return params;
    }

    @Override
    public String getParameter(String name){
        return params.get(name) != null ? params.get(name)[0] : null;
    }

    @Override
    public Enumeration getParameterNames(){
        return new Enumeration<String>() {
            Iterator<String> iter = params.keySet().iterator();

            @Override
            public boolean hasMoreElements(){
                return iter.hasNext();
            }

            @Override
            public String nextElement(){
                return iter.next();
            }
        };
    }

    @Override
    public String[] getParameterValues(String name){
        return params.get(name);
    }

}
