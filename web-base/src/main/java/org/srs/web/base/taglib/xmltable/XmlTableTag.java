package org.srs.web.base.taglib.xmltable;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.jstl.sql.Result;
import org.apache.commons.collections.BeanMap;
import org.srs.web.base.taglib.pagination.PaginatedResultSet;

/**
 *
 * @author The FreeHEP team @ SLAC.
 *
 */
public class XmlTableTag extends SimpleTagSupport {
    
    private Object object;
    private ArrayList list = new ArrayList();
    private String id = null, var = null, defaultsort = null, defaultorder = null, decorator = null;
    private XmlTableCaptionTag caption = null;
    
    public void doTag() throws JspException, IOException {
        
        Writer writer;
        if ( var == null )
            writer = getJspContext().getOut();
        else
            writer = new StringWriter();
         
        // Evaluate any nested tags.
        JspFragment jspBody = getJspBody();
        if (jspBody != null) {
            jspBody.invoke(writer);
        }
        
        
        int rows = -1;
        Map[] maps = null;
        
        if ( object instanceof Result ) {
            Result result = (Result) object;
            rows = result.getRowCount();
            maps = result.getRows();
        } else if ( object instanceof PaginatedResultSet ) {
            PaginatedResultSet result = (PaginatedResultSet) object;
            rows = result.getList().size();
            List list = result.getList();
            maps = new Map[list.size()];
            for ( int i = 0; i < list.size(); i++ )
                maps[i] = (Map) list.get(i);
        } else if ( object instanceof List ) {
            List l = (List) object;
            rows = l.size();
            maps = new Map[rows];
            for ( int i = 0; i < rows; i++ ) 
                maps[i] = new BeanMap(l.get(i));
        }
        else
            throw new JspException("Object "+object.getClass()+" not supported.");
                
        writer.write("<xmltable ");
        if ( defaultsort != null )
            writer.write("defaultsort=\""+defaultsort+"\" ");
        if ( defaultorder != null )
            writer.write("defaultorder=\""+defaultorder+"\" ");
        if ( decorator != null )
            writer.write("decorator=\""+decorator+"\" ");
        writer.write(">\n");
        
        
        if ( this.caption != null ) {
            writer.write("<caption>");
            writer.write("<![CDATA[");
            caption.invokeBody(writer);
            writer.write("]]>");
            writer.write("</caption>");            
        }
        
        writer.write("<columns>\n");
        for ( int i = 0; i < list.size(); i++) {
            XmlTableColumnTag col = (XmlTableColumnTag) list.get(i);
            writer.write("<column name=\""+col.getName()+"\" type=\""+col.getType().toLowerCase()+"\" sortable=\""+col.getSortable()+"\"  property=\""+col.getProperty()+"\" ");
            if (col.getTitle() != null)
                writer.write("title=\""+col.getTitle()+"\" ");
            if (col.getClassstr() != null)
                writer.write("classstr=\""+col.getClassstr()+"\" ");
            if (col.getGroup() != null)
                writer.write("group=\""+col.getGroup()+"\" ");
            writer.write("/>");            
        }
        writer.write("</columns>\n");
        writer.write("<rows>\n");
        
        for ( int i = 0; i < rows; i++ ) {
            writer.write("<row>\n");
            Map map = maps[i];
            getJspContext().setAttribute(id,map);
            for ( int j = 0; j < list.size(); j++) {
                XmlTableColumnTag col = (XmlTableColumnTag) list.get(j);
                writer.write("<cell");
                if ( col.getHref() != null ) {
                    writer.write(" href=\""+col.getHref());
                    if ( col.getParamId() != null )                    
                        writer.write("?"+col.getParamId()+"="+map.get(col.getParamProperty())+"\"");                
                }
                writer.write(">");

                String property = col.getProperty();
                if ( property != null ) {
                    if ( map.get(property) != null )
                        writer.write(map.get(property).toString());
                    else writer.write("");
                }
                else {
                    writer.write("<![CDATA[");
                    col.invokeBody(writer);
                    writer.write("]]>");
                }
                writer.write("</cell>\n");
            }
            writer.write("</row>\n");
        }
        
        
        writer.write("</rows>\n");
        writer.write("</xmltable>\n");
        
        writer.flush();
        
        if ( var != null ) {
            String output = writer.toString();
            getJspContext().setAttribute(var,output);
        }
        
    }
    
    void addColumn(XmlTableColumnTag column) {
        list.add(column);
    }
    
    void setCaption(XmlTableCaptionTag caption) {
        this.caption = caption;
    }

    public void setResult(Object object) {
        this.object = object;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public void setVar(String var) {
        this.var = var;
    }
    
    public void setDefaultsort(String defaultsort) {
        this.defaultsort = defaultsort;
    }
    
    public void setDefaultorder(String defaultorder) {
        this.defaultorder = defaultorder;
    }
    public void setDecorator(String decorator) {
        this.decorator = decorator;
    }
}