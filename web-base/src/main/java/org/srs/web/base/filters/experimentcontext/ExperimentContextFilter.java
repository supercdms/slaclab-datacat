/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.srs.web.base.filters.experimentcontext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.srs.web.base.filters.modeswitcher.Mode;
import org.srs.web.base.filters.modeswitcher.ModeSwitcherFilter;

/**
 *
 * @author turri
 */
public class ExperimentContextFilter implements Filter {

    private static boolean debug = false;
    private ArrayList<Pattern> skipPagesList = new ArrayList<Pattern>();


    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {


        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        String servletPath = getServletPath(httpRequest);

        if ( httpRequest.getParameter("debugExperimentContextFilter") != null )
            debug = httpRequest.getParameter("debugExperimentContextFilter").equals("true");

        if ( skipPagesList.size() > 0 ) {
            for ( int i = 0; i < skipPagesList.size(); i++ ) {
                if ( skipPagesList.get(i).matcher(servletPath).matches() ) {
                    if (debug)
                        System.out.println("**** Skipping context "+servletPath);
                    chain.doFilter(request, response);
                    return;
                }
            }

        }

        if ( debug )
            System.out.println("*** Starting with servletPath: "+servletPath);

        String parameterExp = httpRequest.getParameter("experiment");
        boolean hasParameterExp = parameterExp != null;

        String sessionExp = null;
        HttpSession session = httpRequest.getSession(false);
        if (session != null) {
            Mode m = ModeSwitcherFilter.getMode(session, "experiment");
            if (m != null) {
                sessionExp = m.getValue();
            }
        }
        boolean hasSessionExp = sessionExp != null;


        String servletExp = null;
        if (servletPath.contains("/exp/")) {
            String tmpServletPath = servletPath.replace("/exp/", "");
            if (tmpServletPath.contains("/")) {
                servletExp = tmpServletPath.substring(0, tmpServletPath.indexOf("/"));
            } else {
                servletExp = tmpServletPath;
            }
        }
        boolean hasServletPathExp = servletExp != null;
        
        if ( debug )
            System.out.println("**** Found experiment? "+hasServletPathExp+" "+servletExp);

        //See if servletPath is at the root level. If so, redirect to /
        servletPath = servletPath.replace("/exp/" + servletExp, "");
        if (servletPath.equals("") || servletPath.equals("/")) {
            httpResponse.sendRedirect("/"+httpRequest.getContextPath());
            if (servletExp != null) {
                if (session == null) {
                    session = httpRequest.getSession();
                }
                setExperiment(session, servletExp);
            }
            return;
        }

        if ( debug ) {
            System.out.println("***** In Experiment Context Filter "+hasParameterExp+" "+hasSessionExp+" "+hasServletPathExp+" "+servletPath);
        }


        //If there is no experiment in the servletPath, then it has never been redirected.
        //Redirection must happen if the experiment is defined either as a parameter or in the session
        if (!hasServletPathExp) {
            if (hasParameterExp) {
                doRedirect(servletPath,httpRequest, httpResponse, parameterExp);
                return;
            } else if (hasSessionExp) {
                doRedirect(servletPath,httpRequest, httpResponse, sessionExp);
                return;
            }
        } //If the experiment is defined in the servletPath then redirection must happen only if
        //the experiment defined as a parameter is different
        else {
            if (hasParameterExp && !servletExp.equals(parameterExp)) {
                doRedirect(servletPath,httpRequest, httpResponse, parameterExp);
                return;
            }
            //If the experiment is defined in the servletPath but NOT in the session, then it must be defined.
            if (!hasSessionExp || (hasSessionExp && !sessionExp.equals(servletExp))) {
                if ( session == null ) {
                    session = httpRequest.getSession(true);
                }
                doRedirect(servletPath,httpRequest, httpResponse, servletExp);
                setExperiment(session, servletExp);
                return;
            }
        }




        chain.doFilter(request, response);

    }

    public static void setExperiment(HttpSession session, String experiment) {
        ModeSwitcherFilter.getMode(session, "experiment").setValue(experiment, "user");
//        ModeSwitcherFilter.getVariables(session).put("experiment", experiment);
    }

    public static String getServletPath(HttpServletRequest httpRequest) {
        String requestURL = httpRequest.getRequestURL().toString();
        String servletPath = null;
        if (requestURL.contains("/exp/")) {
            servletPath = requestURL.substring(requestURL.indexOf("/exp/"));
        } else {
            servletPath = httpRequest.getServletPath();
        }
        return servletPath;
    }

    public static void doRedirect(String servletPath, HttpServletRequest request, HttpServletResponse response, String redirectExp) throws IOException {
        String forwardUrl = getExperimentContextForward(servletPath, request, redirectExp);
        if ( debug )
            System.out.println("***** Sending redirect to "+forwardUrl);
        response.sendRedirect(forwardUrl);
    }

    public static String getExperimentContextForward(String servletPath, HttpServletRequest request, String redirectExp) {
        String here = request.getRequestURL().toString();
        String delim = "?";
        if (here.endsWith("?") || here.endsWith("&")) {
            delim = "";
        }
        String requestQuery = request.getQueryString();
        if (requestQuery == null) {
            requestQuery = "";
        }
	//        if (requestQuery == null || requestQuery.equals("")) {
	//            requestQuery = "experiment=" + redirectExp;
	//        }

        if (requestQuery.contains("experiment")) {
            requestQuery = removeString(requestQuery, "experiment=" + request.getParameter("experiment"));
        }
	//        if (!requestQuery.contains("experiment")) {
	//            requestQuery += "&experiment=" + redirectExp;
	//        }

        String servletLocation = "exp/" + redirectExp;

        int lastIndex = servletPath.indexOf("/");
        String newServletPath = null;
        if (lastIndex >= 0) {
            newServletPath = servletLocation + servletPath.substring(lastIndex);
        } else {
            newServletPath = servletLocation + "/";
        }

        if ( debug ) {
            System.out.println("Building redirect "+servletPath+" "+newServletPath+" "+here+" "+redirectExp+" "+servletPath.substring(lastIndex));
        }
        if (here.contains(servletPath)) {
            if (!here.contains(newServletPath)) {
                here = here.replace(servletPath, "/" + newServletPath);
            }
        } else {
            here += newServletPath;
        }

        here += delim + requestQuery;

        if (here.endsWith("&") || here.endsWith("?")) {
            here = here.substring(0, here.length() - 1);
        }

        return here;

    }

    private static String removeString(String input, String remove) {
        int where = input.indexOf(remove);
        if (where < 0) {
            return input;
        }
        int end = where + remove.length();
        if (end < input.length()) {
            end++;
        }
        input = input.substring(0, where) + input.substring(end);
        return input;
    }

    /**
     * Destroy method for this filter
     *
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     *
     */
    public void init(FilterConfig filterConfig) {
        String skipPages = filterConfig.getInitParameter("skipPages");
        if ( skipPages != null ) {
            StringTokenizer stt = new StringTokenizer(skipPages,",");
            while(stt.hasMoreTokens()) {
                String txt = stt.nextToken();
                if ( ! txt.contains("*") ) {
                    txt = ".*"+txt;
                } else {
                    txt = txt.replace("*", ".*");
                }
                skipPagesList.add(Pattern.compile(txt));
            }

        }
    }
}
