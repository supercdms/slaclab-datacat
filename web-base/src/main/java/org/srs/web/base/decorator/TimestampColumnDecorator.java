package org.srs.web.base.decorator;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import javax.servlet.jsp.PageContext;
import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.properties.MediaTypeEnum;
/**
 *
 * @author tonyj
 */
public class TimestampColumnDecorator implements DisplaytagColumnDecorator
{
   private SimpleDateFormat format;
   public TimestampColumnDecorator()
   {
      this("dd-MMM-yyyy HH:mm:ss",Locale.US,TimeZone.getTimeZone("UTC"));
   }
   
   public TimestampColumnDecorator(String pattern, Locale locale, TimeZone tz)
   {
      format = new SimpleDateFormat(pattern,locale);
      format.setTimeZone(tz);
   }
   
   public Object decorate(Object columnValue, PageContext pageContext, MediaTypeEnum media)
   {
      if (columnValue == null) return null;
      try
      {
         synchronized (format)
         {
            return format.format(columnValue);
         }
      }
      catch (IllegalArgumentException x)
      {
         return columnValue;
      }
   }

   public void setTimezone(String timezone) {
       format.setTimeZone(TimeZone.getTimeZone(timezone));
   }

   public void setFormat(String pattern) {
       format.applyPattern(pattern);
   }

}
