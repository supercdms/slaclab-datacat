package org.srs.web.base.filters.login;


import java.io.IOException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import org.apache.commons.net.util.SubnetUtils;
import org.jasig.cas.client.validation.Assertion;
import org.jasig.cas.client.validation.Cas20ProxyTicketValidator;
import org.jasig.cas.client.validation.TicketValidationException;
import org.srs.web.base.db.ConnectionManager;
import org.srs.web.base.filters.modeswitcher.ModeSwitcherFilter;
import org.srs.web.base.utils.SrsWebUtils;


/**
 *
 * @author The FreeHEP team
 * @ SLAC.
 *
 */
public class LoginFilter implements Filter {
    
    private static final Logger logger = Logger.getLogger(LoginFilter.class.getName());

    /**
     * Login staus flags:
     *
     *  * true : sent to cas for login unless the username is valid * false:
     * sent to cas for logout * maybe: * renew: sent to cas to login and change
     * user * null or othewise: sent to gateway to check status; if logged in
     * the ticket is sent back, otherwise stay logged out
     *
     * Session variables: * userName stores the value of the login username
     *
     * Cookies: * srs.UserName.Cookie stores the value of the login username
     *
     * The value of the Cookie and session username are compared to see what
     * happened in other sessions compared to the current one. The value of the
     * session username should match the Cookies one. The following actions
     * should be taken:
     *
     */
    private static final String CAS_COOKIE_USERNAME = "cas.lasthandshake.username";
    private static final String CAS_COOKIE_LASTHANDSHAKE = "cas.lasthandshake.time";
    private static final String TRUSTED_USERNAME_HEADER = "x-cas-username";
    private static final String PROXY_USERNAME_HEADER = "x-remote-user";
    private static final String TRUSTED_CLIENT_ID_HEADER = "x-client-id";
    private static final String JSTL_PARAMS = "FLASH_JSTL_PARAMS";
    protected static final String USERNAME_SESSION_KEY = "userName";
    private static final Pattern TICKET_FINDER = Pattern.compile(".*<samlp:SessionIndex>(.*)</samlp:SessionIndex>.*");

    private boolean requireLogin = false, skipLoginFilter = false, enableHmac = false;
    private static String remoteProxies = "";
    private static final ArrayList<String> trustedServers = new ArrayList<>();
    private static final ArrayList<String> trustedClientIds = new ArrayList<>();
    private final ArrayList<Pattern> skipPagesList = new ArrayList<>();
    private static final HashMap<String, HttpSession> ticketSessionMap = new HashMap<>();
    private static HmacValidator validator;
    private static NetworkAddressValidator networkAddressValidator;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, 
            FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if(skipLoginFilter || isSkipPage(request)){
            filterChain.doFilter(request, response);
            return;
        }
        
        if (enableHmac && isHmacRequest(request)) {
            validator.doFilter(servletRequest, servletResponse, filterChain);
            return;
        }

        if(isTrustedRequest(request)){
            if(request.getHeader(TRUSTED_USERNAME_HEADER) != null){
                request.getSession().setAttribute(USERNAME_SESSION_KEY, 
                        request.getHeader(TRUSTED_USERNAME_HEADER));
                System.out.println("trusted user {}" + request.getHeader(TRUSTED_USERNAME_HEADER));
            }
            finishRequest(request, response, filterChain);
            return;
        }

        if (isRemoteUserRequest(request)){
            System.out.println("proxy user {}" + request.getHeader(PROXY_USERNAME_HEADER));
            request.getSession().setAttribute(USERNAME_SESSION_KEY, request.getHeader(PROXY_USERNAME_HEADER));
            finishRequest(request, response, filterChain);
            return;
        }

        if(receivedLogout(request)){
            return;
        }

        maybeSavePostParameters(request);
        if (receivedTicket(request, response)) {
            response.sendRedirect(sanitizeRequest(request));
            return;
        }

        if(handledLoginRequest(request, response)){
            return;
        }

        finishRequest(request, response, filterChain);
    }
    
    private boolean isSkipPage(HttpServletRequest request){
        String currentPage = SrsWebUtils.getCurrentPage(request);
        if ("".equals(currentPage)) {
            currentPage = "/";
        }
        if (skipPagesList.size() > 0) {
            for(Pattern skipPage: skipPagesList){
                if(skipPage.matcher(currentPage).matches()){
                    return true;
                }
            }
        }
        return false;
    }
    
    private boolean isHmacRequest(HttpServletRequest request){
        String authHeader = request.getHeader("authorization");
        return authHeader != null && authHeader.startsWith("SRS:");
    }
    
    private boolean receivedLogout(HttpServletRequest request){
        // When a user logs out with CAS or its TicketGrantingTicket expires, CAS will send a series of POSTs
        // to all the services that requested a ticket validation. The body of the POST is documented at:
        // https://wiki.jasig.org/display/CASUM/Single+Sign+Out
        // When such a POST is received we procede to invalidate the session corresponding to such ticket.
        // Note that the session might already be invalidated.

        if("POST".equals(request.getMethod())){
            Map reqParams = request.getParameterMap();
            String[] pars = (String[]) reqParams.get("logoutRequest");
            if(pars != null && pars.length > 0){
                Matcher ticketMatcher = TICKET_FINDER.matcher(pars[0]);
                if(ticketMatcher.matches()){
                    String ticket = ticketMatcher.group(1);
                    if(ticket != null){
                        HttpSession sessionToExpire = ticketSessionMap.remove(ticket);
                        if(sessionToExpire != null){
                            try {
                                sessionToExpire.invalidate();
                            } catch(IllegalStateException ise) {
                                // TODO: What should we do requestUrl?
                            }
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }
    
    private boolean receivedTicket(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        if(hasTicket(request)){
            String userName;
            try {
                userName = validateTicketWithCas(request);
            } catch(TicketValidationException ex) {
                throw new ServletException("Unable to validate ticket", ex);
            }
            if(userName != null){
                ticketSessionMap.put(request.getParameter("ticket"), request.getSession());
            }
            handleLoginAttributes(request, response, userName);
            return true;
        }
        return false;
    }
    
    private void maybeSavePostParameters(HttpServletRequest request){
        // Right now, only add the parameters if we're doing a POST. A GET will typically reserve
        // the paramters because they are in the URL.
        if ("POST".equals(request.getMethod()) && !request.getParameterMap().isEmpty()) {
            HashMap<String, String[]> reqParams = new HashMap<>();
            reqParams.putAll(request.getParameterMap());
            request.getSession().setAttribute(JSTL_PARAMS, reqParams);
        }
    }
    
    private HttpServletRequest wrapWithFlashParams(HttpServletRequest request){
        // Before we continue on with the filter chain, 
        // reinstate any flash scoped params from the users session and remove them from the session.
        HttpSession session = request.getSession(false);
        if (session != null) {
            // We need to wrap the request and add the parameters again using the map we got
            // There shouldn't be any new parameters encountered from the redirects
            // The params original params are only stored if we had a POST method.
            final Map<String, String[]> params = (Map) session.getAttribute(JSTL_PARAMS);
            if (params != null) {
                // Wrap the request with the complete set of params 
                request = new FlashRequest(request, params);
                session.removeAttribute(JSTL_PARAMS);
            }
        }
        return request;
    }
    
    private void finishRequest(HttpServletRequest request, HttpServletResponse response, 
            FilterChain filterChain) throws IOException, ServletException{
        if(hasLogin(request) || hasTicket(request)){
            response.sendRedirect(sanitizeRequest(request));
        } else {
            filterChain.doFilter(wrapWithFlashParams(request), response);
        }
    }
    
    private boolean needToGoToCas(HttpServletRequest request){
        //First determine if a trip to the CAS Gateway is needed
        boolean needToGoToCas = false;
        //If no trip was ever taken to the CAS Gateway, go there
        if (request.getSession().getAttribute("loginChecked") == null) {
            request.getSession().setAttribute("loginChecked", "true");
            needToGoToCas = true;
        }
        long lastHandshakeTime = getLastHandshake(request);
        long maxElapsedTimeSinceLastHandshake = 1 * 60 * 1000L;
        if (Math.abs(lastHandshakeTime - System.currentTimeMillis()) > maxElapsedTimeSinceLastHandshake) {
             needToGoToCas = true;
        }
        if (!getCookieUserName(request).equals(LoginUtils.userName(request))) {
             needToGoToCas = true;
        }
        return needToGoToCas;
    }

    private boolean handledLoginRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String login = getLoginRequest(request);
        if((requireLogin && "".equals(LoginUtils.userName(request))) || "true".equals(login)){
            loginWithCas(request, response, false);
            return true;
        }
        if("false".equals(login)){
            logoutWithCas(request, response);
            return true;
        } 
        if("renew".equals(login)){
            loginWithCas(request, response, true);
            return true;
        }
        if(needToGoToCas(request)){
            handleLoginAttributes(request, response, "");
            goToCasGateway(request, response);
            return true;
        }
        return false;
    }
    
    private String getLoginRequest(HttpServletRequest request) {
        return request.getParameter("login");
    }

    protected static String getCasBaseUrl(HttpServletRequest request) {
        String casBaseUrl = null;
        try {
            casBaseUrl = ModeSwitcherFilter.getVariable(request, "casBaseUrl");
        } catch (RuntimeException ex){
            // swallow
        }
        if (casBaseUrl == null) {
            casBaseUrl = "https://identity.slac.stanford.edu/cas";
        }
        return casBaseUrl;
    }
    
    public static void loginWithCas(HttpServletRequest request, HttpServletResponse response) throws IOException {
        loginWithCas(request, response, false);
    }
    
    protected static void loginWithCas(HttpServletRequest request, HttpServletResponse response, boolean renew) throws IOException {
        String requestUrl = sanitizeRequest(request);
        String redirectUrl = getCasBaseUrl(request) + "/login?";
        if(renew){
            redirectUrl += "renew=true&";
        }
        redirectUrl += "service=" + URLEncoder.encode(requestUrl, "UTF-8");
        response.sendRedirect(redirectUrl);
    }
    
    private void logoutWithCas(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handleLogoutAttributes(request, response);
        response.sendRedirect(getCasBaseUrl(request) + "/logout");
        request.getSession().invalidate();
    }

    private String validateTicketWithCas(HttpServletRequest request) 
            throws TicketValidationException, ServletException{
        String service = sanitizeRequest(request);
        String ticket = request.getParameter("ticket");
        if (ticket == null) {
            ticket = (String) request.getSession().getAttribute("casTicket");
        }
        if (ticket == null) {
            return null;
        }

        Cas20ProxyTicketValidator ticketValidator = new Cas20ProxyTicketValidator(getCasBaseUrl(request));

        Assertion a = ticketValidator.validate(ticket, service);

        if (a != null) {
            String userName = a.getPrincipal().getName();
            request.getSession().setAttribute("casTicket", ticket);
            return userName;
        } else {
            throw new ServletException("CAS Validation error ");
        }
    }

    private void goToCasGateway(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestUrl = sanitizeRequest(request);
        String redirectUrl = getCasBaseUrl(request) + "/login?gateway=true&service=" + URLEncoder.encode(requestUrl, "UTF-8");
        response.sendRedirect(redirectUrl);
    }

    private void fireLoginStatusChangedEvent(LoginStatusChangedEvent e) {
        List listeners = LoginUtils.getLoginStatusListenerList(e.getRequest().getSession());
        for(Object listener: listeners){
            ((LoginStatusListener) listener).loginStatusChanged(e);
        }
    }
    
    private String getCookieUserName(HttpServletRequest request) {
        String cookieValue = "";
        Cookie[] cookies = request.getCookies();
        if(cookies != null){
            for(Cookie c: cookies){
                if(LoginFilter.CAS_COOKIE_USERNAME.equals(c.getName())){
                    cookieValue = c.getValue();
                    break;
                }
            }   
        }
        return cookieValue;
    }
    
    public static void cleanupDestroyedSession(HttpSession session) {
        Set<Entry<String, HttpSession>> entrySet = ticketSessionMap.entrySet();
        ArrayList<String> ticketsToRemove = new ArrayList<>();
        for (Entry<String, HttpSession> entry : entrySet) {
            if (entry.getValue() == session) {
                ticketsToRemove.add(entry.getKey());
            }
        }
        for (String ticketToBeRemoved : ticketsToRemove) {
            ticketSessionMap.remove(ticketToBeRemoved);
        }

        LoginUtils.getLoginStatusListenerList(session).clear();
        session.removeAttribute("LoginStatusListenersList");
    }
    
    private long getLastHandshake(HttpServletRequest request) {
        Object lastHandshake = request.getSession().getAttribute(CAS_COOKIE_LASTHANDSHAKE);
        return lastHandshake != null ? (Long) lastHandshake : -1L;
    }
    
    private void handleLogoutAttributes(HttpServletRequest request, HttpServletResponse response) {
        request.getSession().removeAttribute(CAS_COOKIE_USERNAME);
        request.getSession().removeAttribute(CAS_COOKIE_LASTHANDSHAKE);
        request.getSession().removeAttribute(USERNAME_SESSION_KEY);
        fireLoginStatusChangedEvent(
            new LoginStatusChangedEvent(LoginStatusChangedEvent.USER_LOGGED_OUT, null, request)
        );
    }

    private void handleLoginAttributes(HttpServletRequest request, HttpServletResponse response, String userName) {
        String lastUserName = LoginUtils.userName(request);
        response.addCookie(createCookie(CAS_COOKIE_USERNAME, userName, getCookieDomain(request)));
        request.getSession().setAttribute(CAS_COOKIE_LASTHANDSHAKE, System.currentTimeMillis());
        request.getSession().setAttribute(USERNAME_SESSION_KEY, userName);
        
        if (!userName.equals(lastUserName)) {
            fireLoginStatusChangedEvent(
                new LoginStatusChangedEvent(LoginStatusChangedEvent.USER_LOGGED_IN, userName, request)
            );
        }
    }
    
    private static String getCookieDomain(HttpServletRequest request) {
        String cookieDefaultDomain = null;
        try {
            cookieDefaultDomain = ModeSwitcherFilter.getVariable(request, "loginFilterCookieDomain");
        } catch (RuntimeException ex){
            // swallow
        }
        if (cookieDefaultDomain == null) {
            cookieDefaultDomain = ".slac.stanford.edu";
        }
        if (request.getRequestURL().toString().contains(cookieDefaultDomain)) {
            return cookieDefaultDomain;
        }
        // TODO: Why return null? Do we ever return null?
        return null;
    }
    
    public static Cookie createCookie(String cookieName, String value, String domain) {
        Cookie c = new Cookie(cookieName, value);
        //Setting the maxAge of the cookie to a negative number makes it a non-persistent cookie. Its lasts while the browser is open.
        c.setMaxAge(value.equals("") ? 0 : -1);
        c.setPath("/");
        if (domain != null && !domain.equals("")) {
            c.setDomain(domain);
        }
        return c;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Enable
        if ("true".equals(System.getProperty("remoteUser"))){
            remoteProxies = System.getProperty("remoteProxies");
            if (remoteProxies == null || remoteProxies.isEmpty()) {
                System.out.println("ERROR: System Property `remoteProxies' is not set, check catalina.properties and server.xml");
            }  else {
               System.out.println("Will Authenticate users from " + remoteProxies + " as users when header " + PROXY_USERNAME_HEADER + " is set.");
            }
        } else {
            System.out.println("Proxy Users is disabled. Set remoteUser=true and remoteProxies system properties in catalina.proerties to enable");
        }
        String param = filterConfig.getInitParameter("requireLogin");
        requireLogin = "true".equalsIgnoreCase(param);
        skipLoginFilter = "true".equalsIgnoreCase(filterConfig.getInitParameter("skipLogin"));
        enableHmac = "true".equalsIgnoreCase(filterConfig.getInitParameter("enableHmac"));
        if (enableHmac) {
            validator = new HmacValidator();
            validator.init(filterConfig);
        }

        String skipPages = filterConfig.getInitParameter("skipPages");
        if (skipPages != null) {
            StringTokenizer stt = new StringTokenizer(skipPages, ",");
            while (stt.hasMoreTokens()) {
                String txt = stt.nextToken();
                if (!txt.contains("*")) {
                    txt = ".*" + txt;
                } else {
                    txt = txt.replace("*", ".*");
                }
                skipPagesList.add(Pattern.compile(txt));
            }

        }

        String configDb = filterConfig.getInitParameter("configDb");
        if (configDb == null) {
            System.out.println("No configDb parameter provided. Skipping configuration of trusted servers to ignore.");
        } else {
            fillTrustedServerList(configDb);
            fillTrustedClientIdList(configDb);
            createNetworkAddressValidator(configDb);
        }
    }

    @Override
    public void destroy() { }

    protected static String sanitizeRequest(HttpServletRequest request) {
        String queryString = request.getQueryString() != null ? request.getQueryString() : "";
        if(!queryString.isEmpty()){
            if(request.getParameter("ticket") != null){
                queryString = queryString.replace("ticket=" + request.getParameter("ticket"), "");
            }
            if(request.getParameter("login") != null){
                queryString = queryString.replace("login=" + request.getParameter("login"), "");
            }
            queryString = queryString.replace("&&", "&");
            queryString = queryString.replace("&&", "&");
            if(queryString.startsWith("&")){
                queryString = queryString.substring(1);
            }
            if(queryString.endsWith("&")){
                queryString = queryString.substring(0, queryString.length() - 1);
            }
            if(!queryString.isEmpty()){
                queryString = "?" + queryString;   
            }
        }
        return request.getRequestURL().toString() + queryString;
    }

    private boolean hasTicket(HttpServletRequest request) {
        return request.getParameter("ticket") != null;
    }
    
    private boolean hasLogin(HttpServletRequest request) {
        return request.getParameter("login") != null;
    }

    // Keep for backward compatibility.
    public static String userName(HttpServletRequest request) {
        return LoginUtils.userName(request);
    }

    /**
     * Determine if a request is to be trusted by checking if it's from a trusted
     * server or client application. This method may be used in other projects.
     * @param request
     * @return True if trusted request
     */
    public static boolean isTrustedRequest(HttpServletRequest request) {        
        return isTrustedServerRequest(request) || isTrustedClientIdRequest(request) || isTrustedIpRequest(request);
    }

    /**
     * Determine if a request is from proxy server by comparing to
     * remoteProxies system property, and checking PROXY_USERNAME_HEADER is populated
     * @param request
     * @return True if proxy request
     */
    public static boolean isRemoteUserRequest(HttpServletRequest request) {
        if (remoteProxies.contains(request.getRemoteAddr())) {
            if (request.getHeader(PROXY_USERNAME_HEADER) != null){
                return !request.getHeader(PROXY_USERNAME_HEADER).isEmpty();

            }
        }
        return false;
    }

    /**
     * Determine if a request is to be trusted by checking if it's from a trusted server.
     * This method may be used in other projects.
     * @param request
     * @return True if request originated from a trusted server.
     */
    public static boolean isTrustedServerRequest(HttpServletRequest request) {
        return trustedServers.contains(request.getRemoteHost());
    }
    
    /**
     * Determine if a request is to be trusted by checking if there's a trusted client id,
     * This method may be used in other projects.
     * @param request
     * @return True if trusted
     */
    public static boolean isTrustedClientIdRequest(HttpServletRequest request) {
        String clientId = request.getHeader(TRUSTED_CLIENT_ID_HEADER);
        return trustedClientIds.contains(clientId);
    }
    
    /**
     * Determine if a request's IP is trusted and the login filter can be skipped.
     * @param request
     * @return True if trusted
     */
    public static boolean isTrustedIpRequest(HttpServletRequest request) {
        String ipAddress = request.getRemoteAddr();
        logger.log(Level.FINE, "Checking trusted IP address: {0}", new Object[]{ipAddress});
        if ( networkAddressValidator != null )
            return networkAddressValidator.isApproved(ipAddress);
        return false;
    }

    /**
     * Determine if a request originated from a tomcat server.
     * @see #isTrustedServerRequest(javax.servlet.http.HttpServletRequest) 
     * @param request
     * @return Whether or not the request originated from a trusted (tomcat) server.
     */
    @Deprecated
    public static boolean isTomcatServerRequest(HttpServletRequest request) {
        return isTrustedServerRequest(request);
    }

    private void fillTrustedServerList(String configDb) {
        try (Connection c = ConnectionManager.getConnection(configDb)) {
            String sqlQuery = "select host from tomcat_servers";
            PreparedStatement statement = c.prepareStatement(sqlQuery);
            ResultSet r = statement.executeQuery();
            while (r.next()) {
                String host = InetAddress.getByName(r.getString("host")).getHostAddress();
                trustedServers.add(host);
            }
        } catch(JspException | SQLException | UnknownHostException ex) {
            // TODO: Remove print statements
            System.out.println("**** Problem loading list of Tomcat servers to ignore.");
            ex.printStackTrace();
            logger.log(Level.WARNING, "**** Problem loading list of trusted servers to ignore", ex);
        }
    }
    
    private void fillTrustedClientIdList(String configDb) {
        try (Connection c = ConnectionManager.getConnection(configDb)) {
            String sqlQuery = "select client_id from trusted_clients where client_id is not null";
            PreparedStatement statement = c.prepareStatement(sqlQuery);
            ResultSet r = statement.executeQuery();
            while (r.next()) {
                String clientId = r.getString("client_id");
                trustedClientIds.add(clientId);
            }
            System.out.println( "Added Client Ids {}" + trustedClientIds);
        } catch(JspException | SQLException ex) {
            logger.log(Level.WARNING, "**** Problem loading list of Application Client Ids to ignore", ex);
        }
    }
    
    private void createNetworkAddressValidator(String configDb) {
        try (Connection c = ConnectionManager.getConnection(configDb)) {
            String sqlQuery = "select mask from trusted_network_address";
            PreparedStatement statement = c.prepareStatement(sqlQuery);
            ResultSet r = statement.executeQuery();
            List<String> masks = new ArrayList<>();
            while (r.next()) {
                masks.add(r.getString("mask"));
            }
            networkAddressValidator = new NetworkAddressValidator(masks);
        } catch(JspException | SQLException ex) {
            // TODO: Remove print statements
            ex.printStackTrace();
            logger.log(Level.WARNING, "**** Problem loading list of trusted masks", ex);
        }
    }

        private class NetworkAddressValidator {

        private final List<SubnetUtils> approvedAddressList = new ArrayList<>();

        public NetworkAddressValidator(List<String> approvedAddressList) {
            for (String mask : approvedAddressList) {
                SubnetUtils su =  null;
                if (mask.indexOf("/") > 0) {
                    su = new SubnetUtils(mask);
                } else {
                    su = new SubnetUtils(mask + "/32");
                }
                this.approvedAddressList.add(su);
                su.setInclusiveHostCount(true);
            }
        }

        public boolean isApproved(String ipAddress) {
            for (SubnetUtils approvedAddress : approvedAddressList) {
                if (approvedAddress.getInfo().isInRange(ipAddress)) {
                    return true;
                }
            }
            return false;
        }
    }
    
}
