package org.srs.web.base.decorator;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import javax.servlet.jsp.PageContext;
import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;
import org.srs.web.base.utils.TimeUtils;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public class TimeDecorator implements DisplaytagColumnDecorator{
    
    public TimeDecorator() {
    }

    public Object decorate(Object object, PageContext pageContext, MediaTypeEnum mediaTypeEnum) throws DecoratorException {
        String format = (String)pageContext.getAttribute("dateFormat",PageContext.SESSION_SCOPE);
        String timezone = (String)pageContext.getAttribute("timeZone",PageContext.SESSION_SCOPE);
        if ( format == null )
            format = "%d-%b-%Y %H:%M:%S";
        if ( timezone == null )
            timezone = TimeZone.getDefault().getID();
        
        long millis = -1;
        if ( object instanceof Long ) 
            millis = ((Long) object).longValue();
        else if ( object instanceof BigDecimal )
            millis = ((BigDecimal) object).longValue();
        else if ( object instanceof String ) {
            try {                
                millis = Long.valueOf(((String)object).trim()).longValue();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if ( millis < 0 )
            return "None";
        SimpleDateFormat df = new SimpleDateFormat(TimeUtils.convertToSimpleDateFormat(format));
        if ( timezone != null )
            df.setTimeZone(TimeZone.getTimeZone(timezone));
        return df.format(new Date(millis));                

        
        
    }

}
