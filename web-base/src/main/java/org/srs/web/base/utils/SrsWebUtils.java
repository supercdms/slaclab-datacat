/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.srs.web.base.utils;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;
import java.util.regex.Pattern;

/**
 *
 * @author turri
 */
public class SrsWebUtils {

    public static String getContextName(FilterConfig filterConfig) {
        return getContextName(filterConfig.getServletContext());
    }

    public static String getContextName(PageContext pageContext) {
        return getContextName(pageContext.getServletContext());
    }

    static Pattern NUMERIC_PREFIX_CONTEXT = Pattern.compile("^\\d+-");

    public static String getContextName(ServletContext servletContext) {
        String contextName = null;
        try {
            contextName = servletContext.getResource("/").getPath();
            contextName = contextName.substring(0, contextName.length() - 1);
            int last = contextName.lastIndexOf("/");
            if (last == 0) {
                return "";
            }
            contextName = contextName.substring(last + 1);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return contextName;
    }

    public static String getCurrentPage(PageContext pageContext) {
        String url = ((HttpServletRequest) pageContext.getRequest()).getRequestURL().toString();
        String contextName = getContextName(pageContext);
        if ("".equals(contextName) || "ROOT".equals(contextName)) {
            url = url.substring(url.lastIndexOf("/"));
        } else if (url.contains(contextName)){
            url = url.substring(url.indexOf(contextName)).replace(contextName, "");
        } else if (NUMERIC_PREFIX_CONTEXT.matcher(contextName).find() && url.contains(contextName.substring(3))) {
            // match XX-[contextName] contexts
            contextName = contextName.substring(3);
            url = url.substring(url.indexOf(contextName)).replace(contextName, "");
        } else {
            boolean matches = NUMERIC_PREFIX_CONTEXT.matcher(contextName).find();
            boolean contains = url.contains(contextName.substring(3));
            throw new RuntimeException("Unable to find current page for context: " + contextName + " in url: " + url+ ", matches `XX-`="+ matches + ",contains `XX-` substring="+contains);
        }
        if (url.startsWith("/")) {
            url = url.substring(1);
        }
        return url;
    }

    public static String getCurrentPage(HttpServletRequest request) {
        String contextPath = request.getContextPath();
        String requestURI = request.getRequestURI();
//        if ( ! requestURI.endsWith("/") )
//            requestURI += "/";
        if ( requestURI.equals(contextPath) )
            return "";
        return requestURI.substring(contextPath.length() + 1);
    }

    public static String pathBrowser(String path, String basePath) {
        if (path == null) {
            return "";
        }
        String[] paths = path.split("/");
        String returnString = "";
        String accumulatedPath = "";
        if (basePath != null) {
            accumulatedPath = basePath;
        }
        for (int i = 0; i < paths.length - 1; i++) {
            if (paths[i].equals("")) {
                continue;
            }
            accumulatedPath += "/" + paths[i];
            returnString += "<a href=\"" + accumulatedPath + "/\">/" + paths[i] + "</a>";
        }
        return returnString + "/" + paths[paths.length - 1];
    }
}
