package org.srs.web.base.preferences;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class PreferenceTag extends SimpleTagSupport {
    
    private String name, title;
    private int size = -1;
    private ArrayList allowedValues = new ArrayList();
    private ArrayList textValues = new ArrayList();
    private boolean multiple = false;
    
    public void doTag() throws JspException, IOException {
        
        String errorMessage = "";
        String formName = "preference_"+name;
        
        PreferencesTag preferencesTag = (PreferencesTag)findAncestorWithClass(this,PreferencesTag.class);
        Object preferencesBean = preferencesTag.getPreferencesBean();
        
        Writer writer = getJspContext().getOut();
        JspFragment fragment = getJspBody();
        if (fragment != null) fragment.invoke(writer);
        
        boolean hasAllowedValues = allowedValues.size() > 0;
        
        // Look to see if there is a parameter in the request with the name of the property.
        // If so set it on the bean.
        HttpServletRequest request = (HttpServletRequest)((PageContext)getJspContext()).getRequest();
        String inputValue = null;
        if ( multiple ) {
            String[] parValues = request.getParameterValues(formName);
            if ( parValues != null ) {
                for ( int i = 0; i < parValues.length; i++ ) {
                    if ( inputValue == null )
                        inputValue = "";
                    if ( ! inputValue.equals("") )
                        inputValue += ",";
                    inputValue += parValues[i];
                }
            }
        } else
            inputValue = request.getParameter(formName);
        
        if ( inputValue != null ) {
            Method setterMethod = PreferencesUtils.getSetterMethod(preferencesBean,name);
            try {
                PreferencesUtils.setPreference(preferencesBean,setterMethod,inputValue);
                preferencesTag.setSavePreferences(true);
            } catch (Exception setE) {
                String message = findRootMessage(setE);
                if ( message == null )
                    message = "Some problem occurred.";
                errorMessage += message;
            }
        }
        
        Object value = PreferencesUtils.getPreference(preferencesBean, name);
        
        writer.write("<tr>\n");
        writer.write("<td>\n");
        writer.write(title);
        writer.write("</td>\n");
        writer.write("<td>\n");
        
        
        if ( hasAllowedValues ) {
            if ( multiple )
                size = allowedValues.size();
            else
                size = 1;
            
            writer.write("<select name=\""+formName+"\" size=\""+size+"\"");
            if ( multiple )
                writer.write(" multiple ");
            writer.write(" > \n");
            
            StringTokenizer stt = new StringTokenizer(value.toString(),",");
            ArrayList valueList = new ArrayList();
            while(stt.hasMoreTokens())
                valueList.add(stt.nextToken());
            for ( int i = 0; i < allowedValues.size(); i++ ) {
                String option = allowedValues.get(i).toString();
                boolean selected = false;
                if ( multiple )
                    selected = valueList.contains(option);
                else
                    selected = value.equals(option);
                String text = (String) textValues.get(i);
                if ( text == null )
                    text = option;
                writer.write("<option value=\""+option+"\"");
                if ( selected )
                    writer.write(" selected");
                writer.write(">"+text+"</option>");
            }
            writer.write("</select> \n");
        } else {
            
            
            writer.write("<input name=\""+formName+"\" value=\""+value+"\" ");
            if ( size > 0 )
                writer.write(" size = \""+size+"\" ");
            
            writer.write("/> \n");
            
            
        }
        writer.write("</td>\n");
        writer.write("<td>\n");
        if ( ! errorMessage.equals("") ) {
            writer.write("<font color=\"red\" >");
            writer.write(errorMessage);
            writer.write("</font>\n");
        }
        writer.write("</td>\n");
        writer.write("</tr>\n");
        
    }
    
    void addToValues(Object obj, String text) {
        allowedValues.add(obj);
        textValues.add(text);
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public void setSize(int size) {
        this.size = size;
    }
    
    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }
    
    private String findRootMessage(Throwable e) {
        String message = e.getMessage();
        if ( message != null )
            return message;
        Throwable p = e.getCause();
        if ( p != null )
            return findRootMessage(p);
        return null;
    }
}