package org.srs.web.base.taglib.pagination;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.jstl.sql.SQLExecutionTag;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import org.srs.web.base.db.ConnectionManager;
import org.srs.web.base.utils.DisplayTagUtils;

/**
 *
 * @author tonyj
 */
public class Query extends SimpleTagSupport implements SQLExecutionTag {

    private Object rawDataSource;
    private String sql;
    private int pageSize = 25;
    private int pageNumber = 0;
    private boolean ascending = false;
    private String sortCriterion;
    private String sortOrder;
    private String defaultSortCriterion;
    private String var;
    private final List params = new ArrayList();
    private String tableId;

    public void addSQLParameter(Object o) {
        params.add(o);
    }

    public void doTag() throws JspException, IOException {
        StringWriter writer = new StringWriter();
        JspFragment fragment = getJspBody();
        if (fragment != null) {
            fragment.invoke(writer);
        }
        String sqlIn = sql == null ? writer.toString() : sql;
        ServletRequest request = ((PageContext) getJspContext()).getRequest();
        boolean isExporting = DisplayTagUtils.isExporting(request, tableId);

        ascending = sortOrder == null ? false : (sortOrder.startsWith("asc") ? true : false);

        if (request.getParameter("page") != null) {
            pageNumber = Math.max(0, Integer.valueOf(request.getParameter("page")).intValue() - 1);
        }
        if (request.getParameter("sort") != null) {
            sortCriterion = request.getParameter("sort");
        }
        if (request.getParameter("dir") != null) {
            ascending = request.getParameter("dir").equals("asc");
        }

        try {
            Connection connection = ConnectionManager.getConnection((PageContext) getJspContext(), rawDataSource);
            try {
                boolean isMySql = "mysql".equalsIgnoreCase(connection.getMetaData().getDatabaseProductName());
                String actualSql = sqlIn.replace((char) 13, ' ').replace((char) 10, ' ');
                String rowsSql = "select count(1) from (" + actualSql + ") x";
                PreparedStatement stmt = connection.prepareStatement(rowsSql);
                int i = 1;
                for (Object param : params) {
                    stmt.setObject(i++, param);
                }

                ResultSet rs = stmt.executeQuery();
                rs.next();
                int rows = rs.getInt(1);
                stmt.close();


                String sortColumn = sortCriterion;
                if (sortColumn == null || sortColumn.length() == 0) {
                    sortColumn = defaultSortCriterion;
                }
                if (sortColumn != null && sortColumn.length() > 0) {
                    actualSql = "select * from (" + actualSql + ") x order by " + sortColumn + " " + (ascending ? "asc" : "desc");
                }
                if (isExporting) {
                    stmt = connection.prepareStatement(actualSql);
                    i = 1;
                    for (Object param : params) {
                        stmt.setObject(i++, param);
                    }
                } else {
                    if (isMySql) {
                        actualSql = actualSql + " limit ?,?";

                    } else {
                        actualSql = "select * from (select d.*,rownum rn from (" + actualSql + ") d where rownum <= ?) where rn >= ?";
                    }
                    stmt = connection.prepareStatement(actualSql);
                    i = 1;
                    for (Object param : params) {
                        stmt.setObject(i++, param);
                    }
                    if (isMySql) {
                        stmt.setInt(i++, pageSize * pageNumber);
                        stmt.setInt(i++, pageSize);
                    } else {
                        stmt.setInt(i++, pageSize * (pageNumber + 1));
                        stmt.setInt(i++, pageSize * pageNumber + 1);
                    }
                }

                rs = stmt.executeQuery();
                getJspContext().setAttribute(var, new PaginatedResultSet(rs, rows, pageSize, pageNumber, ascending, sortColumn), PageContext.PAGE_SCOPE);
                stmt.close();
            } finally {
                connection.close();
            }
        } catch (SQLException x) {
            throw new JspException("Error executing SQL: \n" + sqlIn, x);
        }
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public void setDataSource(Object rawDataSource) {
        this.rawDataSource = rawDataSource;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setDefaultSortColumn(String sortCriterion) {
        this.defaultSortCriterion = sortCriterion;
    }

    public void setDefaultSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }
}
