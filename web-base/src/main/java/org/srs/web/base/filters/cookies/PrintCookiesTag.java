package org.srs.web.base.filters.cookies;

import java.io.IOException;
import java.io.Writer;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class PrintCookiesTag extends SimpleTagSupport {
    
    public void doTag() throws JspException, IOException {
        HttpServletRequest request = (HttpServletRequest) ((PageContext)getJspContext()).getRequest();
        HttpServletResponse response = (HttpServletResponse) ((PageContext)getJspContext()).getResponse();
        
        
        Cookie[] cookies = request.getCookies();
        
        Writer writer = response.getWriter();
        writer.write("<table>");
        for ( int i = 0; i < cookies.length; i++ ) {
            writer.write("<tr>");
            Cookie c = cookies[i];
            writer.write("<td>");
            writer.write(c.getName());
            writer.write("</td>");
            writer.write("<td>");
            writer.write(c.getValue());
            writer.write("</td>");
            writer.write("<td>");
            writer.write(String.valueOf(c.getMaxAge()));
            writer.write("</td>");
            writer.write("<td>");
            if ( c.getPath() != null )
                writer.write(c.getPath());
            else
                writer.write("null");
            writer.write("</td>");
            writer.write("<td>");
            if ( c.getDomain() != null )
                writer.write(c.getDomain());
            else
                writer.write("null");
            writer.write("</td>");
            writer.write("</tr>");
        }
        writer.write("</table>");
        
    }
    
}