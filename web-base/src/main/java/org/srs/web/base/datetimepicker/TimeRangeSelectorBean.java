package org.srs.web.base.datetimepicker;

import java.util.Date;
import javax.servlet.jsp.PageContext;
import java.lang.Long;
import java.lang.Object;
import org.srs.web.base.utils.TimeUtils;

public class TimeRangeSelectorBean {

// variableName - the stem that will be pre-pended to all session variable names in order to keep track of them
// as user moves back and forth in a page.
//    private String tagStartTimeStr = "tagStart_Time",  tagEndTimeStr = "tagEnd_Time";
    public long nperiod = -1;
    public long selectedNperiod = -1;
    public String period;
    public String basename = null, variableName = null;
    //These are the default values if user of the TimeRangeSelection tag does not specify start/end times.
    public long starttimelong = -1, endtimelong = -1;
    public long sessionStartTime = -1, sessionEndTime = -1;
    //These are the variables that are returned to the JSP
    public long selectedStarttime = -1, selectedEndtime = -1;
    public boolean shownone = true;
    public boolean showform = true;
    public boolean showtime = true;
    public String format = "%b/%e/%y %H:%M:%S";
    public String timezone = "UTC";
    private boolean UserSelectedStart = false;
    private boolean UserSelectedEnd = false;
    private boolean UserSelectedNperiod = false;
    private boolean isNumberSession = false;
    private boolean isNumberSelected = false;
    private long sessionStartTimeValue = -1, sessionEndTimeValue = -1;
    private String sessionPeriodValue;
    private long sessionNperiodValue = -1;
    private long period_units = -1;
    private long storedNperiod = -1;

// the constructor
    public TimeRangeSelectorBean() {
//        System.out.println("TimeRangeSelectorBean constructor called. Create an instance of the bean ");
    }

    public static void invokeTimeRangeSelectionLogic(TimeRangeSelectorBean bean, PageContext pageContext) {
        // this routine exists so we can call the method timeRangeLogic and pass it the pageContext object. JSP cannot
        // evaluate then substitute values in variables.
        //
        bean.timeRangeLogic(pageContext);
    }

// logic
// pageContext provides access to all name spaces associated with a JSP page.
    public void timeRangeLogic(PageContext page) {

        System.out.println("...entering timeRangeLogic");

        if (variableName == null) {
            throw new RuntimeException("Variable name must be passed! ");
        }


        if (getStarttime() == -1 && getEndtime() == -1 && getNperiod() == -1) {
            throw new RuntimeException("startime, endtime and nperiod cannot be all empty");
        }

//      build the variable names so they are prefixed with the basename
        String firstTimeAttribute = getBasename() + "_FirstTimeHere";
        String sessionStartTimeAttributeName = getBasename() + "_SessionStarttime";
        String sessionEndTimeAttributeName = getBasename() + "_SessionEndtime";
        String sessionPeriodAttributeName = getBasename() + "_SessionPeriod";
        String sessionNperiodAttributeName = getBasename() + "_SessionNperiod";

//      System.out.println("...create session names");
//      System.out.println("firstTimeAttribute = " + page.getSession().getAttribute(firstTimeAttribute));
//      System.out.println("sessionStartTimeAttributeName = " + page.getSession().getAttribute(sessionStartTimeAttributeName));
//      System.out.println("sessionEndTimeAttributeName = " + page.getSession().getAttribute(sessionEndTimeAttributeName));
//      System.out.println("sessionPeriodAttributeName = " + page.getSession().getAttribute(sessionPeriodAttributeName));
//      System.out.println("sessionNperiodAttributeName = " + page.getSession().getAttribute(sessionNperiodAttributeName));

// test if first time to page. if first time, initialize session values for some vars. The first time
// thru the start and end times contain the milliseconds because the jsp specified a format with milliseconds
// as an initial time to start things off. We need to chop the milliseconds the first time because the tag
// dictates the format as H:M:S - without millis. Chop the millis and save in the session for later comparisons.
// So, take the starttime and the current time convert them, get the difference, delta and put the converted
// time back into the session. Do this for both start and end time. We don't care about delta, except to see what the difference was.
        if (page.getSession().getAttribute(firstTimeAttribute) == null) {
//             System.out.println("...FIRST TIME TO PAGE");

            page.getSession().setAttribute(firstTimeAttribute, "beenHereDoneThat");
//          System.out.println("****************** "+currentMillis+" "+newConvertedMillis+" "+delta);
            System.out.println("...Calling getter methods ");
            sessionStartTimeValue = convertTimeToInternalFormat(getStarttime());
            sessionEndTimeValue = convertTimeToInternalFormat(getEndtime());

//             System.out.println("currentStartMillis " + currentStartMillis + " getStarttime() " + getStarttime());
//             System.out.println("currentEndMillis " + currentEndMillis + " getEndtime() " + getEndtime());
//
//             System.out.println("convertedCurrentStartMillis " + convertedCurrentStartMillis);
//             System.out.println("convertedCurrentEndMillis " + convertedCurrentEndMillis);
//
//             System.out.println("delta_in_start " + delta_in_start);
//             System.out.println("delta_in_end " + delta_in_end);

//             System.out.println("currentMillis " + currentMillis + " getStarttime() " + getStarttime());
//             System.out.println("newConvertedMillis " + newConvertedMillis);
//             System.out.println("delta " + delta);
//
            sessionPeriodValue = getPeriod();
            sessionNperiodValue = getNperiod();
            // sessionStartTimeValue etc. are type long so don't use String.valueOf to put them in the session attributes
            page.getSession().setAttribute(sessionStartTimeAttributeName, sessionStartTimeValue);
            page.getSession().setAttribute(sessionEndTimeAttributeName, sessionEndTimeValue);
            page.getSession().setAttribute(sessionPeriodAttributeName, sessionPeriodValue);
            page.getSession().setAttribute(sessionNperiodAttributeName, sessionNperiodValue);

////             System.out.println("sessionStartTimeValue " + sessionStartTimeValue);
////             System.out.println("sessionEndTimeValue " + sessionEndTimeValue);
////             System.out.println("sessionPeriodValue " + sessionPeriodValue);
////             System.out.println("sessionNperiodValue " + sessionNperiodValue);
////             System.out.println("...initialize values of selectedStarttime, selectedEndtime and selectedNperiod ");
////             System.out.println("selectedStarttime=" + selectedStarttime);
////             System.out.println("selectedEndtime =" + selectedEndtime);
////             System.out.println("selectedNperiod=" + selectedNperiod);
////
////             System.out.println("...initialize values of session start,end and nperiod variables");
////             System.out.println("sessionStartTimeAttributeName " + String.valueOf(sessionStartTimeAttributeName));
////             System.out.println("sessionEndTimeAttributeName " + String.valueOf(sessionEndTimeAttributeName));
////             System.out.println("sessionNperiodAttributeName " + String.valueOf(sessionNperiodAttributeName));
        } else {
////        System.out.println("...RETRIEVING SESSION VARIABLES");
            sessionStartTimeValue = ((Long) page.getSession().getAttribute(sessionStartTimeAttributeName)).longValue();
            sessionEndTimeValue = ((Long) page.getSession().getAttribute(sessionEndTimeAttributeName)).longValue();
            sessionNperiodValue = ((Long) page.getSession().getAttribute(sessionNperiodAttributeName)).longValue();
        }

//        System.out.println("...CHECK IF A REQUEST HAS BEEN MADE");
        // This is the user's selected startTime from the calendar.
        String selectedStartTimeParam = page.getRequest().getParameter(getStarttimeName());
        String selectedEndTimeParam = page.getRequest().getParameter(getEndtimeName());
        String selectedNperiodParam = page.getRequest().getParameter(getNperiodName());

        // Catch when selectedNperiod is null (it is displayed as null in the jsp). When it's passed as a request to the java code,
        // it cannot be null so test for null and set sessionNperiodValue to -1
////////        try{
////////           long testVal = Long.parseLong(selectedNperiodParam);
////////           System.out.println("testVal or value of request selectedNperiodParam " + testVal);
////////        }
////////        catch(NumberFormatException nfe ){
////////            System.out.println("NumberFormat problem " + nfe.getMessage());
////////            sessionNperiodValue = -1;
////////        }


//        System.out.println("...Request parameters set to: ");
//        System.out.println(getStarttimeName() + " = selectedStartTimeParam = "+selectedStartTimeParam);
//        System.out.println(getEndtimeName()+ " = selectedEndTimePara = "+selectedEndTimeParam);
//        System.out.println(getNperiodName()+" = selectedNperiodParam = "+selectedNperiodParam);

        if (selectedStartTimeParam != null || selectedEndTimeParam != null || selectedNperiodParam != null) {
//        if (selectedStartTimeParam != null || selectedEndTimeParam != null || Long.parseLong(selectedNperiodParam) > 0 ) {
            // check if user actually selected something different from the previous time by comparing current to previous times.
            if (selectedStartTimeParam != null) {
                sessionStartTimeValue = Long.parseLong(selectedStartTimeParam);
                long storedSessionStart = ((Long) page.getSession().getAttribute(sessionStartTimeAttributeName)).longValue();
//                System.out.println("1**** sessionStartTimeValue " + sessionStartTimeValue + " storedSessionStart= " + storedSessionStart + " selectedStartTimeParam= " + selectedStartTimeParam);
                if (sessionStartTimeValue != storedSessionStart) {
                    UserSelectedStart = true;
                    UserSelectedNperiod = false;
                    System.out.println("Setting nperiod to -1 in start. UserSelectedStart = " + UserSelectedStart);
                    sessionNperiodValue = -1;
                }
            }
            if (selectedEndTimeParam != null) {
                sessionEndTimeValue = Long.parseLong(selectedEndTimeParam);
                long storedSessionEnd = (Long) page.getSession().getAttribute(sessionEndTimeAttributeName);
//                System.out.println("2**** sessionEndTimeValue " + sessionEndTimeValue + " storedSessionEnd= " + storedSessionEnd + " selectedEndTimeParam= " + selectedEndTimeParam);
                if (sessionEndTimeValue != storedSessionEnd) {
                    UserSelectedEnd = true;
                    UserSelectedNperiod = false;
//                    System.out.println("Setting nperiod to -1 in end. UserSelectedEnd = " + UserSelectedEnd);
                    sessionNperiodValue = -1;
                }
            }
//
//          If user selected 'none' for both start/end and a period then must set start/end flags to false
            if (selectedStartTimeParam != null && selectedEndTimeParam != null && selectedNperiodParam != null) {
                sessionStartTimeValue = Long.parseLong(selectedStartTimeParam);
                sessionEndTimeValue = Long.parseLong(selectedEndTimeParam);
//                System.out.println("!*** sessionStartTimeValue = " + sessionStartTimeValue);
//                System.out.println("!*** sessionEndTimeValue = " + sessionEndTimeValue);
//                System.out.println("!*** NperiodParam = " + selectedNperiodParam + " sessionNperiodValue = " + sessionNperiodValue );
                if (sessionStartTimeValue == -1 && sessionEndTimeValue == -1){
                    UserSelectedStart = false;
                    UserSelectedEnd = false; 
                }
            }
            if (selectedNperiodParam != null && (!UserSelectedStart && !UserSelectedEnd)) {
                sessionPeriodValue = getPeriod();
                // this try block is here because we have to adjust the nperiod value if the user selects -1. We want
                // to return a blank to the user not a -1
                try {
                    storedNperiod = (Long) page.getSession().getAttribute(sessionNperiodAttributeName);
                    isNumberSession = true;
                } catch (NumberFormatException nfe1) {
                   System.out.println("isNumberSession Format problem for storedNperiod " + nfe1.getMessage() + " = " + storedNperiod);
                    isNumberSession = false;
                }
                try {
                    long testVal = Long.parseLong(selectedNperiodParam);
                    if (testVal > 0) {
                        isNumberSelected = true;
                    }
                } catch (NumberFormatException nfe2) {
                    System.out.println("isNumberSelected Format problem " + nfe2.getMessage());
                    isNumberSelected = false;
                }
                if (isNumberSelected && isNumberSession) {
                    sessionNperiodValue = Long.parseLong(selectedNperiodParam);
//                    System.out.println("3**** sessionNperiodValue " + sessionNperiodValue);
                    if (sessionNperiodValue != storedNperiod) {
                        UserSelectedNperiod = true;
                        UserSelectedStart = false;
                        UserSelectedEnd = false;
                        long period_units = periodUnits(sessionPeriodValue, sessionNperiodValue);
                    }
                } else {
                    System.out.println("Setting nperiod to -1 in ?????");
                    sessionNperiodValue = -1;
                }
            }

//          toggle values; if start/end then period is -1. if period then start & end are -1
//          if users selects all three then by default use the calendar choice
            if ((UserSelectedStart || UserSelectedEnd) && UserSelectedNperiod) {
                System.out.println("Setting nperiod to -1 in logic");
                sessionNperiodValue = -1;
            }
            if ((!UserSelectedStart && !UserSelectedEnd) && UserSelectedNperiod) {
                long now = System.currentTimeMillis();
                sessionEndTimeValue = -1;
                sessionStartTimeValue = -1;
              System.out.println("Millis: UserSelectedNperiod " + UserSelectedNperiod + " sessionStartTimeValue = " + sessionStartTimeValue + " sessionEndTimeValue = " + sessionEndTimeValue);
            }

            // update the attributes in session
            page.getSession().setAttribute(sessionStartTimeAttributeName, sessionStartTimeValue);
            page.getSession().setAttribute(sessionNperiodAttributeName, sessionNperiodValue);
            page.getSession().setAttribute(sessionEndTimeAttributeName, sessionEndTimeValue);

            System.out.println("...UPDATING SESSION ATTRIBUTES");
            System.out.println("sessionStartTimeValue = " + sessionStartTimeValue);
            System.out.println("sessionNperiodValue = " + sessionNperiodValue );
            System.out.println("sessionEndTimeValue = " + sessionEndTimeValue);
        }

        System.out.println("sessionNperiodValue = " + sessionNperiodValue );
        if (sessionNperiodValue == -1) {
            selectedStarttime = sessionStartTimeValue;
            selectedEndtime = sessionEndTimeValue;
            selectedNperiod = sessionNperiodValue;
        } else {
            selectedEndtime = convertTimeToInternalFormat(System.currentTimeMillis());
            selectedStarttime = convertTimeToInternalFormat(selectedEndtime - periodUnits(getPeriod(), sessionNperiodValue));
            selectedNperiod = sessionNperiodValue;
            page.getSession().setAttribute(sessionStartTimeAttributeName, selectedStarttime);
            page.getSession().setAttribute(sessionEndTimeAttributeName, selectedEndtime);
        }
////////
////////        sessionStartTimeValue = ((Long) page.getSession().getAttribute(sessionStartTimeAttributeName)).longValue();
////////        sessionEndTimeValue = ((Long) page.getSession().getAttribute(sessionEndTimeAttributeName)).longValue();
////////        sessionNperiodValue = ((Long) page.getSession().getAttribute(sessionNperiodAttributeName)).longValue();
//////
////////        System.out.println("LOOP2 ");
////////        System.out.println("selectedStarttime =" + selectedStarttime + " sessionStartTimeValue =" + sessionStartTimeValue + " UserSelectedStart " + UserSelectedStart);
////////        System.out.println("selectedEndtime =" + selectedEndtime + " sessionEndTimeValue =" + sessionEndTimeValue + " UserSelectedEnd" + UserSelectedEnd);
////////        System.out.println("selectedNperiod =" + selectedNperiod + " sessionNperiodValue = " + sessionNperiodValue + "UserSelectedNperiod " + UserSelectedNperiod ) ;
//////
////////        if (page.getSession().getAttribute(firstTimeAttribute) != null && (selectedStartTimeParam == null && selectedEndTimeParam == null && selectedNperiodParam == null))  {
//////////
////////              System.out.println("...Nothing submitted...");
////////              System.out.println("selectedStarttime " + sessionStartTimeValue);
////////              System.out.println("selectedEndtime " + sessionEndTimeValue);
////////              System.out.println("selectedNperiod " + sessionNperiodValue);
////////
////////        }
////////
////////        System.out.println("...VALUES PASSED BACK TO CALENDAR WIDGET");
////////        System.out.println("selectedStarttime= " + sessionStartTimeValue);
////////        System.out.println("selectedEndtime= " + sessionEndTimeValue);
////////        System.out.println("selectedNperiod= " + sessionNperiodValue);


        // We pass all the information back through the result bean we named 'range' using the variableName
        // we defined as 'myOwnVar'
        // We get things from the request but we cannot put things back into the request.
        page.setAttribute(variableName, new TimeRangeSelectorResultBean(selectedStarttime, selectedEndtime), PageContext.REQUEST_SCOPE);

        System.out.println("...leaving timeRangeLogic");
    }

    private long convertTimeToInternalFormat(long time) {
        String formattedDateString = TimeUtils.toTime(time, getFormat(), "UTC");
        long convertedTime = -1;

        try {
            convertedTime = TimeUtils.toMillis(formattedDateString, getFormat(), "UTC");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertedTime;
    }

    private long periodUnits(String pType, long nperiod) {

//        System.out.println("...processing periodUnits...");
        if (pType.equalsIgnoreCase("mins")) {
            period_units = nperiod * 60 * 1000;
//             System.out.println("Minutes chosen. Units returned " + period_units);
        }
        if (pType.equalsIgnoreCase("hours")) {
            period_units = nperiod * 60 * 60 * 1000;
//             System.out.println("Hours chosen. Units returned " + period_units);
        }
        if (pType.equalsIgnoreCase("days")) {
            period_units = nperiod * 24 * 60 * 60 * 1000;
//             System.out.println("Days chosen. Units returned " + period_units);
        }

        return period_units;
    }

//  SETTERS
    public void setBasename(String b) {
//        System.out.println("Setter method setting basename to " + b);
        basename = b;
    }

    public void setStarttimeObject(Object s) {
//        System.out.println("Setter method setting startime to "+s);
        if (s instanceof Long) {
            starttimelong = ((Long) s).longValue();
        } else if (s instanceof Date) {
            starttimelong = ((Date) s).getTime();
        } else {
            throw new RuntimeException("Unsupported type " + s.getClass());
        }
    }

    public void setEndtimeObject(Object e) {
//        System.out.println("Setter method setting endtime to " + e);
        if (e instanceof Long) {
            endtimelong = ((Long) e).longValue();
        } else if (e instanceof Date) {
            endtimelong = ((Date) e).getTime();
        } else {
            throw new RuntimeException("Unsupported type " + e.getClass());
        }
    }

    public void setPeriod(String p) {
//        System.out.println("Setter method setting period to " + p);
        period = p;
    }

    public void setNperiod(long n) {
//        System.out.println("Setter method setting nperiod to " + n);
        nperiod = n;
    }

    public void setShownone(boolean a) {
        shownone = a;
    }

    public void setShowform(boolean sf) {
        showform = sf;
    }

    public void setShowtime(boolean d) {
        showtime = d;
    }

    public void setVar(String var) {
//        System.out.println("Setter method setting var to " + var);
        variableName = var;
    }

    public void setTimezone(String tz) {
        timezone = tz;
    }

    public void setFormat(String fmt) {
        format = fmt;
    }

    // GETTERS
    public String getBasename() {
//        System.out.println("Getter method returning basename " + basename);
        return basename;
    }

    public long getSelectedStarttime() {
//        System.out.println("Getter method returning selectedStarttime " + selectedStarttime);
        return selectedStarttime;
    }

    public Date getSelectedStartdate() {
//        System.out.println("Getter method returning selectedStarttime " + selectedStarttime);
        return new Date(selectedStarttime);
    }

    public long getStarttime() {
        return starttimelong;
    }

    public Date getSelectedEnddate() {
//        System.out.println("Getter method returning selectedEndtime " + selectedEndtime);
        return new Date(selectedEndtime);
    }

    public long getSelectedEndtime() {
//        System.out.println("Getter method returning selectedEndtime " + selectedEndtime);
        return selectedEndtime;
    }

    public long getEndtime() {
        return endtimelong;
    }

    public String getStarttimeName() {
        return getBasename() + "_starttime";
    }

    public String getEndtimeName() {
        return getBasename() + "_endtime";
    }

    public String getNperiodName() {
        return getBasename() + "_nperiod";
    }

    public long getNperiod() {
        return nperiod;
    }

    public long getSelectedNperiod() {
        return selectedNperiod;
    }

    public String getPeriod() {
        return period;
    }

    public String getTimezone() {
        return timezone;
    }

    public String getFormat() {
        return format;
    }

    public boolean getShowtime() {
        return showtime;
    }

    public boolean getShownone() {
        return shownone;
    }

}
