package org.srs.web.base.utils;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public class HistoryMap extends LinkedHashMap {
    
    int maxSize = 10;
    
    public HistoryMap() {
        super();
    }
    public HistoryMap(int maxSize) {
        this();
        setMaxSize(maxSize);
    }
    
    public int getMaxSize() {
        return maxSize;
    }
    
    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    protected boolean removeEldestEntry(Entry eldest) {
        if ( size() >= getMaxSize() )
            return true;
        return false;
    }
    

//    
//    public Object put(Object key, Object value) {
//        Object r = super.put(key,value);        
//        if ( size() == maxSize ) {
//            super.
//            removeEldestEntry();
//        }
//        return r;
//    }    
//
    
}
