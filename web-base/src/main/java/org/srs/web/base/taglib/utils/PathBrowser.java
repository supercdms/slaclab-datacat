package org.srs.web.base.taglib.utils;

import java.io.Writer;
import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * Result tag for use with Call tag
 * @author turri
 */
public class PathBrowser extends SimpleTagSupport {

    private String path, pathVar;
    private String href, base, queryString;

    public void doTag() throws JspException, IOException {
        Writer writer = getJspContext().getOut();
        if (path == null) {
            writer.write("");
        } else {
            String[] paths = path.split("/");
            String accumulatedPath = "/";

            int count = 0;
            for (int i = 0; i < paths.length ; i++) {
                if (paths[i].equals("")) {
                    continue;
                }
                accumulatedPath += paths[i]+"/";

                String tmpHref = href;
                if (tmpHref == null) {
                    tmpHref = accumulatedPath;
                }
                String delim = "?";
                if (queryString != null && !queryString.equals("")) {
                    tmpHref += delim + queryString;
                    delim = "&";
                }
                if (pathVar != null) {
                    tmpHref += delim + pathVar + "=" + accumulatedPath;
                }
                if ( count > 0 && i < paths.length )
                    writer.write("/");
                writer.write("<a href=\"" + tmpHref + "\"> " + paths[i] + " </a>");
                count++;
            }
        }


    }

    public void setHref(String href) {
        this.href = href;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public void setPathVar(String pathVar) {
        this.pathVar = pathVar;
    }
}
