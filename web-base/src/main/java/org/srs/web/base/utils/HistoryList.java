package org.srs.web.base.utils;

import java.util.ArrayList;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public class HistoryList extends ArrayList {
    
    int maxSize = 10;
    
    public HistoryList() {
    }
    public HistoryList(int maxSize) {
        super();
        setMaxSize(maxSize);
    }
    
    public int getMaxSize() {
        return maxSize;
    }
    
    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public boolean add(Object o) {
        if ( !contains(o) ) {
            add(0,o);
            if ( size() > maxSize )
                remove(maxSize);
            return true;
        }
        return false;
    }
    
}
