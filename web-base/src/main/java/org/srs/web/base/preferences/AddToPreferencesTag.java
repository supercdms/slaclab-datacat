package org.srs.web.base.preferences;

import java.io.IOException;
import java.lang.reflect.Method;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class AddToPreferencesTag extends SimpleTagSupport {
    
    private String name, property, key, value;
    
    public void doTag() throws JspException, IOException {

        Object preferences = PreferencesUtils.getPreferencesBean(name,getJspContext());
        
        String savePreferenceMethodName = "addTo"+property.substring(0,1).toUpperCase()+property.substring(1);
        Class[] classArg = key == null ? new Class[] {String.class} : new Class[] {String.class, String.class};
        
        try {
            Method savePreferencesMethod = preferences.getClass().getMethod(savePreferenceMethodName, classArg );
            Object[] objArgs = key == null ? new Object[] {value} : new Object[] {key,value};
            savePreferencesMethod.invoke(preferences,objArgs);
        } catch (Exception e) {
            throw new JspException(e);
        }
        PreferencesUtils.savePreferences(preferences,name,getJspContext());
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setProperty(String property) {
        this.property = property;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    public void setKey(String key) {
        this.key = key;
    }

}