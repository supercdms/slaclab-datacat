/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.srs.web.base.datasource;

import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.jsp.jstl.core.Config;
import org.srs.web.base.filters.modeswitcher.Mode;
import org.srs.web.base.filters.modeswitcher.ModeSwitcherChangeListener;
import org.srs.web.base.filters.modeswitcher.ModeSwitcherChangedEvent;
import org.srs.web.base.filters.modeswitcher.ModeSwitcherFilter;

/**
 *
 */
public class DefaultDataSourceSwitcher implements HttpSessionListener, ServletRequestListener, ServletContextListener  {

    private String variable, mode;

    public void contextInitialized(ServletContextEvent contextEvent) {
        ServletContext context = contextEvent.getServletContext();
        mode = context.getInitParameter("org.srs.web.base.datasource.mode");
        variable = context.getInitParameter("org.srs.web.base.datasource.variable");

        if (mode == null || variable == null) {
            throw new RuntimeException("Session listener DefaultDataSourceSwitcher has not been configured properly. The following context init parameters must be specified: org.srs.web.base.datasource.mode and org.srs.web.base.datasource.variable");
        }
    }

    public void contextDestroyed(ServletContextEvent arg0) {
    }

    public void sessionCreated(HttpSessionEvent sessionEvent) {
        DefaultDataSourceModeSwitcherListener modeSwitcherListener = new DefaultDataSourceModeSwitcherListener(this,sessionEvent.getSession());
        ModeSwitcherFilter.addModeSwitcherChangeListener(modeSwitcherListener);
    }

    public void sessionDestroyed(HttpSessionEvent sessionEvent) {

    }

    public void requestDestroyed(ServletRequestEvent requestEvent) {
    }

    public void requestInitialized(ServletRequestEvent requestEvent) {
        ServletRequest request = requestEvent.getServletRequest();
        updateDataSource(request);
    }


    String getMode() {
        return mode;
    }

    String getVariable() {
        return variable;
    }

    public void updateDataSource(HttpSession session) {
        String defaultDataSource = ModeSwitcherFilter.getVariable(session, getVariable());
        if ( defaultDataSource != null && ! defaultDataSource.equals("") ) {
            Config.set(session, Config.SQL_DATA_SOURCE, defaultDataSource);
        }
    }
    public void updateDataSource(ServletRequest request) {
        String defaultDataSource = ModeSwitcherFilter.getVariable((HttpServletRequest)request, getVariable());        
        if ( defaultDataSource != null && ! defaultDataSource.equals("") ) {
            Config.set(request, Config.SQL_DATA_SOURCE, defaultDataSource);
        }
    }

    private class DefaultDataSourceModeSwitcherListener implements ModeSwitcherChangeListener {

        private HttpSession session;
        private DefaultDataSourceSwitcher switcher;

        DefaultDataSourceModeSwitcherListener(DefaultDataSourceSwitcher switcher, HttpSession session) {
            this.session = session;
            this.switcher = switcher;
            try {
            switcher.updateDataSource(session);
            } catch (Exception e ) {
                e.printStackTrace();
            }
        }

        public HttpSession getSession() {
            return session;
        }

        public void modeChanged(ModeSwitcherChangedEvent msce) {
            HashMap<String,String> changedModes = msce.getChangedModesList();
            for (String m : changedModes.keySet() ) {
                if (m.equals(mode) || m.equals("experiment")) {
                    switcher.updateDataSource(getSession());
                    switcher.updateDataSource(msce.getRequest());
                    return;
                }
            }
        }
    }
}
