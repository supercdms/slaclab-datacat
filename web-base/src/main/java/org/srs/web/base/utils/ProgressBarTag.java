package org.srs.web.base.utils;

import java.io.IOException;
import java.io.Writer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import org.srs.web.base.filters.modeswitcher.ModeSwitcherFilter;

/**
 *
 * @author The FreeHEP team @ SLAC.
 *
 */
public class ProgressBarTag extends SimpleTagSupport {
    
    private int doneRate, errorRate = 0;
    private int width = 100, height = 10;
    private String doneHref = null, todoHref = null, errorHref;
    private String href = null;
    
    public void doTag() throws JspException, IOException {
        
        Writer writer = getJspContext().getOut();
        
        HttpServletRequest request = (HttpServletRequest)((PageContext)getJspContext()).getRequest();
        String imageServletUrl = ModeSwitcherFilter.getVariable(request, "imageHandlerUrl");
        if ( imageServletUrl == null || "".equals(imageServletUrl) )
            imageServletUrl = "http://srs.slac.stanford.edu/ImageHandler/imageServlet.jsp";

        imageServletUrl += "?name=pb_spacer&experimentName="+ModeSwitcherFilter.getExperimentFromSession(request.getSession(false));


        int successWidth = (int)(width*doneRate/100);
        if ( doneRate != 0 && successWidth == 0 )
            successWidth = 1;
        int errorWidth = (int)(width*errorRate/100);
        if ( errorRate != 0 && errorWidth == 0 )
            errorWidth = 1;
        int otherWidth = width  - successWidth - errorWidth;
        int toDoRate = 100 - doneRate - errorRate;
        
        writer.write("<table height=\""+height+"\" width=\""+width+"\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" >\n");
        writer.write("<tr>\n");
        writer.write("<td style=\"padding: 0\" width=\""+successWidth+"\" bgcolor=\"green\" >\n");
        if ( doneRate > 0 ) {
            if ( getDoneHref() != null )
                writer.write("<a href=\""+getDoneHref()+"\" >\n");
            writer.write("<img width=\""+successWidth+"\" title=\""+doneRate+"% done\" height=\""+height+"\" border=\"0\" src=\""+imageServletUrl+"\" />\n");
            
            if ( getDoneHref() != null )
                writer.write("</a>\n");
            writer.write("</td>\n");
        }
        
        
        writer.write("<td style=\"padding: 0\" width=\""+otherWidth+"\" bgcolor=\"#ffcc00\" >\n");
        if ( toDoRate > 0 ) {
            if ( getTodoHref() != null )
                writer.write("<a href=\""+getTodoHref()+"\" >\n");
            writer.write("<img width=\""+otherWidth+"\" title=\""+toDoRate+"% to do\" height=\""+height+"\" border=\"0\" src=\""+imageServletUrl+"\" />\n");
            if ( getTodoHref() != null )
                writer.write("</a>\n");
        }
        writer.write("</td>\n");
        
        writer.write("<td style=\"padding: 0\" width=\""+errorWidth+"\" bgcolor=\"red\" >\n");
        if ( errorRate > 0 ) {
            if ( getErrorHref() != null )
                writer.write("<a href=\""+getErrorHref()+"\" >\n");
            writer.write("<img width=\""+errorWidth+"\" title=\""+errorRate+"% errors\" height=\""+height+"\" border=\"0\" src=\""+imageServletUrl+"\" />\n");
            if ( getErrorHref() != null )
                writer.write("</a>\n");
        }
        writer.write("</td>\n");
        
        writer.write("</tr>\n");
        writer.write("</table>\n");
        
    }
    
    public void setDonePercentage(int doneRate) {
        if ( doneRate > 100 || doneRate < 0 )
            throw new IllegalArgumentException("Done Rate must be between 0 and 100");
        this.doneRate = doneRate;
    }
    
    public void setErrorPercentage(int errorRate) {
        if ( errorRate > 100 || errorRate < 0 )
            throw new IllegalArgumentException("Done Rate must be between 0 and 100");
        this.errorRate = errorRate;
    }
    
    public void setWidth(int width) {
        this.width = width;
    }
    
    public void setHeight(int height) {
        this.height = height;
    }
    
    public void setDoneHref(String doneHref) {
        this.doneHref = doneHref;
    }
    
    public String getDoneHref() {
        if ( doneHref != null )
            return doneHref;
        return href;
    }
    
    public void setTodoHref(String todoHref) {
        this.todoHref = todoHref;
    }
    
    public String getTodoHref() {
        if ( todoHref != null )
            return todoHref;
        return href;
    }
    
    public void setHref(String href) {
        this.href = href;
    }
    
    public void setErrorHref(String errorHref) {
        this.errorHref = errorHref;
    }
    
    public String getErrorHref() {
        if ( errorHref != null )
            return errorHref;
        return href;
    }
}