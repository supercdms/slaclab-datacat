package org.srs.web.base.datetimepicker;

import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.TimeZone;
import javax.servlet.ServletException;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import org.srs.web.base.filters.modeswitcher.ModeSwitcherFilter;
import org.srs.web.base.utils.TimeUtils;

/**
 * @author The FreeHEP team @ SLAC.
 *
 */
public class DateTimePickerFormElement extends SimpleTagSupport {
    
    private String calendarURL = null;
    
    private boolean showtime = false, shownone=true;
    private String name, value = null, timezone = "UTC";
    private long longValue = -1;
    protected String format = "%d/%m/%Y";
    private int size = 10;
    private static int[] scope = { PageContext.REQUEST_SCOPE, PageContext.PAGE_SCOPE, PageContext.SESSION_SCOPE};
    private boolean skipinclude = false;
    
    
    public void doTag() throws JspException, IOException {
        
        JspContext jspContext = getJspContext();
        Writer writer = jspContext.getOut();


        if ( calendarURL == null ) {
            try {
                calendarURL = ModeSwitcherFilter.getVariable(((PageContext)jspContext).getSession(), "calendarUrl");
            } catch (Exception e) {
                calendarURL = "http://srs.slac.stanford.edu/Commons/scripts/jscalendar-1.0/";
            }

            if ( calendarURL == null )
                calendarURL = "http://srs.slac.stanford.edu/Commons/scripts/jscalendar-1.0/";
        }
        
        if ( jspContext.getAttribute("CalendarJavaScriptsIncluded") == null ) {
            if ( ! skipinclude ) {
                writer.write("<link href=\""+calendarURL+"calendar-win2k-1.css\" rel=\"stylesheet\" type=\"text/css\">\n");
                writer.write("<script type=\"text/javascript\" src=\""+calendarURL+"calendar.js\"></script>\n");
                writer.write("<script type=\"text/javascript\" src=\""+calendarURL+"calendar-setup.js\"></script>\n");
            }
            writer.write("<script type=\"text/javascript\" src=\""+calendarURL+"lang/calendar-en.js\"></script>\n");
            jspContext.setAttribute("CalendarJavaScriptsIncluded","Added");
        }
        
        if ( value == null ) {
            try {
                value = (String)jspContext.getExpressionEvaluator().evaluate("${param."+name+"}",String.class,jspContext.getVariableResolver(),null);
            } catch (javax.servlet.jsp.el.ELException e) {
            }
            if ( value.equals("") )
                value = null;
        }
        
        if ( value == null ) {
            for (int i = 0; i < scope.length; ++i) {
                value = (String) jspContext.getAttribute(name, scope[i]);
                if (value != null)
                    break;
            }
        }
        
        if ( value != null ) {
            try {
                longValue = Long.parseLong(value);
                value = TimeUtils.toTime(longValue,format,timezone);
                longValue = TimeUtils.toMillis(value,format,timezone);
            } catch (Exception e) {
                value = "None";
                longValue = -1;
            }
        } else {
            longValue = -1;
            value = "None";
        }
        
        jspContext.setAttribute(name,String.valueOf(longValue),PageContext.SESSION_SCOPE);
        
        
        
        String hiddenFieldName = name;
        name += "Date";
        String hiddenFieldValue = String.valueOf(longValue);
        
        writer.write("<table border=\"1px\" cellpadding=\"1\" cellspacing=\"0\">\n");
        writer.write("<tr style=\"border : 0px;\">\n");
        writer.write("<td  style=\"border : 0px;\" align=\"left\"><input type=\"text\" id=\""+name+"\" size=\""+size+"\"");
        if ( value != null )
            writer.write(" value=\""+value+"\" ");
        writer.write("name=\""+name+"\" style=\"border : 0px;\" /></td>\n");
        if ( hiddenFieldValue != null )
            writer.write("<input type=\"hidden\" name=\""+hiddenFieldName+"\" id=\""+hiddenFieldName+"\" value=\""+hiddenFieldValue+"\">");
        
        writer.write("<td  style=\"border : 0px;\"><img id=\""+name+"-image\" src=\""+calendarURL+"calendar.gif\" /></td>\n");
        writer.write("</tr>\n");
        writer.write("</table>\n");
        
        writer.write("<script type=\"text/javascript\">\n");
        writer.write("Calendar.setup(\n");
        writer.write("{\n");
        if ( hiddenFieldValue != null )
            writer.write("hiddenField  : \""+hiddenFieldName+"\",\n");
        writer.write("inputField  : \""+name+"\",\n");
        if ( format != null )
            writer.write("ifFormat    : \""+format+"\",\n");
        if ( shownone )
            writer.write("showsNone   : \""+shownone+"\",\n");
        if ( showtime )
            writer.write("showsTime   : \""+showtime+"\",\n");
        Date d = new Date();
        writer.write("timezoneOffset   : "+TimeZone.getTimeZone(timezone).getOffset(d.getTime())+",\n");
        writer.write("now   : "+TimeUtils.now(timezone)+",\n");
        writer.write("button      : \""+name+"-image\"\n");
        writer.write("}\n");
        writer.write(");\n");
        writer.write("</script>\n");
        
        
        
    }
    
    public void setShowtime(boolean showtime) {
        this.showtime = showtime;
    }
    
    public void setShownone(boolean shownone) {
        this.shownone = shownone;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setFormat(String format) {
        this.format = format;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public void setSize(int size) {
        this.size = size;
    }
    
    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
    
    public void setSkipinclude(boolean skipinclude) {
        this.skipinclude = skipinclude;
    }
    
    public void setCalendarUrl(String calendarURL) {
        this.calendarURL = calendarURL;
    }
    
}