package org.srs.web.base.filters.login;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.srs.web.base.db.ConnectionManager;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
//import jakarta.xml.bind.DatatypeConverter;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author bvan
 */
public class HmacValidator implements Filter {
    private String configDb;

    protected HmacValidator(){ }

    private static final String userNameSession = "userName";
    private static final String HMACSHA1 = "HmacSHA1";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.configDb = filterConfig.getInitParameter( "configDb" );
    }

    @Override
    public void destroy(){ }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException{

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        // Invalidate userName (used in groupmanager) for every request
        // Each request must reauthenticate
        httpRequest.getSession().removeAttribute( userNameSession );

        // Check that we have a valid header
        String authHeader = httpRequest.getHeader( "authorization" );
        if(authHeader == null || !authHeader.startsWith( "SRS:" )){
            httpResponse.sendError( HttpURLConnection.HTTP_UNAUTHORIZED , "InvalidAuthHeader" );
            return;
        }

        // Format of SRS:[keyid]:[digest]
        String[] authorization = httpRequest.getHeader( "authorization" ).split( ":" );
        if(authorization.length != 3){
                httpResponse.sendError( HttpURLConnection.HTTP_UNAUTHORIZED , "InvalidAuthHeader" );
            return;
        }
        // Check database
        String userKeyId = authorization[1];
        HmacUserInfo uinfo = null;
        try {
             uinfo = getHmacUserInfo(userKeyId);
        } catch (SQLException | JspException ex){
            Logger.getLogger( HmacValidator.class.getName() ).log( Level.SEVERE, null, ex );
            httpResponse.sendError( HttpURLConnection.HTTP_UNAUTHORIZED , "KeyResolutionError" );
            return;
        }

        String clientDigest = authorization[2];

        boolean success = validateHash(uinfo.secretKey, clientDigest, httpRequest, httpResponse);
        if(success){
            httpRequest.getSession().setAttribute(userNameSession, uinfo.userName);
            chain.doFilter( request, response );
        }
    }

    protected HmacUserInfo getHmacUserInfo(String userKeyId) throws JspException, SQLException{
        try (Connection conn = ConnectionManager.getConnection( configDb )){
            String sql = "select mk.SecretKey, mn.username from UM_MEMBER_HMAC_KEY mk "
                        + "join UM_MEMBER_USERNAME mn on(mn.memidnum = mk.memidnum) "
                        + "where mk.KeyId = ? and SYSDATE < mk.ExpireDate";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, userKeyId);
            ResultSet rs = stmt.executeQuery();
            if(!rs.next()){
                throw new SQLException("User not found for given key");
            }
            return new HmacUserInfo(userKeyId, rs.getString("username"), rs.getString("SecretKey"));
        }
    }

    protected String requestToString(HttpServletRequest request){
        int pathStart = request.getServletPath().length() + request.getContextPath().length();
        StringBuilder fullHeader = new StringBuilder();
        fullHeader.append( request.getMethod() ).append( "\n" );
        fullHeader.append( request.getRequestURI().substring(pathStart) ).append( "\n" );
        fullHeader.append( notNull(request, "content-md5") ).append( "\n" );
        fullHeader.append( notNull(request, "content-type") ).append( "\n" );
        fullHeader.append( request.getHeader( "date" ) ).append( "\n" );
        return fullHeader.toString();
    }

    protected boolean validateHash(String secretKey, String clientDigest,
            HttpServletRequest request,  HttpServletResponse response ) throws IOException {

        boolean success = false;
        // Check that date is valid
        long maxTimeMillis = 5*60*1000; // 5 minutes
        long currentTime = System.currentTimeMillis();
        long clientTime = request.getDateHeader( "date" );
        long clientSkewMillis = 10000;  // Client clock skew is allowed to be ahead by a second
        if((clientTime - clientSkewMillis) > currentTime || (currentTime - clientTime) > maxTimeMillis){
            response.sendError( HttpURLConnection.HTTP_UNAUTHORIZED , "InvalidDateHeader" );
            return success;
        }
        String fullHeader = requestToString(request);

        Mac hmac = null;
        try {
            hmac = Mac.getInstance(HMACSHA1);

            //Get key from database using clientKeyID
            byte[] rawKey = DatatypeConverter.parseBase64Binary( secretKey );
            SecretKey hmacKey = new SecretKeySpec( rawKey, HMACSHA1);

            hmac.init( hmacKey );
            byte[] serverRawDigest = hmac.doFinal( fullHeader.getBytes() );
            String serverDigest = DatatypeConverter.printBase64Binary( serverRawDigest );

            if(!serverDigest.equals( clientDigest )){
                // InvalidKeyDigest
                response.sendError( HttpURLConnection.HTTP_UNAUTHORIZED , "InvalidKeyDigest" );
                return success;
            } else {
                success = true;
            }
        } catch(NoSuchAlgorithmException | InvalidKeyException | IllegalStateException | IOException ex) {
            Logger.getLogger( HmacValidator.class.getName() ).log( Level.SEVERE, null, ex );
            response.sendError( HttpURLConnection.HTTP_INTERNAL_ERROR , "UnknownError" );
        }
        return success;
    }

    // We want an empty string if the header is a null string
    private String notNull(HttpServletRequest request, String header){
        header = request.getHeader( header );
        if(header == null)
            return "";
        return header;
    }

    private static class HmacUserInfo {
        private String userKeyId;
        private String userName;
        private String secretKey;

        public HmacUserInfo(String userKeyId, String userName, String secretKey){
            this.userKeyId = userKeyId;
            this.userName = userName;
            this.secretKey = secretKey;
        }

    }
}
