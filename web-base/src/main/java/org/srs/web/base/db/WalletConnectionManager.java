/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.srs.web.base.db;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import oracle.jdbc.pool.OracleDataSource;

/**
 *
 * @author turri
 */
public class WalletConnectionManager {

    private final OracleDataSource ds = new OracleDataSource();
    private final Properties attributes = new Properties();
    public final static String walletlock = new String("walletlock");

    public WalletConnectionManager(String dbUrl, String userName, String password) throws Exception {
        ClassLoader loader = WalletConnectionManager.class.getClassLoader();
        String odbc = System.getenv("OJDBC_CLASSPATH");
        if (odbc != null) {
            URLClassLoader ucl = (URLClassLoader) loader;
            URL curDir = new File(System.getProperty("user.dir")).toURI().toURL();
            String[] classpath = odbc.split(File.pathSeparator);
            URL[] urls = new URL[ucl.getURLs().length+classpath.length];
            System.arraycopy(ucl.getURLs(), 0, urls, 0, ucl.getURLs().length);
            for (int i=0; i<classpath.length; i++) {
                urls[ucl.getURLs().length+i] = new URL(curDir,classpath[i]);
            }
            for (URL url : urls) {
                System.out.println("url="+url);
            }
            loader = new URLClassLoader(urls,loader.getParent());
            Thread.currentThread().setContextClassLoader(loader);
        }
        ds.setURL(dbUrl);
        if ( userName != null )
            ds.setUser(userName);
        if ( password != null )
            ds.setPassword(password);
        ds.setConnectionCachingEnabled(true);

        System.out.println("Initializing WalletConnectionManager for "+dbUrl+" "+userName+" "+password);
    }


    /**
     * Get a read/write database connection. Don't forget to close it when you are done.
     */
    public Connection getConnection() throws SQLException {
        return getConnection(false);
    }
    /**
     * Get a database connection. Don't forget to close it when you are done.
     */
    public Connection getConnection(boolean readOnly) throws SQLException {
        Connection conn =  null;
        synchronized (walletlock) {
            conn = ds.getConnection(attributes);
        }
        return conn;
    }

    public void close() throws SQLException {
        ds.close();
    }

}
