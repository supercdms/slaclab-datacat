/*
 * DataMonitoringApplicationTableDecorator.java
 *
 * Created on July 6, 2007, 12:27 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package org.srs.web.base.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.servlet.jsp.PageContext;
import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public class NumberDecorator implements DisplaytagColumnDecorator {
    
    public static NumberFormat format = new DecimalFormat("#######0.000");

    public Object decorate(Object object, PageContext pageContext, MediaTypeEnum mediaTypeEnum) throws DecoratorException {
        return format.format(object);
    }        
}
