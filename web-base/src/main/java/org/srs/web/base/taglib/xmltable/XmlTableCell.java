/*
 * XmlTableCell.java
 *
 * Created on March 2, 2007, 2:34 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package org.srs.web.base.taglib.xmltable;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public class XmlTableCell implements Comparable {
    
    private String value = null;
    private XmlTableColumn col;
    
    
    public XmlTableCell(String value, XmlTableColumn col) {
        this.value = value;
        this.col = col;
    }
    
    public String toString() {
        return value;
    }
  
    public int compareTo(Object o) {
        if ( o instanceof XmlTableCell ) {
            
        }
        return value.compareTo(o.toString() );
    }    
    
    private int compare(String v1, String v2) {
        if ( col.getType().equals("double") )
            return Double.valueOf(v1).compareTo(Double.valueOf(v2));
        if ( col.getType().equals("int") )
            return Integer.valueOf(v1).compareTo(Integer.valueOf(v2));
        return v1.compareTo(v2);
    }
}
