package org.srs.web.base.frames;

import java.io.IOException;
import java.io.Writer;
import java.util.StringTokenizer;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * @author The FreeHEP team @ SLAC.
 *
 */
public class FitToScreenInternalFrame extends SimpleTagSupport {
    
    private String names;
    
    public void doTag() throws JspException, IOException {
        
        JspContext jspContext = getJspContext();
        Writer writer = jspContext.getOut();
        
        writer.write("\n");
        writer.write("<script type=\"text/javascript\">\n");
        writer.write("        function maximiseframe() {\n");
        writer.write("           var frames = new Array(");
        StringTokenizer tt = new StringTokenizer(names,",");
        while (tt.hasMoreTokens()) {
            String t = tt.nextToken();
            writer.write("\""+t+"\"");
            if ( tt.hasMoreTokens() )
                writer.write(",");
        }
        writer.write(");\n");
        writer.write("           var doc = this.document;\n");
        writer.write("           var documentHeight = doc.documentElement.clientHeight;\n");
        writer.write("           var size = documentHeight - 20;\n");
        writer.write("           var headerElement = doc.getElementById(\"headerElement\");\n");
        writer.write("           if ( headerElement )\n");
        writer.write("              size = size - headerElement.clientHeight;\n");
        writer.write("           var footerElement = doc.getElementById(\"footerElement\");\n");
        writer.write("           if ( footerElement )\n");
        writer.write("              size = size - footerElement.clientHeight;\n");
        writer.write("           var i;\n");
        writer.write("           for ( i in frames ) {\n");
        writer.write("              var frame = doc.getElementById(frames[i]);\n");
        writer.write("              frame.height = size;\n");
        writer.write("           }\n");
        writer.write("        }\n");
        writer.write("\n");
        writer.write("        if (window.addEventListener) {\n");
        writer.write("           window.addEventListener(\"load\", maximiseframe, false)\n");
        writer.write("           window.addEventListener(\"resize\", maximiseframe, false)\n");
        writer.write("        }\n");
        writer.write("        else if (window.attachEvent) {\n");
        writer.write("           window.attachEvent(\"onload\", maximiseframe)\n");
        writer.write("           window.attachEvent(\"onresize\", maximiseframe)\n");
        writer.write("        }\n");
        writer.write("        else {\n");
        writer.write("           window.onload=maximiseframe\n");
        writer.write("           window.onresize=maximiseframe\n");
        writer.write("        }\n");
        writer.write("</script>\n");
        writer.write("\n");
    }
    
    public void setIds(String names) {
        this.names = names;
    }
}