package org.srs.web.base.filters.login;

import java.io.Serializable;
import javax.servlet.http.HttpSession;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public interface LoginStatusListener extends Serializable {
    
    public void loginStatusChanged(LoginStatusChangedEvent e);

    public HttpSession getSession();
    
}
