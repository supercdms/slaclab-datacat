package org.srs.web.base.filters.refresh;

/**
 *
 * @author The FreeHEP team @ SLAC.
 */
public class RefreshPreferencesBean {
    
    public int refreshRate = -1;
    
    public int getRefreshRate() {
        if ( refreshRate < 0 )
            return RefreshFilter.getRefreshRate();
        return refreshRate;
    }
    
    public void setRefreshRate(int refreshRate) {
        if ( RefreshFilter.checkRefreshRate(refreshRate) )
            this.refreshRate = refreshRate;
        else
            throw new IllegalArgumentException("Refresh Rate cannot be less than "+RefreshFilter.getMinRefreshRate()+" seconds.");
    }
    
    
}
