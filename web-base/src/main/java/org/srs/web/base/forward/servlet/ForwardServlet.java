/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.srs.web.base.forward.servlet;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 */
public class ForwardServlet extends HttpServlet {

    private String forwardURL;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        forwardURL = config.getInitParameter("forwardURL");
        if (forwardURL == null) {
            throw new ServletException("Parameter \"forwardURL\" must be provided when initializing the ForwardServlet ");
        }
        if ( ! forwardURL.endsWith("/") )
            forwardURL += "/";
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        String requestPathInfo = request.getPathInfo();
        if ( requestPathInfo == null )
            requestPathInfo = "";
        if ( requestPathInfo.startsWith("/") )
            requestPathInfo = requestPathInfo.substring(1);

        String queryString = request.getQueryString();
        if ( queryString != null && ! queryString.equals("") )
            requestPathInfo += "?"+queryString;

        String redirect = forwardURL+requestPathInfo;
        resp.sendRedirect(redirect);
    }
}
