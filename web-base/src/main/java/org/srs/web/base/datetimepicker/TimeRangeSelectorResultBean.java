package org.srs.web.base.datetimepicker;

import java.util.Date;

public class TimeRangeSelectorResultBean {

    private long selectedStarttime = -1, selectedEndtime = -1;

    public TimeRangeSelectorResultBean(long start, long end) {
        selectedStarttime = start;
        selectedEndtime = end;
    }


//    public long getSelectedStarttime() {
//        return selectedStarttime;
//    }
//
    public Date getStart() {
        if ( selectedStarttime != -1 )
            return new Date(selectedStarttime);
        return null;
    }

    public Date getEnd() {
        if ( selectedEndtime != -1 )
            return new Date(selectedEndtime);
        return null;
    }

//    public long getSelectedEndtime() {
//        return selectedEndtime;
//    }

}
