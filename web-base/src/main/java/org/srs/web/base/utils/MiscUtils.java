/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.srs.web.base.utils;

/**
 *
 * @author turri
 */
public class MiscUtils {

    public static String pathBrowser(String path, String basePath) {
        if ( path == null )
            return "";
        String[] paths = path.split("/");
        String returnString = "";
        String accumulatedPath = "";
        if ( basePath != null )
            accumulatedPath = basePath;
        for ( int i = 0; i < paths.length-1; i++ ) {
            if ( paths[i].equals("") )
                continue;
            accumulatedPath += "/"+paths[i];
            returnString += "<a href=\""+accumulatedPath+"\">/"+paths[i]+"</a>";
        }
        return returnString+"/"+paths[paths.length-1];
    }

}
