
package org.srs.web.base.filters.login;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.PageContext;
import static org.srs.web.base.filters.login.LoginFilter.USERNAME_SESSION_KEY;

/**
 *
 * @author bvan
 */
public class LoginUtils {
    
    private LoginUtils(){}

    public static void addLoginStatusListener(LoginStatusListener l) {
        getLoginStatusListenerList(l.getSession()).add(l);
    }

    public static String userName(HttpServletRequest request) {
        Object userObj = userNameObject(request);
        if (userObj != null) {
            return (String) userObj;
        }
        return "";
    }
    
    public static String userName(JspContext context) {
        Object userName = context.getAttribute(USERNAME_SESSION_KEY, PageContext.SESSION_SCOPE);
        if (userName == null) {
            return "";
        }
        return (String) userName;
    }

    protected static List getLoginStatusListenerList(HttpSession session) {
        List listeners = (List) session.getAttribute("LoginStatusListenersList");
        if (listeners == null) {
            listeners = new ArrayList();
            session.setAttribute("LoginStatusListenersList", listeners);
        }
        return listeners;
    }
    
    private static Object userNameObject(HttpServletRequest request) {
        return request.getSession().getAttribute(USERNAME_SESSION_KEY);
    }

}
