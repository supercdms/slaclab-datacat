package org.srs.web.base.filters.cookies;

import java.io.IOException;
import java.io.Writer;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class CreateCookiesTag extends SimpleTagSupport {
    
    public void doTag() throws JspException, IOException {
        HttpServletRequest request = (HttpServletRequest) ((PageContext)getJspContext()).getRequest();
        HttpServletResponse response = (HttpServletResponse) ((PageContext)getJspContext()).getResponse();
        
        String cookieName = "";
        String requestCookieName = request.getParameter("cookieName");
        String sessionCookieName = (String)request.getSession().getAttribute("cookieName");        
        if ( sessionCookieName != null )
            cookieName = sessionCookieName;        
        if ( requestCookieName != null )
            cookieName = requestCookieName;
        request.getSession().setAttribute("cookieName",cookieName);
        
        String cookieAge = "";
        String requestcookieAge = request.getParameter("cookieAge");
        String sessioncookieAge = (String)request.getSession().getAttribute("cookieAge");        
        if ( sessioncookieAge != null )
            cookieAge = sessioncookieAge;        
        if ( requestcookieAge != null )
            cookieAge = requestcookieAge;
        request.getSession().setAttribute("cookieAge",cookieAge);

        String cookieValue = "";
        String requestcookieValue = request.getParameter("cookieValue");
        String sessioncookieValue = (String)request.getSession().getAttribute("cookieValue");        
        if ( sessioncookieValue != null )
            cookieValue = sessioncookieValue;        
        if ( requestcookieValue != null )
            cookieValue = requestcookieValue;
        request.getSession().setAttribute("cookieValue",cookieValue);
        
        String cookiePath = "";
        String requestcookiePath = request.getParameter("cookiePath");
        String sessioncookiePath = (String)request.getSession().getAttribute("cookiePath");        
        if ( sessioncookiePath != null )
            cookiePath = sessioncookiePath;        
        if ( requestcookiePath != null )
            cookiePath = requestcookiePath;
        request.getSession().setAttribute("cookiePath",cookiePath);
        
        String cookieDomain = "";
        String requestcookieDomain = request.getParameter("cookieDomain");
        String sessioncookieDomain = (String)request.getSession().getAttribute("cookieDomain");        
        if ( sessioncookieDomain != null )
            cookieDomain = sessioncookieDomain;        
        if ( requestcookieDomain != null )
            cookieDomain = requestcookieDomain;
        request.getSession().setAttribute("cookieDomain",cookieDomain);
        
        
        
        Writer writer = response.getWriter();
        writer.write("<table class=\"filterTable\">");
        writer.write("<form>");
        writer.write("<tr>");        
        writer.write("<th>");        
        writer.write("Name: ");
        writer.write("</th>");        
        writer.write("<td>");        
        writer.write("<input type=\"text\" value=\""+cookieName+"\" name=\"cookieName\" />");
        writer.write("</td>");        
        writer.write("</tr>");        
        writer.write("<tr>");        
        writer.write("<th>");        
        writer.write("Value: ");
        writer.write("</th>");        
        writer.write("<td>");        
        writer.write("<input type=\"text\" value=\""+cookieValue+"\" name=\"cookieValue\" />");
        writer.write("</td>");        
        writer.write("</tr>");        
        writer.write("<tr>");        
        writer.write("<th>");        
        writer.write("Age: ");
        writer.write("</th>");        
        writer.write("<td>");        
        writer.write("<input type=\"text\" value=\""+cookieAge+"\" name=\"cookieAge\" />");
        writer.write("</td>");        
        writer.write("</tr>");        
        writer.write("<tr>");        
        writer.write("<th>");        
        writer.write("Path: ");
        writer.write("</th>");        
        writer.write("<td>");        
        writer.write("<input type=\"text\" value=\""+cookiePath+"\" name=\"cookiePath\" />");
        writer.write("</td>");        
        writer.write("</tr>");        
        writer.write("<tr>");        
        writer.write("<th>");        
        writer.write("Domain: ");
        writer.write("</th>");        
        writer.write("<td>");        
        writer.write("<input type=\"text\" value=\""+cookieDomain+"\" name=\"cookieDomain\" />");
        writer.write("</td>");        
        writer.write("</tr>");        
        writer.write("<tr>");        
        writer.write("<td colspan=\"2\">");        
        writer.write("<input type=\"submit\" value=\"Create Cookie\" name=\"CreateCookie\" />");        
        writer.write("</td>");        
        writer.write("</tr>");        
        writer.write("</form>");
        writer.write("</table>");
        
        if ( ! cookieName.equals("") && request.getParameter("CreateCookie") != null) {
            writer.write("<font color=\"red\">Cookie Created</font>");
            Cookie c = new Cookie(cookieName, cookieValue);
            
            if ( ! cookieAge.equals("") ) {
                try {
                    int age = Integer.valueOf(cookieAge).intValue();
                    c.setMaxAge(age);
                } catch (Exception e) {
                }
            }
            
            if ( ! cookiePath.equals("") )
                c.setPath(cookiePath);
            if ( ! cookieDomain.equals("") )
                c.setPath(cookieDomain);
            response.addCookie(c);
        }
        
    }
    
}