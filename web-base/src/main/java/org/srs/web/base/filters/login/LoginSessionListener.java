/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.srs.web.base.filters.login;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author turri
 */
public class LoginSessionListener implements HttpSessionListener {

    public void sessionCreated(HttpSessionEvent event) {
    }

    public void sessionDestroyed(HttpSessionEvent event) {
        LoginFilter.cleanupDestroyedSession(event.getSession());
    }

}
