package org.srs.web.base.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author The FreeHEP team @ SLAC.
 *
 */
public class FileInclude extends SimpleTagSupport {
    
    private String fileName = null;
    private boolean escapeXml = false;
    
    public void doTag() throws JspException, IOException {
        
        
        Writer writer = getJspContext().getOut();
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line;
        while( (line = reader.readLine()) != null ) {
            if ( escapeXml ) {
                line = line.replace("<","&lt;");
                line = line.replace(">","&gt;");
            }
            writer.write(line+"\n");
        }
    }
    
    public void setFilename(String fileName) {
        this.fileName = fileName;
    }
    
    public void setEscapeXml(boolean escapeXml) {
        this.escapeXml = escapeXml;
    }
}