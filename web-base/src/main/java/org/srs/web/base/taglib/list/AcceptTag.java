package org.srs.web.base.taglib.list;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspTag;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author tonyj
 * @version $Id: AcceptTag.java,v 1.1 2008/01/11 02:28:52 tonyj Exp $
 */

public class AcceptTag extends SimpleTagSupport
{
   private boolean accept;
   /**Called by the container to invoke this tag.
    * The implementation of this method is provided by the tag library developer,
    * and handles all tag processing, body iteration, etc.
    */
   public void doTag() throws JspException
   {
      JspTag tag = this.getParent();
      if (tag instanceof ListFilterTag) ((ListFilterTag) tag).accept(accept); 
   }
   public void setTest(boolean value)
   {
      accept = value;
   }
}
