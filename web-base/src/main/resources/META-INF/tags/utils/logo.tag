<%@tag description="Tag to import the menu bar" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@attribute name="url" required="false"%>
<%@attribute name="externalUrl" required="false"%>
<%@attribute name="topheader" required="false"%>
<%@attribute name="title" required="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://srs.slac.stanford.edu/utils" prefix="srs_utils" %>



<c:if test="${ empty url }">
    <c:set var="url" value="${appVariables.imageHandlerUrl}"/>
</c:if>

<c:if test="${ empty url }">
    <c:set var="url" value="http://srs.slac.stanford.edu/ImageHandler/imageServlet.jsp"/>
</c:if>

<c:choose>
    <c:when test="${ ! empty topheader }" >
        <c:set var="experimentName" value="${topheader}"/>
    </c:when>
    <c:otherwise>
        <c:set var="experimentName" value="${appVariables.experiment}"/>
    </c:otherwise>
</c:choose>



<c:if test="${experimentName == 'Fermi'}">
    <c:set var="experimentName" value="${experimentName} LAT"/>
</c:if>

<c:url var="titleUrl" value="${fn:replace(url,'imageServlet','titleServlet')}">
    <c:param name="experimentName" value="${experimentName}"/>
    <c:param name="title" value="${title}"/>
</c:url>


<c:if test="${! empty externalUrl}">
    <a href="${externalUrl}">
</c:if>
        
<table>
    <tr>
        <td align="middle">
            <srs_utils:image url="${url}" name="logo" height="${initParam.imageHeight}"/>
        </td>
        <td align="middle">
            <img src="${titleUrl}" <c:if test="${! empty initParam.titleHeight}">height="${initParam.titleHeight}"</c:if> />
        </td>
    </tr>
</table>

<c:if test="${! empty externalUrl}">
    </a>
</c:if>
