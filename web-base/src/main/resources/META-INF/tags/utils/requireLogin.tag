<%@tag description="header decorator" pageEncoding="UTF-8"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty userName}">
    <c:set var="url" value="${pageContext.request.requestURL}"/>

    <c:choose>
        <c:when test="${fn:contains(url,'?')}">
            <c:set var="url" value="${url}&"/>
        </c:when>
        <c:otherwise>
            <c:set var="url" value="${url}?"/>
        </c:otherwise>
    </c:choose>

    <c:if test="${! empty pageContext.request.queryString}">
        <c:set var="url" value="${url}${pageContext.request.queryString}&"/>
    </c:if>

    <c:redirect url="${url}login=true" />
</c:if>
