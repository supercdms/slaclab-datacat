<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="srs_utils" uri="http://srs.slac.stanford.edu/utils" %>

<%@attribute name="url" required="true"%>
<%@attribute name="name" required="true"%>
<%@attribute name="iswelcome" required="false"%>

<c:if test="${ empty currentPage }" >
    <c:set var="currentPage" value="${srs_utils:currentPage(pageContext)}" />
</c:if>

<c:choose>
    <c:when test="${ (empty currentPage && iswelcome) || ( ! empty currentPage && fn:contains(currentPage,url)) }">
        <b>${name}</b>
    </c:when>
    <c:otherwise>
        <a href="${url}" target="_top">${name}</a>
    </c:otherwise>
</c:choose>
