<%@tag description="Tag to import the menu bar" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@attribute name="url" required="false"%>
<%@attribute name="name" required="true"%>
<%@attribute name="height" required="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<c:if test="${ empty url }">
    <c:set var="url" value="${appVariables.imageHandlerUrl}"/>
</c:if>

<c:if test="${ empty url }">
    <c:set var="url" value="http://srs.slac.stanford.edu/ImageHandler/imageServlet.jsp"/>
</c:if>

<c:set var="experimentName" value="${appVariables.experiment}"/>

<c:url var="imageServletUrl" value="${url}">
    <c:param name="experimentName" value="${experimentName}"/>
    <c:param name="name" value="${name}"/>
</c:url>

<img src="${imageServletUrl}"  <c:if test="${! empty height}">height="${height}"</c:if>/>
