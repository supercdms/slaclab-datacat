<%@tag description="Tag to display and manipulate data info" pageEncoding="UTF-8"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@attribute name="url" required="false"%>
<%@attribute name="useQueryString" required="false"%>

<c:if test="${ empty url }">
    <c:set var="url" value=""/>
</c:if>

<c:if test="${ empty useQueryString }">
    <c:set var="useQueryString" value="false"/>
</c:if>

<c:choose>
    <c:when test="${fn:contains(url,'?')}">
        <c:set var="url" value="${url}&"/>
    </c:when>
    <c:otherwise>
        <c:set var="url" value="${url}?"/>        
    </c:otherwise>
</c:choose>

<c:if test="${useQueryString}">
    <c:if test="${! empty pageContext.request.queryString}">
        <c:set var="url" value="${url}${pageContext.request.queryString}&"/>        
    </c:if>
</c:if>
<c:choose>
    <c:when test="${empty userName}">
        <a href="${url}login=true"  target="_top" >Login</a>
    </c:when>
    <c:otherwise>
        User: ${userName}&nbsp;.&nbsp;(<a href="${url}login=renew"  target="_top" >Switch</a>|<a href="?login=false"  target="_top" >Logout</a>)
    </c:otherwise>
</c:choose>                                        
