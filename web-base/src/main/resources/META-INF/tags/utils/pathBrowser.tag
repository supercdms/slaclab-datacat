<%@tag description="Tag to display and manipulate data info" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@attribute name="path" required="true"%>
<%@attribute name="url" required="true"%>
<%@attribute name="parname" required="true"%>


<c:set var="paths" value="${fn:split(path,'/')}"/>
<c:if test="${fn:length(paths) > 1}">
    <c:forEach var="i"  begin="0" end="${fn:length(paths)-2}" >
        <c:set var="parentPath" value=""/>
        <c:forEach var="j"  begin="0" end="${fn:length(paths)-2}" >                                    
            <c:set var="parentPath" value="${parentPath}/${paths[j]}"/>
        </c:forEach>
        <c:url value="${url}" var="linkUrl">
            <c:param name="${parname}" value="${parentPath}"/>
        </c:url>
        <a href="${linkUrl}">/${paths[i]}</a>
    </c:forEach>
</c:if>
/${paths[fn:length(paths)-1]}
