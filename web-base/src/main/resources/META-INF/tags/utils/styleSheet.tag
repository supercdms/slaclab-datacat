<%@tag description="Tag to import the menu bar" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@attribute name="url" required="false"%>




<c:if test="${ empty url }">
    <c:set var="url" value="${appVariables.styleSheetUrl}"/>
</c:if>

<c:if test="${ empty url }">
    <c:set var="url" value="http://srs.slac.stanford.edu/Commons/css/srsCommons.jsp"/>
</c:if>

<c:url var="styleUrl" value="${url}">
    <c:param name="experimentName" value="${appVariables.experiment}"/>
</c:url>


<link href="${styleUrl}" rel="stylesheet" type="text/css">
