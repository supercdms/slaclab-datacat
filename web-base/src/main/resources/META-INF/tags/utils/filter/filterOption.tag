<%@tag description="An option for use with filterSelection" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@attribute name="value"%>
<jsp:doBody var="option"/>
<c:set var="optionSelected" value="false"/>
<c:set var="valueOrOption" value="${empty value ? option : value}"/>
<c:forEach var='val' items='${selectedValues}'> 
    <c:if test="${val == valueOrOption}">
        <c:set var="optionSelected" value="true"/>
    </c:if>
</c:forEach>
<option value="${valueOrOption}" ${optionSelected?'selected':''}>${option}</option>

