<%@tag description="A filter based on text input" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@attribute name="var" required="true" rtexprvalue="false"%>
<%@attribute name="size" type="java.lang.Integer"%>
<%@attribute name="title" required="true"%>
<%@attribute name="defaultValue" %>
<%@variable scope="AT_END" name-from-attribute="var" alias="result" %>

<c:set var="result" value="${reset || empty param[title] ? (empty defaultValue ? '' : defaultValue) : param[title]}"/>
<td>${title} <input type="text" name="${title}" value="${result}" size="${empty size ? 10 : size}"></td>
