<%@tag description="A filter based on selecting one from a set of choices" pageEncoding="UTF-8" body-content="scriptless"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@attribute name="var" required="true" rtexprvalue="false"%>
<%@attribute name="title" required="true"%>
<%@attribute name="multiple" required="false"%>
<%@attribute name="size" required="false"%>
<%@attribute name="defaultValue"%>
<%@variable scope="AT_BEGIN" name-from-attribute="var" alias="result" variable-class="boolean"%>
<%@variable name-given="selectedValues" scope="NESTED" %>

<c:set var="isMultiple" value="${'true' eq multiple}"/>
<c:set var="selectSize" value="${isMultiple ? !empty size ? size : 4 : 1}"/>


<c:set var="result" value="${reset || empty param[title] ? defaultValue : param[title]}"/>
<c:set var="selectedValues" value="${reset || empty paramValues[title] ? defaultValue : paramValues[title]}" scope="request"/>
<td>${title} <select name="${title}" size="${selectSize}" <c:if test="${isMultiple}">multiple</c:if>><jsp:doBody/></select></td>
