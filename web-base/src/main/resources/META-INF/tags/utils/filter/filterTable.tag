<%@tag description="A simple table for filtering queries" pageEncoding="UTF-8" body-content="scriptless"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@variable name-given="reset" scope="NESTED" %>
<c:set var="reset" value="${empty param.submit}" scope="request"/>

<form name="filterform">
    <table class="filtertable">
        <tr>
            <jsp:doBody/>
            <td><input type="submit" value="Filter" name="submit">&nbsp;<input type="submit" value="Reset Defaults" name="reset"></td>
        </tr>
    </table>
</form>