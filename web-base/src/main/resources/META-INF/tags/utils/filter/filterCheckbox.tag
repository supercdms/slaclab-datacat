<%@tag description="A filter based on a checkbox" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@attribute name="var" required="true" rtexprvalue="false"%>
<%@attribute name="title" required="true"%>
<%@attribute name="defaultValue"  type="java.lang.Boolean" %>
<%@variable scope="AT_END" name-from-attribute="var" alias="result" variable-class="boolean"%>

<c:set var="result" value="${reset ? (empty defaultValue ? false : defaultValue) : !empty param[title]}"/>
<td>${title} <input type="checkbox" name="${title}" ${result?'checked':''}></td>
