<%@tag description="Generate even/odd table rows" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@attribute name="reset" type="java.lang.Boolean"%>

<c:set var="evenOdd" scope="request" value="${reset ? 1 : evenOdd+1}"/>
<tr class="${evenOdd%2==0 ? 'even' : 'odd'}"><jsp:doBody/></tr>