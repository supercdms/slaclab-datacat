<%@tag description="Tag to display and manipulate data info" pageEncoding="UTF-8" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@taglib prefix="utils" uri="http://srs.slac.stanford.edu/utils" %>
<%@taglib prefix="time" uri="http://srs.slac.stanford.edu/time"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<%@attribute name="basename" required="true" rtexprvalue="true" %>
<%@attribute name="var" required="true" rtexprvalue="false" %>
<%@attribute name="period" required="false" rtexprvalue="true" %>
<%@attribute name="nperiod" required="false" rtexprvalue="true" %>
<%@attribute name="shownone" required="false" rtexprvalue="true" %>
<%@attribute name="showform" required="false" rtexprvalue="true" %>
<%@attribute name="showtime" required="false" rtexprvalue="true" %>
<%@attribute name="format" required="false" rtexprvalue="true" %>
<%@attribute name="starttime" required="false" rtexprvalue="true" type="java.lang.Object" %>
<%@attribute name="endtime" required="false" rtexprvalue="true" type="java.lang.Object" %>
<%@attribute name="timezone" required="false" rtexprvalue="true" %>

<%-- invoke the logic on the bean.
We invoke jsp:useBean and make id 'range' as our way of accessing the java code,
TimeRangeSelectorBean.java. We set the properties based on what came from the request.

The useBean tag looks for an instance of TimeRangeSelectorBean in the session. If the instance is
already there it will update the old instance. If it's not there the bean will create a new instance
of TimeRangeSelectorBean and put it into the session. The setProperty tags automatically collect the data
from the form, match the form names to the getter/setter methods and place the data into the bean,
TimeRangeSelectorBean. The form data is always collected by the bean.
--%>

<jsp:useBean id = "range" class="org.srs.web.base.datetimepicker.TimeRangeSelectorBean"/>

<c:set var="showMsg" value="0"/>

<%-- setProperty gets the input form data, matches names against the bean method names and places
the data into the bean. --%>
<c:if test="${! empty basename}">
    <jsp:setProperty name="range" property="basename" value="${basename}"/>
    <c:if test="${showMsg == 1}">
        <c:out value="trsTag:setProperty basename =  ${basename}" />
    </c:if>
</c:if>
<c:if test="${! empty var}">
    <jsp:setProperty name="range" property="var" value="${var}"/>
    <c:if test="${showMsg == 1}">
        <c:out value="trsTag:setProperty var = ${var}" />
    </c:if>
</c:if>
<c:if test="${! empty starttime}">
    <jsp:setProperty name="range" property="starttimeObject" value="${starttime}"/>
    <c:if test="${showMsg == 1}">
        <c:out value="trsTag:setProperty starttime =  ${starttime}" />
    </c:if>
</c:if>
<c:if test="${! empty endtime}">
    <jsp:setProperty name="range" property="endtimeObject" value="${endtime}"/>
    <c:if test="${showMsg == 1}">
        <c:out value="trsTag:setProperty endtime =  ${endtime}" />
    </c:if>
</c:if>
<c:if test="${! empty period}">
    <jsp:setProperty name="range" property="period" value="${period}"/>
    <c:if test="${showMsg == 1}">
        <c:out value="trsTag:setProperty period =  ${period}" />
    </c:if>
</c:if>

<c:if test="${empty period}">
    <c:set var="period" value="mins"/>
    <jsp:setProperty name="range" property="period" value="${period}"/>
    <c:if test="${showMsg == 1}">
        <c:out value="trsTag:setProperty period =  ${period}" />
    </c:if>
</c:if>

<c:if test="${! empty nperiod}">
    <jsp:setProperty name="range" property="nperiod" value="${nperiod}"/>
    <c:if test="${showMsg == 1}">
        <c:out value="trsTag:setProperty nperiod =  ${nperiod}" />
    </c:if>
</c:if>
<c:if test="${empty nperiod}">
    <c:set var="nperiod" value="-1"/>
    <jsp:setProperty name="range" property="nperiod" value="-1"/>
    <c:if test="${showMsg == 1}">
        <c:out value="trsTag:setProperty nperiod =  ${nperiod}" />
    </c:if>
</c:if>
<c:if test="${! empty showform}">
    <jsp:setProperty name="range" property="showform" value="${showform}"/>
    <c:if test="${showMsg == 1}">
        <c:out value="trsTag:setProperty showform =  ${showform}" />
    </c:if>
</c:if>
<c:if test="${empty showform}">
    <jsp:setProperty name="range" property="showform" value="true"/>
    <c:set var="showform" value="true"/>
    <c:if test="${showMsg == 1}">
        <c:out value="trsTag:setProperty showform = ${showform}" />
    </c:if>
</c:if>
<c:if test="${! empty shownone}">
    <jsp:setProperty name="range" property="shownone" value="${shownone}"/>
    <c:if test="${showMsg == 1}">
        <c:out value="trsTag:setProperty shownone =  ${shownone}"/>
    </c:if>
</c:if>
<c:if test="${! empty showtime}">
    <jsp:setProperty name="range" property="showtime" value="${showtime}"/>
    <c:if test="${showMsg == 1}">
        <c:out value="trsTag:setProperty showtime =  ${showtime}"/>
    </c:if>
</c:if>
<c:if test="${! empty timezone}">
    <jsp:setProperty name="range" property="timezone" value="${timezone}"/>
    <c:if test="${showMsg == 1}">
        <c:out value="trsTag:setProperty timezone =  ${timezone}"/>
    </c:if>
</c:if>
<c:if test="${! empty format}">
    <jsp:setProperty name="range" property="format" value="${format}"/>
    <c:if test="${showMsg == 1}">
        <c:out value="trsTag:setProperty format =  ${format}"/>
    </c:if>
</c:if>



<%--
The jsp calls this tag and we pass the bean, "range", and the pageContext object
to the method invokeTimeRangeSelectionLogic in TimeRangeSelectorBean.java.
--%>
${time:invokeTimeRangeSelectionLogic(range,pageContext)}

<c:if test = "${showform}">
    <form method="get" >
    </c:if>
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <time:dateTimePicker size="20"  name="${range.starttimeName}" value="${range.selectedStarttime}" shownone="${range.shownone}" showtime="${range.showtime}" format="${range.format}" timezone="${range.timezone}"/>
                        </td><td>
                            <time:dateTimePicker size="20" name="${range.endtimeName}" value="${range.selectedEndtime}" shownone="${range.shownone}" showtime="${range.showtime}" format="${range.format}" timezone="${range.timezone}"/>
                        </td><td>
                            ${range.period}  <input type="text" size="4" name ="${range.nperiodName}" value="${range.selectedNperiod > 0 ? range.selectedNperiod : ''}">
                        </td>                        
                    </tr>
                </table>
            </td>
        <c:if test = "${showform}">
                <td>
                    <input type="submit" value="Submit" name="submit" />
                </td>
            </c:if>
        </tr>
    </table>
    <c:if test = "${showform}">
    </form>
</c:if>

   