<%@tag description="Tag to display and manipulate data info" pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@taglib prefix="utils" uri="http://srs.slac.stanford.edu/utils" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@attribute name="url" required="true"%>


<c:import url="${url}" var="input"/>
<c:catch var="error">            
    <c:set var="model" value="${utils:xmltable(input)}" />
    <%--
    <c:choose>
        <c:when test="${ ! empty model.decorator && model.decorator != null }">
            <display:table class="datatable" name="${model.rows}"  defaultsort="${model.defaultsort}" defaultorder="${model.defaultorder}" decorator="${model.decorator}"  >
        </c:when>
        <c:otherwise>
        </c:otherwise>
    </c:choose>
        --%>
    <display:table class="datatable" name="${model.rows}"  defaultsort="${model.defaultsort}" defaultorder="${model.defaultorder}"  >
        <c:if test="${model.caption != null && ! empty model.caption}">
            <display:caption>
                ${model.caption}
            </display:caption>
        </c:if>
        <c:forEach items="${model.columns}" var="col">
            <c:choose>
                <c:when test="${ ! empty col.group && col.group != null }">
                    <c:choose>
                        <c:when test="${ ! empty col.decorator && col.decorator != null }">
                            <c:choose>
                                <c:when test="${ ! empty col.classstr && col.classstr != null }">
                                    <display:column title="${col.title}" sortable="${col.sortable}" property="${col.name}" class="${col.classstr}" decorator="${col.decorator}" group="${col.group}"  />
                                </c:when>
                                <c:otherwise>
                                    <display:column title="${col.title}" sortable="${col.sortable}" property="${col.name}" decorator="${col.decorator}" group="${col.group}"  />
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${ ! empty col.classstr && col.classstr != null }">
                                    <display:column title="${col.title}" sortable="${col.sortable}" property="${col.name}" class="${col.classstr}" group="${col.group}" />                    
                                </c:when>
                                <c:otherwise>
                                    <display:column title="${col.title}" sortable="${col.sortable}" property="${col.name}" group="${col.group}" />                    
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </c:when>
                <c:otherwise>
                    <c:choose>
                        <c:when test="${ ! empty col.decorator && col.decorator != null }">
                            <c:choose>
                                <c:when test="${ ! empty col.classstr && col.classstr != null }">
                                    <display:column title="${col.title}" sortable="${col.sortable}" property="${col.name}" class="${col.classstr}" decorator="${col.decorator}"  />
                                </c:when>
                                <c:otherwise>
                                    <display:column title="${col.title}" sortable="${col.sortable}" property="${col.name}" decorator="${col.decorator}"  />
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${ ! empty col.classstr && col.classstr != null }">
                                    <display:column title="${col.title}" sortable="${col.sortable}" property="${col.name}" class="${col.classstr}" />                    
                                </c:when>
                                <c:otherwise>
                                    <display:column title="${col.title}" sortable="${col.sortable}" property="${col.name}" />                    
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </display:table>
</c:catch>
<c:if test="${!empty error}">
    <font color="red">Problem generating the table: ${error}</font>
    <pre>
                ${input}
    </pre>
</c:if>