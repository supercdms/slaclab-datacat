/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.srs.web.base.filters.login;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
//import jakarta.xml.bind.DatatypeConverter;
import javax.xml.bind.DatatypeConverter;
import junit.framework.TestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

/**
 *
 * @author bvan
 */
public class HmacValidatorTest extends TestCase {
    public HmacValidatorTest(String testName){
        super( testName );
    }

    public void testRequestToString() throws Exception{
        System.out.println( "requestToString" );
        HmacValidator instance = new HmacValidator();
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/fake/resource");
        request.setPathInfo( "/fake/resource");
        request.addHeader( "content-type", "application/xml");
        request.addHeader( "date", "Wed, 21 Aug 2013 19:01:08 GMT");
        String requested = "GET\n/fake/resource\n\napplication/xml\nWed, 21 Aug 2013 19:01:08 GMT\n";
        System.out.print("Request:\n" + requested);
        System.out.print("Actual:\n" + instance.requestToString( request ) );
        assertEquals( "Request string: ", requested, instance.requestToString( request ) );
    }

    /**
     * Test of validateHash method, of class HmacValidator.
     */
    public void testValidateHash() throws Exception{
        System.out.println( "validateHash" );
        KeyGenerator keyg = KeyGenerator.getInstance( "HmacSHA1");
        SecretKey secretKey = keyg.generateKey();
        String keyId = UUID.randomUUID().toString();

        System.out.println( "Offset too old" );
        assertFalse( checkRequest(secretKey, keyId, getHeaderTime(-1000000) ) );

        System.out.println( "Offset -2" );
        assertTrue(checkRequest(secretKey, keyId, getHeaderTime(-2000) ) );

        System.out.println( "Offset -1.9" );
        assertTrue(checkRequest(secretKey, keyId, getHeaderTime(-1999) ) );

        System.out.println( "Offset 0" );
        assertTrue(checkRequest(secretKey, keyId, getHeaderTime(0) ) );

        System.out.println( "Offset in allowed skew" );
        assertTrue( checkRequest(secretKey, keyId, getHeaderTime(999) ) );

        System.out.println( "Offset in future" );
        assertFalse( checkRequest(secretKey, keyId, getHeaderTime(11000) ) );

        System.out.println( "No Date" );
        assertFalse( checkRequest(secretKey, keyId, null) );
    }

    private boolean checkRequest(SecretKey secretKey, String keyId, Date date)
            throws Exception {
        HmacValidator instance = new HmacValidator();
        MockHttpServletRequest request = newRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        String secretKeyB64 = DatatypeConverter.printBase64Binary( secretKey.getEncoded() );

        Mac hmac = Mac.getInstance( "HmacSHA1" );
        hmac.init( secretKey );

        if(date != null){
            request.addHeader( "date", date );
        }

        byte[] clientDigestRaw = hmac.doFinal( instance.requestToString( request ).getBytes() );
        String clientDigest = DatatypeConverter.printBase64Binary( clientDigestRaw );
        request.addHeader( "authorization", "SRS:" + keyId + ":" + clientDigest);
        return instance.validateHash( secretKeyB64, clientDigest, request, response );
    }

    private Date getHeaderTime(long offset){
        return new Date( System.currentTimeMillis() + offset );
    }

    private MockHttpServletRequest newRequest(){
        String path = "/test/path";
        MockHttpServletRequest request = new MockHttpServletRequest("GET", path){

            @Override
            public String getHeader(String name){
                if("date".equals( name)){
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "EEE, dd MMM yyyy HH:mm:ss z", Locale.US );
                    dateFormat.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
                    return dateFormat.format( new Date(super.getDateHeader( "date" ) ) );
                }
                return super.getHeader( name );
            }
        };
        request.setPathInfo( path );
        request.addHeader( "content-type", "application/xml");
        return request;
    }

}
