package org.srs.web.base.decorator;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.displaytag.decorator.*;

/**
 *
 * @author The FreeHEP team @ SLAC
 */
public class BaseDecorator extends TableDecorator {
    
    // FIXME: We should eliminate duplication between this class and TimestampColumnDecorator
    private SimpleDateFormat format;
    private String formatStr = "dd-MMM-yyyy HH:mm:ss.SSS";
    private String timeZone = "UTC";
    
    private SimpleDateFormat getFormat() {
        if ( format == null ) {
            format = new SimpleDateFormat(formatStr);
            format.setTimeZone(TimeZone.getTimeZone(timeZone));
        }
        return format;
    }
    
    
    public String getDatestring() {
        if ( format == null ) {
            Object tmpformatStr = getPageContext().getSession().getAttribute("dateFormat");
            Object tmptimeZone  = getPageContext().getSession().getAttribute("timeZone");
            if ( tmpformatStr != null )
                this.formatStr = (String) tmpformatStr;
            if ( tmptimeZone != null )
                this.timeZone = (String) tmptimeZone;
            // FIXME: The format and timezone are never used, nor is it clear how to do this since this
            // format object is shared between all session.
            format = getFormat();
            
        }
        // FIXME: This is not thread safe, need to synchronize on format
        return format.format( getCurrentRowObject() );
    }
    
}
